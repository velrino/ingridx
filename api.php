<?php

function get_beer($page, $slug){
 
	$ch = curl_init();
 
	$optArray = array(
		CURLOPT_URL => 'https://api.punkapi.com/v2/beers'.$slug.'&page='.$page.'&per_page=3',
		CURLOPT_RETURNTRANSFER => true
	);
 
	curl_setopt_array($ch, $optArray);
	$result = curl_exec($ch);
	
	$response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	
	curl_close($ch);
	
	if($response == '200'){
		return json_decode($result);
	}else{
		return $response;
	}
    
}

$page = 20;

$abv_gt = 4;
$abv_lt = 10;
$ibu_gt = 5;
$ibu_lt = 31;
$ebc_gt = 5;
$ebc_lt = 20;
$beer_name = "a";

$slug = "?abv_gt=" . $abv_gt . "&abv_lt=" . $abv_lt . "&ibu_gt=" . $ibu_gt . "&ibu_lt=" . $ibu_lt . "&ebc_gt=" . $ebc_gt . "&ebc_lt=" . $ebc_lt . "&beer_name=" . $beer_name . "";

for($i = 1; $i < $page; $i++){
 
    $return = get_beer($i, $slug);
    
    if(!empty($return)){
	    $arr[] = $return;
    }
    sleep(1);
}

echo "<pre>";
print_r($arr);



?>
