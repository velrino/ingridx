<?php 

$boleto = array (
	'cnpj' => '27124614000131',
	'chave' => '6cded98c-6165-4fd3-87c1-a1168146a388',
	'referencia' => '423',
	'cliente' => 
	array (
		'nome' => 'Marcus Brune',
		'email' => 'marcus-teste@hotmail.com',
		'identidade' => '392833852',
		'endereco' => 
		array (
			'rua' => 'Rua do Pagamento',
			'numero' => '12',
			'complemento' => 'casa dos fundos',
			'cep' => '13200000',
			'cidade' => 'Jundiaí',
			'estado' => 'SP',
			'pais' => 'BRA',
		),
	),
	'pagamento' => 
	array (
		'tipo' => 'boleto',
		'valor' => 12300,
		'moeda' => 'BRL',
		'pais' => 'BRA',
		'parcelas' => 1,
		'instrucoes' => 'Não aceitar após vencimento.',
		'demonstrativo' => 'NOME DA EMPRESA - REF.:**SEU DEMONSTRATIVO AQUI**',
	),
);


$json = json_encode($boleto);

$url = "teste.ultrapag.com.br/v1/transacao";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
   'Content-Type: application/json',
   'Content-Length: ' . strlen($json)
)); 

$result = curl_exec($curl);

$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

$array_content = json_decode($result);
$content = get_object_vars($array_content);

$boleto = $content['pagamento']->boleto_link;

if($httpcode == '200') {
	header("Location: ".$boleto);
}
else {
	echo "Erro";
	exit();
}



curl_close($curl);

?>