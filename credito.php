<?php

$data = array (
	'cnpj' => '27124614000131',
	//produção = beb7eff1-e1a4-4d8b-832e-2defb1ec7f26
	'chave' => '6cded98c-6165-4fd3-87c1-a1168146a388',
	'referencia' => '5515646',
	'cliente' => 
	array (
		'nome' => 'José da Silva',
		'email' => 'josesilva@email-teste.com',
		'identidade' => '392833852',
		'endereco' => 
		array (
			'rua' => 'Rua dos Pagamentos',
			'numero' => '12',
			'complemento' => 'casa dos fundos',
			'cep' => '13200000',
			'cidade' => 'Jundiaí',
			'estado' => 'SP',
			'pais' => 'BRA',
		),
	),
	'pagamento' => 
	array (
		'tipo' => 'credito',
		'valor' => 171,
		'moeda' => 'BRL',
		'pais' => 'BRA',
		'parcelas' => 1,
		'cartao' => 
		array (
			'cartao_numero' => '4000000000000001',
			'cartao_nome' => 'Jose da Silva',
			'data_expiracao' => '08/2019',
			'cvv' => '123',
			'bandeira' => 'master',
		),
	),
);

$json = json_encode($data);

$url = "teste.ultrapag.com.br/v1/transacao";

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(  
   'Content-Type: application/json',
   'Content-Length: ' . strlen($json)
)); 

$result = curl_exec($curl);

$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

echo "<pre>";
print_r($result);

if($httpcode == '200') {
	echo "Pagamento efetuado com sucesso";
}
else {
	echo "Erro";
	exit();
}


curl_close($curl);

?>	
