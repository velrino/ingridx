<?php
/*
Template Name: Login cliente
*/

get_header();

?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<style>
	
	.bloco-login{
        margin-left: 23% !important;
        margin-right: 35% !important;
        background: #FFFFFF;
        padding-bottom: 10px;
        -webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        -moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
	}
    
    .bloco-login h2{
        color: #876A42;
        font-size: 32px;
        margin-bottom: 25px;

    }
    .bloco-login-med h2{
        color: #344C5B;
        font-size: 32px;
        margin-bottom: 25px;

    }
	.form-group{
        margin-bottom: 0;
        margin-left: auto;
        margin-right: auto;
    }
    .form-group input{
        margin: 0;
        margin-bottom: 2px;
        width: 100%;
        margin-left: 30px;
        margin-right: 30px;
        background: #876A42;
        color: #FFFFFF;
    }

    .form-group.med input{
        margin: 0;
        margin-bottom: 2px;
        width: 100%;
        margin-left: 30px;
        margin-right: 30px;
        background: #344C5B;
        color: #FFFFFF;
    }
    
    .btn-ingridix-med{
        width: 100%;
        height: 50px;
        border-radius: 40px;
        background: #7AB8DB;
        color: #FFFFFF;
        font-weight: bolder;
        margin-top: 15px;
        font-size: 17px;
    }

    .btn-ingridix-cli{
        width: 100% !important;
        height: 50px;
        border-radius: 40px;
        background: #ECBB71 !important;
        color: #FFFFFF;
        font-weight: bolder;
        margin-top: 15px;
        font-size: 17px;
        margin-bottom: 0 !important;
        padding: 0 10px 0 10px !important;
    }
    
    .no-padding{
        padding: 0;
    }
    .small{
        margin-top: 5px;
        font-size: 13px;
        color: #545454;
        margin-left: 30px;
    }
    .clique_aqui{
        margin-top: 5px;
        font-size: 14px;
        color: #876A42;
    }
    
    #wrapper{
        background: #f1f1f1;
    }
    
    .titulo-pagina{
     
        margin: -20px 0 35px 45px;
    }
  
    a{
        text-decoration: underline;
        color: #876A42 ;
    }





    /* Breakpoints */
    @media only screen and (min-width: 320px) and (max-width: 620px) {

        .bloco-login {
            margin-left: 0 !important;
            margin-right: 0 !important;
        }

        .titulo-pagina {
            margin: -20px 0 0px 0px !important;
        }

        .container {
        
        }
        .form-group input{
            margin-left: 0 !important;
        }
        
        .btn-div{
            margin-left: 0 !important;
        }

        .bloco-login h2 {
            padding-top: 20px;
        }
        
        .no-padding-mobile{
            padding: 0 !important;
        }

        .bloco-login-med h2{
            font-size: 23px !important;
        }
        
        .titulo-pagina{
            font-size: 32px;
        }
    }

    @media only screen and (min-width: 621px) and (max-width: 940px) {
        .bloco-login {
            margin-left: 0 !important;
            margin-right: 0 !important;
        }

        .titulo-pagina {
            margin: -20px 0 0px 0px !important;
        }

        .container {
        
        }
        .form-group input{
            margin-left: 0 !important;
        }

        .btn-div{
            margin-left: 0 !important;
        }

        .bloco-login h2 {
            padding-top: 20px;
        }
        
       
    }

    @media only screen and (min-width: 941px) and (max-width: 1260px) {

    }

    @media only screen and (min-width: 1261px) {

    }
    
    
    
    
</style>
<center>
	<div class="container">
        <h1 class="titulo-pagina">ÁREA DO USUÁRIO</h1>
         <div class="col-md-10 no-padding-mobile">
             <div class="col-md-6 no-padding-mobile">
                
                 <div class="col-md-12 bloco-login">
                     <h2>ACESSO CLIENTE</h2>
                     <form name="loginform" id="loginform" class="form-horizontal" method="post">
                         <div class="form-group ">
                             <div class="col-md-10 ">
                                 <input type="text" name="cpf_login" id="cpf_login"  class="input" size="20" placeholder="CPF" />
                             </div>
                         </div>
                         <div class="form-group ">
                             <div class="col-md-10 ">
                                 <input type="password" name="user_pass" id="user_pass" class="input" value="" size="20" placeholder="Senha" />
                             </div>
                         </div>

                         <div class="col-md-10">
                             <small class="pull-left small">Esqueceu a senha <u><a href="<?php echo wp_lostpassword_url('acesso-cliente'); ?>" title="Lost Password">clique aqui</a></u></small>
                         </div>

                         <div class="col-md-10  btn-div" style="margin-left: 26px">
                             <button type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large btn-ingridix-cli"  >ACESSAR RECEITAS INGRIDIX</button>
                         </div>

                         <div class="col-md-12" style="margin-left: 30px">
                             <p class="pull-left clique_aqui" >Ou clique <u><a href="<?= get_site_url().'/cadastro-cliente'; ?>">aqui</a></u> e faça seu cadastro.</p>
                         </div>
                         <input type="hidden" name="tipo_user" value="paciente">
                     </form>
                 </div>
             </div>

             <div class="col-md-6 no-padding-mobile">
           
                 <div class="col-md-12 bloco-login bloco-login-med">
                     <h2 style="font-size: 25px">ACESSO PROFISSIONAL DA SAÚDE</h2>
                     <form name="loginform" id="loginform" class="form-horizontal" method="post" style="margin-top: 33px">
                         <div class="form-group med">
                             <div class="col-md-10 ">
                                 <input type="text" name="cpf_login" id="cpf_login_med"  class="input" size="20" placeholder="CPF" />
                             </div>
                         </div>
                         <div class="form-group med">
                             <div class="col-md-10 ">
                                 <input type="password" name="user_pass" id="user_pass" class="input" value="" size="20" placeholder="Senha" />
                             </div>
                         </div>

                         <div class="col-md-10">
                             <small class="pull-left small">Esqueceu a senha <u><a href="<?php echo wp_lostpassword_url('acesso-cliente'); ?>" title="Lost Password">clique aqui</a></u></small>
                         </div>

                         <div class="col-md-10 btn-div" style="margin-left: 26px">
                             <button type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large btn-ingridix-med"  >ACESSAR INGRIDIX LAB</button>
                         </div>

                         <div class="col-md-12" style="margin-left: 30px">
                             <p class="pull-left clique_aqui">Ou clique <u><a href="<?= get_site_url().'/cadastro-medico'; ?>">aqui</a></u> e faça seu cadastro.</p>
                         </div>
                         <input type="hidden" name="tipo_user" value="medico">
                     </form>
                 </div>
             </div>
         </div>
	</div>
</center>

    <script src='<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.min.js'></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.maskedinput.min.js"></script>
   
    <script>
        $(function(){
            $("#cpf_login, #cpf_login_med").mask("999.999.999-99");
        })
        
    </script>

<?php
get_footer();
?>