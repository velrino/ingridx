<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * Load framework
 */
require_once ( trailingslashit( get_template_directory() ) . '/inc/init.php' );

/**
 * Load TGM Plugin
 */
if( !function_exists('jevelin_register_required_plugins') ) :
    require_once ( trailingslashit( get_template_directory() ) . '/inc/plugins/TGM-Plugin-Activation/class-tgm-plugin-activation.php' );
    function jevelin_register_required_plugins() {

        tgmpa(array(
            array(
                'name'      => esc_html__( 'Unyson', 'jevelin' ),
                'slug'      => 'unyson',
                'required'  => true,
            ),

            array(
                'name'      => esc_html__( 'WooCommerce', 'jevelin' ),
                'slug'      => 'woocommerce',
                'required'  => false,
            ),

            array(
                'name'      => esc_html__( 'Revolution slider', 'jevelin' ),
                'slug'      => 'revslider',
                'source'    => trailingslashit( get_template_directory() ) . '/inc/plugins/revslider.zip',
                'required'  => false,
                'version'   => '5.4.5.1',
            ),

            array(
                'name'      => esc_html__( 'Envato WordPress Toolkiter', 'jevelin' ),
                'slug'      => 'envato-wordpress-toolkit',
                'source'    => trailingslashit( get_template_directory() ) . '/inc/plugins/envato.zip',
                'required'  => false,
                'version'   => '1.7.3',
            ),
        ), array( 'is_automatic' => true ));

    }
    add_action( 'tgmpa_register', 'jevelin_register_required_plugins' );
endif;

//Campos personalizados
add_action('init', 'user_login');
function user_login(){
	
	//Login paciente
	if(isset($_POST['cpf_login']) && $_POST['user_pass'] && $_POST['tipo_user'] == 'paciente')
	{
		global $wpdb;
		
		$usercpf = $wpdb->escape($_REQUEST['cpf_login']);
		$password = $wpdb->escape($_REQUEST['user_pass']);

		$usermeta = $wpdb->get_results( "SELECT * FROM wp_usermeta WHERE meta_value = '".$usercpf."'" );
		$userbyid = get_user_by('id', $usermeta[0]->user_id);
		
		$confirmType = $wpdb->get_results( "SELECT * FROM wp_usermeta WHERE user_id = '".$usermeta[0]->user_id."' and meta_value = 'paciente'" );
		
		if(count($confirmType) > 0){
			$username = $userbyid->user_login;
			
			$creds = array('user_login' => $username, 'user_password' => $password, 'remember' => true );
			$user = wp_signon( $creds, false );
			wp_set_current_user($user->ID);
			
			if(is_user_logged_in()){
				echo "<script type='text/javascript'>window.location.href='". get_site_url().'/area-cliente' ."'</script>";
				exit();
			}
		}
	}
	
	//Login médico
	if(isset($_POST['cpf_login']) && $_POST['user_pass'] && $_POST['tipo_user'] == 'medico')
	{
		global $wpdb;
		
		$usercrm = $wpdb->escape($_REQUEST['cpf_login']);
		$password = $wpdb->escape($_REQUEST['user_pass']);

		$usermeta = $wpdb->get_results( "SELECT * FROM wp_usermeta WHERE meta_value = '".$usercrm."'" );
		$userbyid = get_user_by('id', $usermeta[0]->user_id);
	
		$confirmType = $wpdb->get_results( "SELECT * FROM wp_usermeta WHERE user_id = '".$usermeta[0]->user_id."' and meta_value = 'medico'" );
		
		if(count($confirmType) > 0){
			$username = $userbyid->user_login;
			
			$creds = array('user_login' => $username, 'user_password' => $password, 'remember' => true );
			$user = wp_signon( $creds, false );
			wp_set_current_user($user->ID);
			
			if(is_user_logged_in()){
				echo "<script type='text/javascript'>window.location.href='". get_site_url().'/area-medico' ."'</script>";
				exit();
			}
		}
	}
}

add_action('init', 'add_novo_paciente');
function add_novo_paciente() {
	//Login paciente
	if(isset($_POST['nome_paciente_amed']) && $_POST['novo_paciente'] == 'novo_paciente') {
		global $wpdb;
		global $DATAUSER;
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
		
		$nome    = wp_strip_all_tags($_REQUEST['nome_paciente_amed']);
		$celular = wp_strip_all_tags($_REQUEST['cel_amed']);
		$cpf     = wp_strip_all_tags($_REQUEST['cpf_amed']);
		$email   = wp_strip_all_tags($_REQUEST['email_amed']);
		
		if(validaCPF($cpf) == false){
			$alert = "O CPF inserido é inválido!";
			$DATAUSER['error_cpf']   = $alert;
			$DATAUSER['error_nome']  = $nome;
			$DATAUSER['error_cel']   = $celular;
			$DATAUSER['error_email'] = $email;
			
			return $DATAUSER;
		}else{
			
			$confirmCpf = $wpdb->get_results( "SELECT * FROM cadastros_pacientes WHERE user_id = '".$user_id."' and cpf_paciente = '".$cpf."'" );
			
			if(count($confirmCpf) > 0){
				$alert = "O CPF inserido já foi cadastrado!";
				$DATAUSER['error_cpf'] = $alert;
				$DATAUSER['error_nome'] = $nome;
				$DATAUSER['error_cel'] = $celular;
				$DATAUSER['error_email'] = $email;
			
				return $DATAUSER;
			}else{
				$data['user_id']        =  $user_id;
				$data['nome_paciente']  =  $nome;
				$data['cpf_paciente']   =  $cpf;
				$data['cel_paciente']   =  $celular;
				$data['email_paciente'] =  $email;
				
				if($wpdb->insert("cadastros_pacientes", $data)){
					$alert = "Paciente inserido com sucesso!";
					$DATAUSER['sucesso_amed'] = $alert;
					
					return $DATAUSER;
				}
			}
		}
	}
}

add_action('init', 'add_my_user');
function add_my_user() {
	
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	
	if(isset($_POST['first_name']) && $_POST['cpf'] && $_POST['tipo_user'] == 'paciente') {
		
		global $wpdb;
		global $DATAUSER;
		$name = wp_strip_all_tags($_REQUEST['first_name']);
		$email = wp_strip_all_tags($_REQUEST['user_email']);
		$password = wp_strip_all_tags($_REQUEST['user_pass']);
		$cpf = wp_strip_all_tags($_REQUEST['cpf']);
		
		$username = $name;
		$fistname = current( str_word_count(  $username, 2 ) );
		$lastname = str_replace("".$fistname."", "", $name);
		
		$myrows = $wpdb->get_results( "SELECT * FROM wp_usermeta WHERE meta_value LIKE '%".$cpf."%' " );
		
		$user_id = wp_create_user( $fistname.$lastname, $password, $email );
		
		if(validaCPF($cpf) == false){
			
			$error = "O CPF inserido é inválido!";
			$DATAUSER['error'] = $error;
			create_session_user();
			wp_delete_user( $user_id );
			
		}elseif(count($myrows) > 0){
			
			$error = "O CPF inserido já foi cadastrado!";
			$DATAUSER['error'] = $error;
			create_session_user();
			wp_delete_user( $user_id );
			
		}elseif( !is_wp_error( $user_id ) ){
			unset($DATAUSER['error']);
			$user = get_user_by( 'id', $user_id );
			$user->set_role( 'customer' );
			
			$endereco = wp_strip_all_tags($_REQUEST['endereco']) . ", " . wp_strip_all_tags($_REQUEST['numero']);
			
			update_usermeta( $user_id, 'first_name', $fistname);
			update_usermeta( $user_id, 'last_name', $lastname);
			update_usermeta( $user_id, 'billing_first_name', $fistname);
			update_usermeta( $user_id, 'billing_last_name', $lastname);
			update_usermeta( $user_id, 'billing_address_1', $endereco);
			update_usermeta( $user_id, 'billing_address_2', wp_strip_all_tags($_REQUEST['complemento']));
			update_usermeta( $user_id, 'billing_bairro', wp_strip_all_tags($_REQUEST['bairro']));
			update_usermeta( $user_id, 'billing_city', wp_strip_all_tags($_REQUEST['cidade']));
			update_usermeta( $user_id, 'billing_postcode', wp_strip_all_tags($_REQUEST['cep']));
			update_usermeta( $user_id, 'billing_state', wp_strip_all_tags($_REQUEST['estado']));
			update_usermeta( $user_id, 'billing_phone', wp_strip_all_tags($_REQUEST['celular']));
			update_usermeta( $user_id, 'billing_cpf', wp_strip_all_tags($_REQUEST['cpf']));
			update_usermeta( $user_id, 'tipo_user', 'paciente');
			
			$creds = array('user_login' => $username, 'user_password' => $password, 'remember' => true );
			$user = wp_signon( $creds, false );
			wp_set_current_user($user->ID);
			destroy_session_user();
			return $user;
			
		}else{
			$error = $user_id->get_error_message();
			$DATAUSER['error'] = $error;
			create_session_user();
			wp_delete_user( $user_id );
		}
	}
	
	if(isset($_POST['first_name']) && $_POST['cpf'] && $_POST['tipo_user'] == 'medico') {
	
		global $wpdb;
		global $DATAUSER;
		$name = wp_strip_all_tags($_REQUEST['first_name']);
		$email = wp_strip_all_tags($_REQUEST['user_email']);
		$password = wp_strip_all_tags($_REQUEST['user_pass']);
		$cpf = wp_strip_all_tags($_REQUEST['cpf']);
		$crm = wp_strip_all_tags($_REQUEST['crm']);
		
		$username = $name;
		$fistname = current( str_word_count(  $username, 2 ) );
		$lastname = str_replace("".$fistname."", "", $name);
		
		$myrows = $wpdb->get_results( "SELECT * FROM wp_usermeta WHERE meta_value LIKE '%".$cpf."%' " );
		
		$user_id = wp_create_user( $fistname.$lastname, $password, $email );
		
		if(validaCPF($cpf) == false){
			
			$error = "O CPF inserido é inválido!";
			$DATAUSER['error'] = $error;
			create_session_user();
			wp_delete_user( $user_id );
			
		}elseif(count($myrows) > 0){
			
			$error = "O CPF inserido já foi cadastrado!";
			$DATAUSER['error'] = $error;
			create_session_user();
			wp_delete_user( $user_id );
			
		}elseif( !is_wp_error( $user_id ) ){
			unset($DATAUSER['error']);
			$user = get_user_by( 'id', $user_id );
			$user->set_role( 'customer' );
			update_usermeta( $user_id, 'first_name', $fistname);
			update_usermeta( $user_id, 'last_name', $lastname);
			update_usermeta( $user_id, 'billing_first_name', $fistname);
			update_usermeta( $user_id, 'billing_last_name', $lastname);
			update_usermeta( $user_id, 'billing_state', wp_strip_all_tags($_REQUEST['estado']));
			update_usermeta( $user_id, 'billing_phone', wp_strip_all_tags($_REQUEST['celular']));
			update_usermeta( $user_id, 'billing_cpf', wp_strip_all_tags($_REQUEST['cpf']));
			update_usermeta( $user_id, 'crm', wp_strip_all_tags($_REQUEST['crm']));
			update_usermeta( $user_id, 'tipo_user', 'medico');
			
			$creds = array('user_login' => $username, 'user_password' => $password, 'remember' => true );
			$user = wp_signon( $creds, false );
			wp_set_current_user($user->ID);
			destroy_session_user();
			return $user;
			
		}else{
			$error = $user_id->get_error_message();
			$DATAUSER['error'] = $error;
			create_session_user();
			wp_delete_user( $user_id );
		}
	}
}

add_action('init', 'edit_my_user');
function edit_my_user() {
	
	require_once( ABSPATH.'wp-admin/includes/user.php' );
	
	if(isset($_POST['tipo_user']) == 'edit_paciente') {
		
		$user_id = get_current_user_id();
		
		$endereco = wp_strip_all_tags($_REQUEST['endereco']) . ", " . wp_strip_all_tags($_REQUEST['numero']);
		
		update_user_meta( $user_id, 'billing_address_1', $endereco);
		update_user_meta( $user_id, 'billing_address_2', wp_strip_all_tags($_REQUEST['complemento']));
		update_user_meta( $user_id, 'billing_bairro', wp_strip_all_tags($_REQUEST['bairro']));
		update_user_meta( $user_id, 'billing_city', wp_strip_all_tags($_REQUEST['cidade']));
		update_user_meta( $user_id, 'billing_postcode', wp_strip_all_tags($_REQUEST['cep']));
		update_user_meta( $user_id, 'billing_state', wp_strip_all_tags($_REQUEST['estado']));
		update_user_meta( $user_id, 'billing_phone', wp_strip_all_tags($_REQUEST['celular']));
	}
	
	if(isset($_POST['tipo_user']) == 'edit_medico') {
		
		$user_id = get_current_user_id();
		
		update_usermeta( $user_id, 'billing_state', wp_strip_all_tags($_REQUEST['estado']));
		update_usermeta( $user_id, 'billing_phone', wp_strip_all_tags($_REQUEST['celular']));
	}
}



function create_session_user(){
	global $DATAUSER;
	$DATAUSER['first_name'] = wp_strip_all_tags($_REQUEST['first_name']);
	$DATAUSER['sexo'] = wp_strip_all_tags($_REQUEST['sexo']);
	$DATAUSER['cpf'] = wp_strip_all_tags($_REQUEST['cpf']);
	$DATAUSER['idade'] = wp_strip_all_tags($_REQUEST['idade']);
	$DATAUSER['celular'] = wp_strip_all_tags($_REQUEST['celular']);
	$DATAUSER['email'] = wp_strip_all_tags($_REQUEST['user_email']);
	$DATAUSER['cep'] = wp_strip_all_tags($_REQUEST['cep']);
	$DATAUSER['endereco'] = wp_strip_all_tags($_REQUEST['endereco']);
	$DATAUSER['numero'] = wp_strip_all_tags($_REQUEST['numero']);
	$DATAUSER['bairro'] = wp_strip_all_tags($_REQUEST['bairro']);
	$DATAUSER['complemento'] = wp_strip_all_tags($_REQUEST['complemento']);
	$DATAUSER['cidade'] = wp_strip_all_tags($_REQUEST['cidade']);
	$DATAUSER['estado'] = wp_strip_all_tags($_REQUEST['estado']);
	$DATAUSER['crm'] = wp_strip_all_tags($_REQUEST['crm']);
}

function destroy_session_user(){
	global $DATAUSER;
	unset($DATAUSER['first_name']);
	unset($DATAUSER['sexo']);
	unset($DATAUSER['cpf']);
	unset($DATAUSER['idade']);
	unset($DATAUSER['celular']);
	unset($DATAUSER['email']);
	unset($DATAUSER['cep']);
	unset($DATAUSER['endereco']);
	unset($DATAUSER['numero']);
	unset($DATAUSER['bairro']);
	unset($DATAUSER['complemento']);
	unset($DATAUSER['cidade']);
	unset($DATAUSER['estado']);
	unset($DATAUSER['crm']);
}

function validaCPF($cpf = null) {
	
	// Verifica se um número foi informado
	if(empty($cpf)) {
		return false;
	}
	
	// Elimina possivel mascara
	$cpf = ereg_replace('[^0-9]', '', $cpf);
	$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
	
	// Verifica se o numero de digitos informados é igual a 11
	if (strlen($cpf) != 11) {
		return false;
	}
	// Verifica se nenhuma das sequências invalidas abaixo
	// foi digitada. Caso afirmativo, retorna falso
	else if ($cpf == '00000000000' ||
	         $cpf == '11111111111' ||
	         $cpf == '22222222222' ||
	         $cpf == '33333333333' ||
	         $cpf == '44444444444' ||
	         $cpf == '55555555555' ||
	         $cpf == '66666666666' ||
	         $cpf == '77777777777' ||
	         $cpf == '88888888888' ||
	         $cpf == '99999999999') {
		return false;
		// Calcula os digitos verificadores para verificar se o
		// CPF é válido
	} else {
		
		for ($t = 9; $t < 11; $t++) {
			
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf{$c} != $d) {
				return false;
			}
		}
		
		return true;
	}
}

function woocommerce_maybe_add_multiple_products_to_cart( $url = false ) {
	// Make sure WC is installed, and add-to-cart qauery arg exists, and contains at least one comma.
	if ( ! class_exists( 'WC_Form_Handler' ) || empty( $_REQUEST['add-to-cart'] ) || false === strpos( $_REQUEST['add-to-cart'], ',' ) ) {
		return;
	}
	
	// Remove WooCommerce's hook, as it's useless (doesn't handle multiple products).
	remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'add_to_cart_action' ), 20 );
	
	$product_ids = explode( ',', $_REQUEST['add-to-cart'] );
	$count       = count( $product_ids );
	$number      = 0;
	
	foreach ( $product_ids as $id_and_quantity ) {
		// Check for quantities defined in curie notation (<product_id>:<product_quantity>)
		// https://dsgnwrks.pro/snippets/woocommerce-allow-adding-multiple-products-to-the-cart-via-the-add-to-cart-query-string/#comment-12236
		$id_and_quantity = explode( ':', $id_and_quantity );
		$product_id = $id_and_quantity[0];
		
		$_REQUEST['quantity'] = ! empty( $id_and_quantity[1] ) ? absint( $id_and_quantity[1] ) : 1;
		
		if ( ++$number === $count ) {
			// Ok, final item, let's send it back to woocommerce's add_to_cart_action method for handling.
			$_REQUEST['add-to-cart'] = $product_id;
			
			return WC_Form_Handler::add_to_cart_action( $url );
		}
		
		$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $product_id ) );
		$was_added_to_cart = false;
		$adding_to_cart    = wc_get_product( $product_id );
		
		if ( ! $adding_to_cart ) {
			continue;
		}
		
		$add_to_cart_handler = apply_filters( 'woocommerce_add_to_cart_handler', $adding_to_cart->get_type(), $adding_to_cart );
		
		// Variable product handling
		if ( 'variable' === $add_to_cart_handler ) {
			woo_hack_invoke_private_method( 'WC_Form_Handler', 'add_to_cart_handler_variable', $product_id );
			
			// Grouped Products
		} elseif ( 'grouped' === $add_to_cart_handler ) {
			woo_hack_invoke_private_method( 'WC_Form_Handler', 'add_to_cart_handler_grouped', $product_id );
			
			// Custom Handler
		} elseif ( has_action( 'woocommerce_add_to_cart_handler_' . $add_to_cart_handler ) ){
			do_action( 'woocommerce_add_to_cart_handler_' . $add_to_cart_handler, $url );
			
			// Simple Products
		} else {
			woo_hack_invoke_private_method( 'WC_Form_Handler', 'add_to_cart_handler_simple', $product_id );
		}
	}
}

// Fire before the WC_Form_Handler::add_to_cart_action callback.
add_action( 'wp_loaded', 'woocommerce_maybe_add_multiple_products_to_cart', 15 );


/**
 * Invoke class private method
 *
 * @since   0.1.0
 *
 * @param   string $class_name
 * @param   string $methodName
 *
 * @return  mixed
 */
function woo_hack_invoke_private_method( $class_name, $methodName ) {
	if ( version_compare( phpversion(), '5.3', '<' ) ) {
		throw new Exception( 'PHP version does not support ReflectionClass::setAccessible()', __LINE__ );
	}
	
	$args = func_get_args();
	unset( $args[0], $args[1] );
	$reflection = new ReflectionClass( $class_name );
	$method = $reflection->getMethod( $methodName );
	$method->setAccessible( true );
	
	$args = array_merge( array( $class_name ), $args );
	return call_user_func_array( array( $method, 'invoke' ), $args );
}

add_action( 'rest_api_init', 'wpc_register_wp_api_endpoints' );
function wpc_register_wp_api_endpoints() {
	register_rest_route( 'api/pedido_io', '/status', array(
		'methods' => 'POST',
		'callback' => 'wpc_somename_search_callback',
	));
}

function wpc_somename_search_callback( $request_data ) {
	
	global $wpdb;
	
	$referencia = $_POST['id_pedido'];
	$valor      = str_replace(",", ".", $_POST['valor']);
	$status     = $_POST['status'];
	$token      = $_POST['token'];
	
	if(empty($valor)){
		$response = array(
			"sucesso" => "false",
			"erro" => "Campo valor vazio!"
		);
		
		$json = json_encode($response, JSON_UNESCAPED_SLASHES);
		
		return  json_decode($json) ;
	}elseif(empty($status)){
		$response = array(
			"sucesso" => "false",
			"erro" => "Campo status vazio!"
		);
		
		$json = json_encode($response, JSON_UNESCAPED_SLASHES);
		
		return  json_decode($json) ;
	}elseif(empty($token)){
		$response = array(
			"sucesso" => "false",
			"erro" => "Campo token vazio!"
		);
		
		$json = json_encode($response, JSON_UNESCAPED_SLASHES);
		
		return  json_decode($json) ;
		
	}elseif($token == "4lBZp2ydYd"){
		
		$select = $wpdb->get_results("select rr.id_ref, rr.id_pac, cp.cel_paciente, cp.cpf_paciente from referencia_receitas rr join cadastros_pacientes cp where id_ref = ". $referencia ." and rr.id_pac = cp.id_paciente ");
		
		if(count($select) == 0){
			$response = array(
				"sucesso" => "false",
				"erro" => "O id do pedido inserido não existe!"
			);
			
			$json = json_encode($response, JSON_UNESCAPED_SLASHES);
			
			return  json_decode($json) ;
		}else{
			if(!empty($referencia)){
				
				//Função pra pegar id do usuario wordpress
				$cpf_paciente = $select[0]->cpf_paciente;
				$select_pac = $wpdb->get_results("select user_id from wp_usermeta where meta_value = '" . $cpf_paciente . "' ");
				
				if(count($select_pac) > 0){
					$user_id = $select_pac[0]->user_id;
					
					$cep = get_user_meta( $user_id , 'billing_postcode', true );
					
					if(!empty($cep)){
						$frete      = calcula_frete('04510','05314000','' . $cep . '','1','n','0','n');
						
						if($status == 2){
							$wpdb->query("UPDATE referencia_receitas set status = '". $status ."', valor = $valor, frete = $frete where id_ref = $referencia ");
						}else{
							$wpdb->query("UPDATE referencia_receitas set status = '". $status ."' where id_ref = $referencia ");
						}
						
					}else{
						$wpdb->query("UPDATE referencia_receitas set status = '". $status ."', valor = $valor where id_ref = $referencia ");
					}
					
				}else{
					$wpdb->query("UPDATE referencia_receitas set status = '". $status ."', valor = $valor where id_ref = $referencia ");
				}
				
				//Envia o numero para api de sms e dispara o sms para o paciente cadastrado pelo médico
				$numero = "55".$select[0]->cel_paciente;
				$numero = str_replace("(", "", $numero);
				$numero = str_replace(")", "", $numero);
				$numero = str_replace(" ", "", $numero);
				$numero = str_replace("-", "", $numero);
				
				send_sms($numero, $status);
				
				$response = array(
					"sucesso" => "true",
					"erro" => "Dados inseridos com sucesso!",
					"Numero" => "". $numero .""
				);
				
				$json = json_encode($response, JSON_UNESCAPED_SLASHES);
				
				return  json_decode($json);
			}else{
				$response = array(
					"sucesso" => "false",
					"erro" => "Erro ao inserir os dados!"
				);
				
				$json = json_encode($response, JSON_UNESCAPED_SLASHES);
				
				return  json_decode($json) ;
			}
		}
	}else{
		
		$response = array(
			"sucesso" => "false",
			"erro" => "Token inválido!"
		);
		
		$json = json_encode($response, JSON_UNESCAPED_SLASHES);
		
		return  json_decode($json) ;
	}
	
}

function send_sms($numero, $status){
	
	$msg = "";
	
	switch ($status){
		case 1:
			$msg = "Ingridix: Olá! Acabamos de receber a receita de seu composto probiótico. Em breve entraremos em contato.";
			break;
		case 2:
			$msg = "Ingridix: a receita de sua fórmula está disponível. Complete seu cadastro e/ou finalize seu pedido em https://loja.ingridix.com.br. Obrigado.";
			break;
		case 3:
			$msg = "Ingridix: seu pagamento foi confirmado. Sua fórmula está em produção em nosso laboratório e será enviada em breve. Você pode consultar o status de seu pedido em https://loja.ingridix.com.br. Obrigado.";
			break;
		case 4:
			$msg = "Ingridix: sua fórmula esta pronta e será enviada para o endereço cadastrado. Previsão de chegada entre 2 a 5 dias Obrigado.";
			break;
	}
	
	$data = array (
		"from" => "INGRIDIX",
		"to" => "". $numero ."",
		"text" => "". $msg .""
	);
	
	$json = json_encode($data);
	
	$url = "107.20.199.106/restapi/sms/1/text/single";
	
	$curl = curl_init();
	
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($json),
		'Authorization: Basic aW5ncmlkaXg6OERqRTgyS2xZOQ=='
	));
	
	$result = curl_exec($curl);
}

function calcula_frete($servico,$cep_origem,$cep_destino,$peso,$mao_propria,$valor_declarado,$aviso_recebimento){
	
	$mao_propria = (strtolower($mao_propria) == 's')?'s':'n';
	$aviso_recebimento = (strtolower($aviso_recebimento) == 's')?'s':'n';
	
	$url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem='. $cep_origem .'&sCepDestino='. $cep_destino .'&nVlPeso='. $peso .'&nCdFormato=1&nVlComprimento=20&nVlAltura=5&nVlLargura=15&sCdMaoPropria='. $mao_propria .'&nVlValorDeclarado='. $valor_declarado .'&sCdAvisoRecebimento='. $aviso_recebimento .'&nCdServico='. $servico .'&nVlDiametro=0&StrRetorno=xml';;
	
	//print_r(file_get_contents($url));die;
	$frete_calcula = simplexml_load_string(file_get_contents($url));
	/*
	CASO QUEIRA VER TUDO QUE VEM DO SITE DOS CORREIOS, DESCOMENTE A LINHA ABAIXO.
	echo print_r($frete_calcula);
	*/
	
	$frete = $frete_calcula->cServico;
	
	if($frete->Erro == '0'){
		
		switch($frete->Codigo){
			case '04510': $servico = 'PAC'; break;
			case '40045': $servico = 'SEDEX a Cobrar'; break;
			case '40215': $servico = 'SEDEX 10'; break;
			case '40290': $servico = 'SEDEX Hoje'; break;
			default: $servico = 'SEDEX';
		}
		
		$retorno = str_replace(',', '.', $frete->Valor);
		
	}elseif($frete->Erro == '7'){
		
		$retorno = 'Serviço temporariamente indisponível, tente novamente mais tarde.';
		
	}else{
		
		$retorno = 'Erro no cálculo do frete, código de erro: '.$frete->Erro;
		
	}
	
	return $retorno;
	
}