<?php
/*
Template Name: Functions ajax
*/

global $wpdb;

if($_POST['selecionar_paciente'] == 'ok'){
	$current_user = wp_get_current_user();
	$user_id = get_current_user_id();
	$nome = $_POST['nome'];
	
	$rows = $wpdb->get_results( "SELECT * FROM cadastros_pacientes WHERE nome_paciente LIKE '%".$nome."%' and user_id = '".$user_id."' order by nome_paciente asc " );
	
	echo json_encode($rows);
}

if($_GET['selecionar_sintomas'] == 'ok'){
	
	$args = array( 'post_type' => 'product', 'posts_per_page' => 100, 'offset'=> 0);
	$produtos = get_posts( $args );
	
	$cadaSintoma = "";
	foreach($produtos as $row):
		$sintomas = get_post_meta(  $row->ID , 'sintomas', true );
		
		$cadaSintoma .= $sintomas;
	
	endforeach;
	
	$newCadastroSintomas = explode(";", $cadaSintoma);
	
	$cadaSintoma = array_unique($newCadastroSintomas);
	
	
	$newString = "";
	foreach($cadaSintoma as $key => $row):
		
		$newString .= "".$row.",";
	
	endforeach;
	
	$newCadastroSintomas = explode(",", $newString);
	
	$cadaSintoma = array_unique($newCadastroSintomas);
	
	echo json_encode($cadaSintoma);
	
}

if($_POST['status_minha_receita'] == 'ok'){
	
	$id_medico        = $_POST['id_medico'];
	$id_minha_receita = $_POST['id_minha_receita'];
	$idpaciente       = $_POST['idpaciente'];
	
	/*$rows = $wpdb->get_results( "select cp.nome_paciente, mr.status from receitas_medicos rm join minhas_receitas mr join cadastros_pacientes cp join wp_posts wp
where rm.id_receita = mr.id_receita_med and mr.id_pac = cp.id_paciente and mr.id_pac = $idpaciente and mr.id_med = $id_medico
and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_minha_receita limit 1" );*/
	
	$rows = $wpdb->get_results( "SELECT * FROM referencia_receitas WHERE id_ref = (SELECT MAX(id_ref) FROM referencia_receitas)" );;
	echo ($rows[0]->id_ref + 1);
}

if($_POST['repetir_receita_medico'] == 'ok'){
	
	$id_medico       = $_POST['id_medico'];
	$idpaciente      = $_POST['idpaciente'];
	
	//Inserir na tabela referencias_receita
	$data['id_med']       =  $id_medico;
	$data['id_pac']       =  $idpaciente;
	
	$wpdb->insert("referencia_receitas", $data);
	
	$arr = array(
		'success' => 'ok'
	);
	
	echo json_encode($minhas_receitas);
	
}

if($_POST['editar_receita'] == 'ok'){
	
	$nome_receita        = $_POST['nome_receita'];
	$id_minha_receita    = $_POST['id_minha_receita'];
	
	$data['nome_receita'] = $nome_receita;
	$data['id_receita']   = $id_minha_receita;
	
	$update = $wpdb->update( "receitas_medicos", array("nome_receita" => $nome_receita), array("id_receita" => $id_minha_receita) );
	
	if($update){
		echo "done";
	}else{
		echo "error";
	}
}

if($_POST['add_receita_medico'] == 'ok'){
	
	$id_medico       = $_POST['id_medico'];
	$idpaciente      = $_POST['idpaciente'];
	$guardar         = $_POST['guardar'];
	
	//Inserir na tabela referencias_receita
	$data['id_med']       =  $id_medico;
	$data['id_pac']       =  $idpaciente;
	
	$wpdb->insert("referencia_receitas", $data);
	
	$ultima_referencia_inserida = $wpdb->get_results( "SELECT id_ref FROM referencia_receitas WHERE id_ref = (SELECT MAX(id_ref) FROM referencia_receitas)" );
	
	//Pega telefone do paciente caso houver
	$tel_paciente = $wpdb->get_results("SELECT cel_paciente from cadastros_pacientes where id_paciente = $idpaciente ");
	
	if(count($tel_paciente[0]->cel_paciente) > 0){
		//Envia o numero para api de sms e dispara o sms para o paciente cadastrado pelo médico
		$numero = "55".$tel_paciente[0]->cel_paciente;
		$numero = str_replace("(", "", $numero);
		$numero = str_replace(")", "", $numero);
		$numero = str_replace(" ", "", $numero);
		$numero = str_replace("-", "", $numero);
		
		send_sms($numero, 1);
	}
	//Inserir na tabela receitas_medicos
	$data['id_med']       =  $id_medico;
	$data['dataCriacao']  =  date('d-m-Y h:i:s');
	$data['guardar']      = $guardar;
	$data['id_ref']       = $ultima_referencia_inserida[0]->id_ref;
	
	$wpdb->query("insert into receitas_medicos (id_med, id_ref, guardar, dataCriacao)
                         VALUES ($id_medico, '". $ultima_referencia_inserida[0]->id_ref ."', $guardar, '". date('d-m-Y h:i:s') ."'  )");
	
	$minhas_receitas = $wpdb->get_results( "SELECT * FROM receitas_medicos WHERE id_med = $id_medico  ORDER BY id_receita DESC LIMIT 1 " );
	
	echo json_encode($minhas_receitas);
	
}

if($_POST['add_receita_medico_sem_paciente'] == 'ok'){
	
	$id_medico       = $_POST['id_medico'];
	//Inserir na tabela referencias_receita
	$data['id_med']       =  $id_medico;
	
	$wpdb->insert("referencia_receitas", $data);
	
	$ultima_referencia_inserida = $wpdb->get_results( "SELECT * FROM referencia_receitas WHERE id_ref = (SELECT MAX(id_ref) FROM referencia_receitas)" );
	
	//Inserir na tabela receitas_medicos
	$id_med      =  $_POST['id_medico'];;
	$dataCriacao =  date('d-m-Y h:i:s');
	$guardar      =  1;
	$id_ref       = $ultima_referencia_inserida[0]->id_ref;
	
	$wpdb->query("insert into receitas_medicos (id_med, id_ref, guardar, dataCriacao)
                         VALUES ($id_med, $id_ref, $guardar, '". $dataCriacao ."'  );");
	
	$minhas_receitas = $wpdb->get_results( "SELECT * FROM receitas_medicos WHERE id_med = $id_medico  ORDER BY id_receita DESC LIMIT 1 " );
	
	echo json_encode($minhas_receitas);
	
}

if($_POST['add_minha_receita'] == 'ok'){
	
	$arrayCepa = $_POST['arrayCepa'];
	
	foreach($arrayCepa as $key => $row){
		$data['id_receita_med'] = $arrayCepa[$key]['id_minha_receita'];
		$data['id_med']         = $arrayCepa[$key]['id_medico'];
		$data['id_pac']         = $arrayCepa[$key]['idpaciente'];
		$data['id_ref']         = $arrayCepa[$key]['id_ref'];
		$data['qtd_produto']    = $arrayCepa[$key]['qtdProdutoUnit'];
		$data['status']         = 0;
		$data['id_produto']     = $arrayCepa[$key]['idProduto'];
		
		$wpdb->insert("minhas_receitas", $data);
	}
	
	echo "done";
	
}

if($_POST['add_minha_receita_sem_paciente'] == 'ok'){
	
	$arrayCepa = $_POST['arrayCepa'];
	
	foreach($arrayCepa as $key => $row){
		$data['id_receita_med'] = $arrayCepa[$key]['id_minha_receita'];
		$data['id_med']         = $arrayCepa[$key]['id_medico'];
		$data['id_ref']         = $arrayCepa[$key]['id_ref'];
		$data['qtd_produto']    = $arrayCepa[$key]['qtdProdutoUnit'];
		$data['status']         = 0;
		$data['id_produto']     = $arrayCepa[$key]['idProduto'];
		
		$wpdb->insert("minhas_receitas", $data);
	}
	
	echo "done";
}

if($_POST['woocommerce_confirmacao'] == "ok"){
	
	$billing_first_name = strip_tags(filter_var ( $_POST['billing_first_name'], FILTER_SANITIZE_STRING));
	$billing_last_name  = strip_tags(filter_var ( $_POST['billing_last_name'], FILTER_SANITIZE_STRING));
	
	$cartao_nome         = strip_tags(filter_var ( $_POST['cartao_nome'], FILTER_SANITIZE_STRING));
	$billing_email      = strip_tags(filter_var ( $_POST['billing_email'], FILTER_SANITIZE_STRING));
	$billing_address_1  = strip_tags(filter_var ( $_POST['billing_address_1'], FILTER_SANITIZE_STRING));
	$billing_address_2  = strip_tags(filter_var ( $_POST['billing_first_name'], FILTER_SANITIZE_STRING));
	$billing_city       = strip_tags(filter_var ( $_POST['billing_city'], FILTER_SANITIZE_STRING));
	$billing_state      = strip_tags(filter_var ( $_POST['billing_state'], FILTER_SANITIZE_STRING));
	$billing_postcode   = strip_tags(filter_var ( $_POST['billing_postcode'], FILTER_SANITIZE_STRING));
	$billing_phone      = strip_tags(filter_var ( $_POST['billing_phone'], FILTER_SANITIZE_STRING));
	$billing_company    = strip_tags(filter_var ( $_POST['billing_company'], FILTER_SANITIZE_STRING));
	
	$cartao_credito     = strip_tags(filter_var ( $_POST['cartao_credito'], FILTER_SANITIZE_NUMBER_INT));
	$cartao_mes         = strip_tags(filter_var ( $_POST['cartao_mes'], FILTER_SANITIZE_NUMBER_INT));
	$cartao_ano         = strip_tags(filter_var ( $_POST['cartao_ano'], FILTER_SANITIZE_NUMBER_INT));
	$cartao_codigo      = strip_tags(filter_var ( $_POST['cartao_codigo'], FILTER_SANITIZE_NUMBER_INT));
	$cartao_bandeira    = strip_tags(filter_var ( $_POST['cartao_bandeira'], FILTER_SANITIZE_STRING));
	$itemNumero         = strip_tags(filter_var ( $_POST['itemNumero'], FILTER_SANITIZE_NUMBER_INT));
	
	$referenciaReceita  = strip_tags(filter_var ( $_POST['referenciaReceita'], FILTER_SANITIZE_NUMBER_INT));
	
	$cartaoData = $cartao_mes."/20".$cartao_ano;
	
	global $woocommerce;
	
	$address = array(
		'first_name' => ''.$billing_first_name.'',
		'last_name'  => ''.$billing_last_name.'',
		'company'    => '',
		'email'      => ''.$billing_email.'',
		'phone'      => ''.$billing_phone.'',
		'address_1'  => ''.$billing_address_1.'',
		'address_2'  => ''.$billing_address_2.'',
		'city'       => ''.$billing_city.'',
		'state'      => ''.$billing_state.'',
		'postcode'   => ''.$billing_postcode.'',
		'country'    => 'BRA'
	);
	
	// Now we create the order
	$order = wc_create_order();
	
	// The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
	for($i=1; $i <= $itemNumero; $i++){
		//Inseri os produtos na ordem de serviço
		$prodId  = strip_tags(filter_var ( $_POST['prodId_'.$i], FILTER_SANITIZE_NUMBER_INT));
		$prodQtd = strip_tags(filter_var ( $_POST['prodQtd_'.$i], FILTER_SANITIZE_NUMBER_INT));
		$order->add_product( get_product( ''.$prodId.'' ), ''.$prodQtd.'' ); // This is an existing SIMPLE product
	}
	
	$order->set_address( $address, 'billing' );
	//
	$order->calculate_totals();
	
	$total = $order->get_total();
	$total              = str_replace(",", "", $total);
	$total              = str_replace(".", "", $total);
	
	//Valor total tex
	$select_valor_referencia = $wpdb->get_results("select valor, frete from referencia_receitas where id_ref = $referenciaReceita");
	
	$valor_referencia = $select_valor_referencia[0]->valor;
	$frete_referencia = $select_valor_referencia[0]->frete;
	
	$totalComFrete          = $valor_referencia + $frete_referencia;
	
	//Mudar aqui
	$totalComFreteUltrapag = number_format($totalComFrete, 2, ',', '');
	$totalComFreteUltrapag  = str_replace(",", "", $totalComFreteUltrapag);

	update_post_meta( $order->ID, '_order_total', ''. $totalComFrete .'' );
	
	// Get the order key
	$order_key = get_post_meta( $order->ID, '_order_key', true);
	$returnURL = site_url().'/checkout/order-received/'.$order->ID.'/?key='.$order_key;
	
	$referencia = md5($order->ID);
	
	//Inseri os dados na API de pagamento Ultrapag
	$data = array (
		'cnpj' => '27124614000131',
		'chave' => '6cded98c-6165-4fd3-87c1-a1168146a388', // TOken teste
		//'chave' => 'beb7eff1-e1a4-4d8b-832e-2defb1ec7f26', //Token de produção
		'referencia' => ''.$referencia.'',
		'cliente' =>
			array (
				'nome' => ''. $cartao_nome .'',
				'email' => ''.$billing_email.'',
				'endereco' =>
					array (
						'rua' => ''.$billing_address_1.'',
						'numero' => '12',
						'complemento' => ''.$billing_address_2.'',
						'cep' => ''.$billing_postcode.'',
						'cidade' => ''.$billing_city.'',
						'estado' => ''.$billing_state.'',
						'pais' => 'BRA',
					),
			),
		'pagamento' =>
			array (
				'tipo' => 'credito',
				'valor' => $totalComFreteUltrapag,
				'moeda' => 'BRL',
				'pais' => 'BRA',
				'parcelas' => 1,
				'cartao' =>
					array (
						'cartao_numero' => ''.$cartao_credito.'',
						'cartao_nome' => ''.$cartao_nome.'',
						'data_expiracao' => ''.$cartaoData.'',
						'cvv' => ''.$cartao_codigo.'',
						'bandeira' => ''.$cartao_bandeira.'',
					),
			),
	);
	
	$json = json_encode($data);
	
	$url = "teste.ultrapag.com.br/v1/transacao";
	//$url = "https://ultrapag.com.br/v1/transacao";
	
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_POST, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $json);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($json)
	));
	
	$result = curl_exec($curl);
	
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	
	$decode = json_decode($result);
	
	if($httpcode == '200') {
		
		if($decode->pagamento->status == 1){
			
			//	$order->update_status("completed", 'Imported order', TRUE);
			
			$wpdb->query("UPDATE minhas_receitas set status = 1 where id_ref = $referenciaReceita ");
			update_post_meta( $order->ID, 'tid', ''. $decode->pagamento->tid .'' );
			update_post_meta( $order->ID, 'cartao_bandeira', ''. $cartao_bandeira .'' );
			update_post_meta( $order->ID, 'tipo_pagamento', ''. $decode->pagamento->tipo .'' );
			update_post_meta( $order->ID, 'nsu', ''. $decode->pagamento->nsu .'' );
			
			global $woocommerce;
			$woocommerce->cart->empty_cart();
			
		}else{
			$order->update_status("cancelled", 'Imported order', TRUE);
		}
		
		$arr = array(
			'status' => 1,
			'url' => $returnURL
		);
		curl_close($curl);
		echo json_encode($arr);
	}
	else {
		
		$arr = array(
			'status' => 0,
			'url' => $returnURL
		);
		
		curl_close($curl);
		//echo "<pre>";
		//print_r(json_decode($result));
		
		echo json_encode($decode);
	}
	
}

if($_POST['check_bandeira_cartao'] == "ok"){
	
	$cartao   = strip_tags(filter_var ( $_POST['bandeia_cartao'], FILTER_SANITIZE_NUMBER_INT));
	
	$bandeira = valida_cartao($cartao);
	
	echo json_encode($bandeira);
}

if($_POST['pega_cpf'] == "ok"){
	
	$cpf = $_POST['billing_cpf'];
	$selectCpf = $wpdb->get_results("select * from cadastros_pacientes where cpf_paciente = '". $cpf ."' ");
	
	//echo $selectCpf[0]->cpf_paciente;
	echo json_encode($selectCpf);
}

if($_POST['add_minha_receita_teste'] == 'ok'){
	
	$arrayCepa = $_POST['arrayCepa'];
	
	foreach($arrayCepa as $key => $row){
		$data['id_receita_med'] = $arrayCepa[$key]['id_minha_receita'];
		$data['id_med']         = $arrayCepa[$key]['id_medico'];
		$data['id_pac']         = $arrayCepa[$key]['idpaciente'];
		$data['id_ref']         = $arrayCepa[$key]['id_ref'];
		$data['qtd_produto']    = $arrayCepa[$key]['qtdProdutoUnit'];
		$data['status']         = 0;
		$data['id_produto']     = $arrayCepa[$key]['idProduto'];
		
		$wpdb->insert("minhas_receitas", $data);
	}
	
	echo "done";
	
}




/**
 * @author Felipe Braz
 * @website https://www.braz.pro.br/blog
 * @param int $cartao
 * @param int $cvc
 * @return array
 */
function valida_cartao($cartao, $cvc=false){
	$cartao = preg_replace("/[^0-9]/", "", $cartao);
	if($cvc) $cvc = preg_replace("/[^0-9]/", "", $cvc);
	
	$cartoes = array(
		'visa'		 => array('len' => array(13,16),    'cvc' => 3),
		'master' => array('len' => array(16),       'cvc' => 3),
		'diners'	 => array('len' => array(14,16),    'cvc' => 3),
		'elo'		 => array('len' => array(16),       'cvc' => 3),
		'amex'	 	 => array('len' => array(15),       'cvc' => 4),
		'discover'	 => array('len' => array(16),       'cvc' => 4),
		'aura'		 => array('len' => array(16),       'cvc' => 3),
		'jcb'		 => array('len' => array(16),       'cvc' => 3),
		'hipercard'  => array('len' => array(13,16,19), 'cvc' => 3),
	);
	
	switch($cartao){
		case (bool) preg_match('/^(636368|438935|504175|451416|636297)/', $cartao) :
			$bandeira = 'elo';
			break;
		
		case (bool) preg_match('/^(606282)/', $cartao) :
			$bandeira = 'hipercard';
			break;
		
		case (bool) preg_match('/^(5067|4576|4011)/', $cartao) :
			$bandeira = 'elo';
			break;
		
		case (bool) preg_match('/^(3841)/', $cartao) :
			$bandeira = 'hipercard';
			break;
		
		case (bool) preg_match('/^(6011)/', $cartao) :
			$bandeira = 'discover';
			break;
		
		case (bool) preg_match('/^(622)/', $cartao) :
			$bandeira = 'discover';
			break;
		
		case (bool) preg_match('/^(301|305)/', $cartao) :
			$bandeira = 'diners';
			break;
		
		case (bool) preg_match('/^(34|37)/', $cartao) :
			$bandeira = 'amex';
			break;
		
		case (bool) preg_match('/^(36,38)/', $cartao) :
			$bandeira = 'diners';
			break;
		
		case (bool) preg_match('/^(64,65)/', $cartao) :
			$bandeira = 'discover';
			break;
		
		case (bool) preg_match('/^(50)/', $cartao) :
			$bandeira = 'aura';
			break;
		
		case (bool) preg_match('/^(35)/', $cartao) :
			$bandeira = 'jcb';
			break;
		
		case (bool) preg_match('/^(60)/', $cartao) :
			$bandeira = 'hipercard';
			break;
		
		case (bool) preg_match('/^(4)/', $cartao) :
			$bandeira = 'visa';
			break;
		
		case (bool) preg_match('/^(5)/', $cartao) :
			$bandeira = 'master';
			break;
	}
	
	$dados_cartao = $cartoes[$bandeira];
	if(!is_array($dados_cartao)) return array(false, false, false);
	
	$valid     = true;
	$valid_cvc = false;
	
	if(!in_array(strlen($cartao), $dados_cartao['len'])) $valid = false;
	if($cvc AND strlen($cvc) <= $dados_cartao['cvc'] AND strlen($cvc) !=0) $valid_cvc = true;
	return array($bandeira, $valid, $valid_cvc);
}
