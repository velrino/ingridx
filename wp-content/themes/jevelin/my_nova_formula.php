<?php
/*
Template Name: Nova fórmula
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}
global $wpdb;
get_header();
$current_user = wp_get_current_user();
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<link rel="stylesheet" href="<?= get_site_url().'/wp-content/themes/jevelin/css/dataTables.bootstrap.css'; ?>" >


<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?= get_site_url().'/wp-content/themes/jevelin/js/bootstrap3-typeahead.js'?>"></script>


<script src="<?= get_site_url().'/wp-content/themes/jevelin/js/dataTables.bootstrap.js'; ?>"></script>


<style>
    .area-menu{
        background: #EAECED;
        text-align: left;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        padding: 0;
        font-size: 14px;
        font-weight: bolder;
        color: #3e3e3e;
        padding-top: 13px;
        height: 520px;
    }

    .area-menu li{
        display: block;
        padding: 1px 15px 5px 17px;

    }
    .area-menu li a{
        cursor: pointer;
    }
    .area-menu li a:hover{
        cursor: pointer;
        color: #337ab7 !important;
    }
    .area-menu-footer{
        background: #334C5A;
        padding: 7px 0 0 0;
        position: absolute;
        bottom: 11px;
        width: 100%;

    }

    .area-menu-footer li{
        color: #FFFFFF;
        font-size: 12px;
        padding: 1px 15px 5px 17px;
        text-decoration: underline;
    }

    .col-md-3{
        padding: 0 !important;
    }

    .area-content-list{
        margin-top:20px;
    }

    .area-content-all{
        border: 1px solid #8BB5CF;
        background: #FFFFFF;
        -webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        -moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        height: 520px;
        padding: 0;
    }

    .area-content-alert{
        background: #8BB4D0;
        color: #FFFFFF;
        font-weight: bolder;
        text-align: left;
        padding-top: 16px;
        font-size: 13px;
        color: #FFFFFF;
    }
    .area-content-alert .busca_paciente{
        height: auto;
        background: #BCD1DE;
        color: #555555;
        margin-left: 10px;
        margin-top: -5px;
    }

    .area-content-alert div input,.area-content-alert div input:focus{
        margin: 0;
        padding: 0 !important;
        height: 25px;
        width: 360px;
        background: transparent;
        outline: none;
        border: none;
        color: #555555;
        padding-left: 5px !important;
        text-transform: uppercase;
    }

    .area-content-alert #cpf_paciente{
        width: 200px;
        height: 25px;
        background: #BCD1DE;
        color: #555555;
        margin-left: 10px;
        margin-top: -5px;
        padding: 4px;
    }

    .area-content-alert ul{
        position: absolute;
        z-index: 999;
        background: #fff;
        padding: 0;
        left: 87px;
        top: 20px;
    }

    .area-content-alert ul li{
        padding: 10px 20px 10px 20px;
        color: #555555;
        list-style-type: none;
        display: block;
        cursor: pointer;
    }

    .area-content-alert ul li:hover{
        background: #73818a;
        color: #FFFFFF;
    }

    .area-content-alert h2{
        color: #FFFFFF;
        font-size: 25px;
        padding: 5px;
    }
    .div-list{
        padding: 5px;
        font-size: 13px;

    }
    .div-list p{
        margin: 0;
        text-align: left;
        color: #555555 ;
    }

    .pagamento{
        height: 120px;
        border: 1px solid #82AC63;
        margin-bottom: 25px;
        padding: 0 6px 0 6px;
    }

    .centered {
        margin: 0 auto !important;
        float: none !important;
    }
    .no-padding{
        padding: 0;
    }
    .btn-ingridix-cli{
        width: 27.4%;
        height: 50px;
        padding: 12px 10px 8px 10px;
        border-radius: 40px;
        background: #79B8DB;
        color: #FFFFFF;
        font-weight: bolder;
        font-size: 17px;
        margin-bottom: 15px;
        box-shadow: inset 0px 0px 30px 10px #54a1cc;
    }
    .btn-ingridix{
        height: 35px;
        padding: 9px 10px 8px 10px;
        border-radius: 40px;
        background: #ECBB71;
        color: #FFFFFF;
        font-weight: bolder;
        font-size: 13px;
        margin-bottom: 5px;
    }
    .nome-logado{
        text-align: left;
        color: #3C505D;
        margin-bottom: 15px;
    }
    .nome-logado a, .nome-logado a:hover{
        color: #3C505D;
    }
    #wrapper > .sh-page-layout-default{
        padding: 30px 0 ;
    }

    label{
        color: #FFFFFF;
        margin-right: 15px;
    }
    .imprimir-color{
        background: #00664C;
        height: 50px;
    }
    .enviar-email-color{
        background: #8C2300;
    }
    .efetuar-color{
        background: #5C8F34;
    }
    .list-receita li{
        color: #555555;
        font-size: 12px;
        margin-bottom: 5px;
    }

    .block-cepa-content{
        background: #73818A;
        margin: 0 0 5px 0;
        padding: 4px;
    }
    .content-count{
        font-weight: bolder;
        line-height: 16px;
        margin-top: 6px;
        font-size: 11px;
        color: #FFFFFF;
        padding: 0;
    }
    .square-count{
        background: #FFFFFF;
        font-size: 26px;
        margin: 0;
        padding: 0;
        color: #555555;
    }
    .receitas{
        padding-left: 0;
        margin-top: 10px;
        text-align: left;

    }
    .receitas a, .receitas a:hover{
        color: #555555;
        text-decoration: none;
        font-size: 13px;
        font-weight: bolder;
    }
    .ingrdientes-list{

        color: #292929;
    }
    .salvar-receita{
        cursor: pointer;
    }
    /**************************/
    input[type="text"]{
        padding: 0 !important;
    }
    .count-input {
        position: relative;
        width: 65px;
    }
    .count-input span{
        position: relative;
        top: -6px;
    }
    .count-input input {
        width: 25px;
        height: 20px;
        border-radius: 2px;
        text-align: center;
        color: #000000;
        background: #DBE7EE;
        margin-left: 20px;
    }
    .count-input input:focus {
        outline: none;
    }
    .count-input .incr-btn {
        display: block;
        position: absolute;
        width: 20px;
        height: 20px;
        font-size: 20px;
        font-weight: 300;
        text-align: center;
        line-height: 30px;
        top: 15px;
        right: 0;
        margin-top: -15px;
        text-decoration:none;
        background: #8BB5D0;
        cursor: pointer;
    }
    .count-input .incr-btn:first-child {
        right: auto;
        left: 0;
    }
    .count-input.count-input-sm {
        max-width: 125px;
    }
    .count-input.count-input-sm input {
        height: 36px;
    }
    .count-input.count-input-lg {
        max-width: 200px;
    }
    .count-input.count-input-lg input {
        height: 70px;
        border-radius: 3px;
    }

    .ingrdientes-list div:nth-child(2){
        text-align: left;
        padding-left: 10px;
    }

    .area-menu li a, .area-menu li a:hover{
        color: #3e3e3e;
        text-align: none;
    }

    .area-menu-footer li a, .area-menu-footer li a:hover{
        color: #FFFFFF !important;
        text-align: none;
    }

    .active{
        color: #337ab7 !important;
    }

    .inline-flex{
        display: inline-flex;
    }

    .hidden{
        display: none;
    }

    .limpar-receita{
        cursor: pointer;
    }

    .cepa_item{
        font-size: 14px
    }

    table td{
        padding: 2px 1px 0px 10px !important;
        border: none !important;
    }

    #dinamico_filter label{
        color: #3e3e3e;
    }

    @media (min-width: 768px) {

        #dinamico_wrapper .row .col-sm-6:nth-child(1){
            width: 0 !important;
        }

        #dinamico_wrapper .row .col-sm-6:nth-child(2){
            width: 100% !important;;
        }

        #dinamico_wrapper .row:nth-child(2) .col-sm-12{
            padding-right: 0 !important;
        }

        .form-control.input-sm{
            width: 100% !important;
        }

        #dinamico_filter label {
            color: #3e3e3e;
            width: 70%;
        }
    }

    .tooltip-inner p{
        color: #fff !important;
        padding: 10px 10px 0 10px !important;
        font-size: 14px !important;
        line-height: 20px;
    }

    .tooltip-inner p:last-child{
        color: #fff !important;
        padding: 3px 10px 10px 10px !important;
    }
    .icon-info{
        cursor: pointer;
    }
    
    #dinamico_filter{
        display: none;
    }

    /* Breakpoints */
    @media only screen and (min-width: 320px) and (max-width: 620px) {

        .col-md-9{
            padding: 0 5px 0 7px;
        }

        .area-content-all{
            height: auto;
        }

        .area-content-list{
        
        }
        .div-list {
            padding: 0 !important;
        }

        .td_content2 {
            background-color: #F5F5F5;
            padding: 6px;
            color: #56A235;
            font-size: 10px;
        }

        .area-menu{
            padding-bottom: 20px;
            height: auto;
        }
        .nome-logado{
            display: inline-block;
        }

        .btn-ingridix-cli{
            width: 100% !important;
        }
        .btn-ingridix-cli2{
            margin-bottom: 20px;
        }

        .centered{
            text-align: left;
        }

        .cpf-span1{
            padding-right: 85px !important;
        }

        .cpf-span2{
            width: 92% !important;
            padding-right: 94px !important;
        }

        .p-hidden1{
            display: inline;
        }
        .p-hidden2{
            display: none;
        }
        .p-hidden3{
            display: none;
        }

        .div-list {
            margin-bottom: 30px;
            margin-top: 20px;
        }

        .area-content-alert{
            padding-bottom: 1px;
        }

        .area-content-alert div input{
            width: 163px !important;
        }

        .square-count{
            text-align: center;
        }

        .salvar-receita, .limpar-receita{
            text-align: center;
        }

        .imprimir-color{
            height: auto;
        }
        
        .content-btn{
            margin-top: 20px !important;
        }
    }

    @media only screen and (min-width: 621px) and (max-width: 940px) {

        .td_content2 {
            background-color: #F5F5F5;
            padding: 6px;
            color: #56A235;
            font-size: 10px;
        }

        .area-menu{
            padding-bottom: 20px;
            height: auto;
        }

        .btn-ingridix-cli{
            width: 100% !important;
        }

        .form-cpf{
            margin-top: 15px;
        }

        .error{
            width: 100%;
        }

        .area-content-all{
            width: 100%;
            height: auto;
        }

        .p-hidden1{
            display: none;
        }
        .p-hidden2{
            display: inline;
        }
        .p-hidden3{
            display: none;
        }

        .div-list {
            margin-bottom: 30px;
            margin-top: 20px;
        }

        .cpf-span2{
            width: 92% !important;
            padding-right: 207px !important;
        }

        .area-content-alert{
            padding-bottom: 1px;
        }

        .area-content-alert .busca_paciente{
            width: 313px;
        }
        .area-content-alert #cpf_paciente{
            width: 350px;
        }
        .square-count{
            text-align: center;
        }
        .content-btn{
            margin-top: 20px !important;
        }
        
        .nome-logado{
            margin-bottom: 0;
        }
    }

    @media only screen and (min-width: 941px) and (max-width: 1260px) {
        .p-hidden1{
            display: none;
            text-align: right;
        }
        .p-hidden2{
            display: none;
            text-align: right;
        }
        .p-hidden3{
            display: inline;
            text-align: right;
        }
    }

    @media only screen and (min-width: 1261px) {
        .p-hidden1{
            display: none;
            text-align: right;
        }
        .p-hidden2{
            display: none;
            text-align: right;
        }
        .p-hidden3{
            display: inline;
            text-align: right;
        }
    }
</style>
<center>
    <div class="container">

        <div class="row">

            <div class="col-md-11 centered">

                <div class="col-md-11 no-padding">
                    <div class="button button-primary button-large btn-ingridix-cli pull-left" style="width: 27.4%">INGRIDIX LAB</div>
                </div>
                <div class="col-md-3 nome-logado">
                    Olá <b> <?= $current_user->user_login; ?></b>
                </div>
                <div class="col-md-8 nome-logado">
                    <b><a >&nbsp;&nbsp;&nbsp;</a></b>
                </div>
                <div class="col-md-3">
                    <ul class="area-menu list-unstyled">
                        <li><a href="<?= get_site_url().'/cadastrar-novo-paciente'?>">CADASTAR NOVO PACIENTE</a></li>
                        <li><a class="active" data-toggle="modal" data-target="#novareceitaModal" data-whatever="@mdo">CRIAR NOVA FÓRMULA</a></li>
                        <li><a href="<?= get_site_url().'/area-medico'?>">RECEITAS ANTERIORES</a></li>
                        <li><a href="http://ingridix.com.br/duvidas-sobre-o-uso-profissionais" target="_blank">DÚVIDAS SOBRE O USO</a></li>
                        <li><a href="<?= get_site_url().'/minhas-receitas'?>">MINHAS RECEITAS</a></li>
                        <li><a href="http://ingridix.com.br/tabela-de-cepas-e-sintomas" target="_blank">TABELA DE CEPAS E SINTOMAS</a></li>
                        <li><br></li>

                        <div class="area-menu-footer">
                            <li><a href="<?= get_site_url()?>/meu-cadastro-medico">MEUS DADOS DE CADASTRO</a></li>
                            <li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
                        </div>
                    </ul>
                </div>

                <div class="col-md-9 area-content">
                    <div class="col-md-12 area-content-all pull-right">
                        <div class="col-md-12 area-content-alert">
                            <div class="form-group col-md-7 inline-flex">
                                PACIENTE <div id="busca_paciente" class="busca_paciente"><input id="get_paciente" type="text" value=""></div>
                                <ul class="hidden">

                                </ul>
                            </div>
                            <div class="form-group col-md-4 inline-flex">
                                CPF <div id="cpf_paciente" ></div>
                            </div>
                        </div>

                        <div class="col-md-12 area-content-list">
                            <div class="col-md-3 " style="padding-left: 0">
                                <div class="col-md-12 " style="padding-left: 0">
                                    <div class="col-md-12 block-cepa-content">
                                        <div class="col-md-9 content-count">CONTAGEM DE UFC POR BILHÃO</div>
                                        <div class="col-md-3 square-count">0</div>
                                    </div>
                                </div>
                                <div class="col-md-12 content-btn" style="padding-left: 0; margin-top: 30px">
                                    <div class="button button-primary button-large btn-ingridix imprimir-color salvar-receita">ENVIAR RECEITA AO PACIENTE</div>
                                    <div class="button button-primary button-large btn-ingridix enviar-email-color limpar-receita">LIMPAR RECEITA</div>
                                </div>

                                <div class="col-md-12" style="padding-left: 0;">
                                    <div class="form-group">
                                        <input type="checkbox" id="guardarReceita" name="guardarReceita" aria-label="..."> <b>Salvar receita no arquivo</b>
                                    </div>
                                </div>

                                <div class="col-md-12 receitas" >
                                    <div class="col-md-12 " style="margin-bottom: 5px;">
                                        <a class="active" href="<?= get_site_url().'/nova-formula';?>">RECEITA POR CEPA</a>
                                    </div>
                                    <div class="col-md-12" >
                                        <a href="<?= get_site_url().'/nova-formula-dinamica';?>">RECEITA POR SINTOMA</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 div-list" style="margin-left: 5px; padding-top: 0">

                                <div class="col-md-12 pull-right no-padding">

                                    <div class="col-md-12 no-padding">

                                        <table id="dinamico" class="table" cellspacing="0" width="100%" style="border: none !important;margin-top: 0 !important;">
                                            <thead style="display: none">
                                            <tr>
                                                <th>Name</th>
                                                <th>Position</th>
                                                <th> </th>
                                            </tr>
                                            </thead>
                                            <tbody>
											
											<?php
											
											$args = array( 'post_type' => 'product', 'posts_per_page' => 100, 'offset'=> 0);
											$produtos = get_posts( $args );
											
											$qtd = 0;
											foreach($produtos as $row):
												$qtd++;
												$sintomas = get_post_meta(  $row->ID , 'sintomas', true );
												?>

                                                <tr >
                                                    <td width="10%">
                                                        <div class="col-md-3 count-input space-bottom pull-left ">
                                                            <a class="incr-btn" data-action="decrease"  onclick="btnMenos(this, 'incr-btn');"><span>-</span></a>
                                                            <input class="quantity" id="quantity_<?= $row->ID; ?>" data-cepa-id="<?= $row->ID?>" type="text" name="quantity" value="0" onkeyup="inserirNoInput(id)" />
                                                            <a class="incr-btn" data-action="increase" onclick="btnMais(this, 'incr-btn');"><span>+</span></a>
                                                        </div>
                                                    </td>
                                                    <td width="90%" data-teste="<?= $row->ID; ?>" data-search="<?php echo $sintomas; ?>">
                                                        <div class="col-md-12 no-padding cepa_item" ><?= $row->post_title; ?></div>
                                                    </td>
                                                    <td width="5%">
                                                        <i class="icon-info sh-alert-icon" aria-hidden="true" data-toggle="tooltip" data-placement="left" data-html="true" title='<?= $row->post_content; ?>' style="margin-top: -5px;"></i>

                                                    </td>
                                                </tr>
												
												<?php
											endforeach;
											?>

                                            </tbody>
                                        </table>

                                        <div class="hidden_qtd">
											<?php
											
											foreach($produtos as $row):
												?>
                                                <input type="hidden" class="count_qtd" data-cepa-id="<?= $row->ID; ?>" id="id_<?= $row->ID; ?>" value="0">
												<?php
											endforeach;
											?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</center>

<?php
get_footer();
?>

<script>


    function inserirNoInput(id){

        var val = $("#" + id).val();
        var id = $("#" + id).attr("data-cepa-id")

        //Inseri o calor no input hidden
        $("#id_" + id).val(val);

        var qtd = 0;
        $(".hidden_qtd").find("input").each(function(){
            qtd += parseFloat($(this).val());
        });

        $(".square-count").text(qtd);
    }

    function btnMais(obj, btnMais){
        var $button = $(obj);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');

        //Aumentar quantidade de cfu
        var qtdcfu = $(".square-count").text();
        var cepa_id = $button.parent().find('.quantity').attr("data-cepa-id");

        if ($button.data('action') == "increase") {

            var newVal = parseFloat(oldValue) + 1;
            //Add mais 1 ao contador de cfu
            var newCfu = parseFloat(qtdcfu) + 1;

            //Pega todas os itens selecionados para inserir no banco
            var qtd = 1;
            $(".hidden_qtd").find("input").each(function(){
                qtd += parseFloat($(this).val());
            });

            if(qtd >= 201){
                // alert("Aténção! 200 é o numero máximo de cepas celecionáveis.")
            }else{
                if(qtd >= 0){
                    $("#id_"+cepa_id).val(newVal);
                    $button.parent().find('.quantity').val(newVal);
                    $(".square-count").text(qtd);
                }
            }
        }
    }

    function btnMenos(obj, btnMenos){
        var $button = $(obj);
        var oldValue = $button.parent().find('.quantity').val();
        $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');

        //Aumentar quantidade de cfu
        var qtdcfu = $(".square-count").text();
        var cepa_id = $button.parent().find('.quantity').attr("data-cepa-id");

        //Remove 1 ao contador de cfu
        if (oldValue > 0) {
            var newCfu = parseFloat(qtdcfu) - 1;

            //Pega todas os itens selecionados para inserir no banco
            var qtd = -1;
            $(".hidden_qtd").find("input").each(function(){
                qtd += parseFloat($(this).val());
            });

            if(newCfu >= 0){
                $(".square-count").text(qtd);
            }
        }
        // Don't allow decrementing below 1
        if (oldValue > 0) {
            var newVal = parseFloat(oldValue) - 1;
        } else {
            newVal = 0;
            $button.addClass('inactive');
        }
        $("#id_"+cepa_id).val(newVal);
        $button.parent().find('.quantity').val(newVal);

    }

    $(function(){

        var dataSrc = [];

        $('#dinamico').DataTable({
            "bLengthChange": false,
            oLanguage: {
                sSearch: "Filtrar por sintomas",
                oPaginate: {
                    "sNext": '<i class="fa fa-chevron-right" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                }
            },
            "bInfo" : false,
            'initComplete': function(){
                var api = this.api();

                // Initialize Typeahead plug-in
                $('.dataTables_filter input[type="search"]', api.table().container())
                    .typeahead({
                            source: function(query, process){
                                $.getJSON('<?= get_site_url()."/functions-ajax?selecionar_sintomas=ok";?>', { q: query }, function (data){
                                    return process(data);
                                });
                            },
                            afterSelect: function(value){
                                api.search(value).draw();
                            }
                        }
                    );
            }
        });

        $('[data-toggle="tooltip"]').tooltip();
        setInterval(function(){
            $('[data-toggle="tooltip"]').tooltip();
        },1000);
        
        $('.quantity').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

        $(".salvar-receita").click(function(){

            var idpaciente = $("#get_paciente").attr('data-id-paciente');
            var user_id = <?= $current_user->ID ;?>;
            var id_minha_receita, id_ref;

            var guardarReceita = $("#guardarReceita");

            if (guardarReceita.is(":checked"))
            {
                guardarReceita = 1;
            }else{
                guardarReceita = 0;
            }

            if(guardarReceita == 1 && $("#cpf_paciente").text().length <= 0 ){
                
                if($(".square-count").text() > 200){
                    alert("Selecione no máximo 200 bilhões de CFU's!");
                }else if($(".square-count").text() < 1) {
                    alert("Selecione no mínimo 1 bilhão de CFU's!");
                }else{
                    $("#id_loaderajax").removeClass("hidden");
                    $.ajax({
                        url: '<?= get_site_url()."/functions-ajax";?>',
                        type: "post",
                        data: {id_medico : user_id, guardar : guardarReceita, add_receita_medico_sem_paciente : "ok"},
                        dataType: "json",
                        success: function(data1){

                            id_ref           = data1[0].id_ref;
                            user_id          = data1[0].id_med;
                            id_minha_receita = data1[0].id_receita;
                            var arr = [];

                            $(".hidden_qtd input").each(function(){
                                var qtdProdutoUnit = parseFloat($(this).val());
                                var idProdutoCepa  = $(this).attr("data-cepa-id");

                                if( qtdProdutoUnit > 0){

                                    var obj = {'id_medico' : user_id,
                                        'id_minha_receita' : id_minha_receita,
                                        'idpaciente' : idpaciente,
                                        'id_ref' : id_ref,
                                        'idProduto' : idProdutoCepa,
                                        'qtdProdutoUnit' : qtdProdutoUnit };
                                    arr.push(obj);
                                }
                            });

                            //Envia para função ajax
                            $.ajax({
                                url: '<?= get_site_url()."/functions-ajax";?>',
                                type: "post",
                                data: { arrayCepa: arr , add_minha_receita_sem_paciente : "ok"},

                                success: function(data){

                                    window.location.href="<?= get_site_url().'/receita-salva/?idr='?>"+ id_minha_receita +"&idrr="+ id_ref +" ";
                                },
                                error: function(){
                                    alert('errorr')
                                }
                            });
                           
                        },
                        error: function(){
                            //  alert('errorr')
                        }
                    });
                }


            } else
            if(guardarReceita == 1 && $("#cpf_paciente").text().length > 0 ){
                
                if($("#cpf_paciente").text().length <= 0){
                    alert("Você deve selecionar um paciente antes de seguir.");
                }else if($(".square-count").text() > 200){
                    alert("Selecione no máximo 200 bilhões de CFU's!");
                }else if($(".square-count").text() <= 0 ){
                    alert("Selecione os itens para salvar!");
                }else if($(".square-count").text() < 1){
                    alert("Selecione no mínimo 1 bilhão de CFU's!");
                }else{
                    $("#id_loaderajax").removeClass("hidden");
                    //Envia para função ajax, adiciona nova receita ao medico
                    $.ajax({
                        url: '<?= get_site_url()."/functions-ajax";?>',
                        type: "post",
                        data: {id_medico : user_id, idpaciente : idpaciente, guardar : guardarReceita, add_receita_medico : "ok"},
                        dataType: "json",
                        success: function(data){

                            id_minha_receita = data[0].id_receita;
                            id_ref           = data[0].id_ref;
                            var arr = [];
                            
                            $(".hidden_qtd input").each(function(){
                                var qtdProdutoUnit = parseFloat($(this).val());
                                var idProdutoCepa  = $(this).attr("data-cepa-id");
                                
                                if( qtdProdutoUnit > 0){
                                    var obj = {'id_medico' : user_id,
                                        'id_minha_receita' : id_minha_receita,
                                        'idpaciente' : idpaciente,
                                        'id_ref' : id_ref,
                                        'idProduto' : idProdutoCepa,
                                        'qtdProdutoUnit' : qtdProdutoUnit };
                                    arr.push(obj);
                                }
                            });
                            
                            //Envia para função ajax
                            $.ajax({
                                url: '<?= get_site_url()."/functions-ajax";?>',
                                type: "post",
                                data: { arrayCepa: arr , add_minha_receita : "ok"},

                                success: function(data){

                                    window.location.href="<?= get_site_url().'/receita-salva/?idr='?>"+ id_minha_receita + "&idrr=" + id_ref + "&idp="+ idpaciente +"&complete=true ";

                                },
                                error: function(){
                                    alert('errorr')
                                }
                            });
                        },
                        error: function(){
                         //   alert('Error minha receita!')
                        }
                    });
                }

            } else
            if(guardarReceita == 0 ){
                
                if($("#cpf_paciente").text().length <= 0){
                    alert("Você deve selecionar um paciente antes de seguir.");
                }else if($(".square-count").text() > 200){
                    alert("Selecione no máximo 200 bilhões de CFU's!");
                }else if($(".square-count").text() <= 0 ){
                    alert("Selecione os itens para salvar!");
                }else if($(".square-count").text() < 1){
                    alert("Selecione no mínimo 1 bilhão de CFU's!");
                }else{
                    $("#id_loaderajax").removeClass("hidden");
                    //Envia para função ajax, adiciona nova receita ao medico
                    $.ajax({
                        url: '<?= get_site_url()."/functions-ajax";?>',
                        type: "post",
                        data: {id_medico : user_id, idpaciente : idpaciente, guardar : guardarReceita, add_receita_medico : "ok"},
                        dataType: "json",
                        success: function(data){

                            id_minha_receita = data[0].id_receita;
                            id_ref           = data[0].id_ref;
                            var arr = [];
                            
                            $(".hidden_qtd input").each(function(){
                                var qtdProdutoUnit = parseFloat($(this).val());
                                var idProdutoCepa  = $(this).attr("data-cepa-id");

                                if( qtdProdutoUnit > 0){
                                    var obj = {'id_medico' : user_id,
                                        'id_minha_receita' : id_minha_receita,
                                        'idpaciente' : idpaciente,
                                        'id_ref' : id_ref,
                                        'idProduto' : idProdutoCepa,
                                        'qtdProdutoUnit' : qtdProdutoUnit };
                                    arr.push(obj);
                                }
                            });

                            //Envia para função ajax
                            $.ajax({
                                url: '<?= get_site_url()."/functions-ajax";?>',
                                type: "post",
                                data: { arrayCepa: arr , add_minha_receita : "ok"},

                                success: function(data){

                                    window.location.href="<?= get_site_url().'/receita-salva/?idr='?>"+ id_minha_receita + "&idrr=" + id_ref + "&idp="+ idpaciente +"&complete=true ";

                                },
                                error: function(){
                                    alert('errorr')
                                }
                            });
                            
                            
                            
                            
                        },
                        error: function(){
                        //    alert('Error minha receita!')
                        }
                    });
                }

            }
        });

        $(".limpar-receita").click(function(){
            $('.div-list input:not(".input-sm")').val(0);
            $("#get_paciente").val('');
            $("#cpf_paciente").text('');
            $(".square-count").text(0);
        });

        $("#get_paciente").keyup(function(){

            var input = $(this).val();

            $.ajax({
                url: '<?= get_site_url()."/functions-ajax";?>',
                type: "post",
                data: {nome : input, selecionar_paciente : "ok"},
                dataType: 'json',
                success: function(data){

                    $("#get_paciente").attr('value', input);

                    if(data.length >= 1){

                        $(".area-content-alert ul li").remove();

                        for(var i = 0; i < data.length; i++){

                            $('<li id="pacienteId-'+data[i].id_paciente+'" data-id-paciente="'+data[i].id_paciente+'" data-nome-paciente="'+data[i].nome_paciente+'" data-cpf-paciente="'+data[i].cpf_paciente+'" onclick="nome_paciente(id)" >'+data[i].nome_paciente+'</li>').appendTo(".area-content-alert ul")
                            $(".area-content-alert ul").removeClass('hidden');
                        }

                    }else{
                        $("#cpf_paciente").text('');
                        $(".area-content-alert ul").addClass('hidden');
                        $(".area-content-alert ul li").remove();
                    }

                    if(input.length <= 0){
                        $("#cpf_paciente").text('');
                        $(".area-content-alert ul").addClass('hidden');
                        $(".area-content-alert ul li").remove();
                    }

                }
            })
            return false;
        });

    })

    function nome_paciente(id){

        var nome = $("#"+id).attr('data-nome-paciente');
        var cpf = $("#"+id).attr('data-cpf-paciente');
        var idpaciente = $("#"+id).attr('data-id-paciente');

        $("#get_paciente").val(nome).attr('data-id-paciente', idpaciente);
        $("#cpf_paciente").text(cpf);
        $(".area-content-alert ul").addClass('hidden');
        $(".area-content-alert ul li").remove();
    }
</script>

