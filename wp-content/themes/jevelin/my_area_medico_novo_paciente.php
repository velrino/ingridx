<?php
/*
Template Name: Área médico cadastrar paciente
*/
if(!is_user_logged_in()){
	
	wp_redirect( get_site_url()."/acesso-cliente/" );
}

get_header();
$current_user = wp_get_current_user();
global $DATAUSER;
?>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<style>
		.area-menu{
			background: #EAECED;
			text-align: left;
			border-top-left-radius: 20px;
			border-top-right-radius: 20px;
			padding: 0;
			font-size: 14px;
			font-weight: bolder;
			color: #3e3e3e;
			padding-top: 13px;
			height: 350px;
		}
		
		.area-menu li{
			display: block;
			padding: 1px 15px 5px 17px;
			
		}
        .area-menu li a{
            cursor: pointer;
        }
        .area-menu li a:hover{
            cursor: pointer;
            color: #337ab7 !important;
        }
		.area-menu-footer{
			background: #334C5B;
			padding: 7px 0 0 0;
			position: absolute;
			bottom: 11px;
			width: 100%;
			
		}

        .area-menu-footer li, .area-menu-footer li a, .area-menu-footer li a:hover{
            color: #FFFFFF !important;
            font-size: 12px;
            text-decoration: underline;
        }
		
		.col-md-3{
			padding: 0 !important;
		}
		
		.area-content-list{
		
		}
		
		.area-content-all{
			border: 1px solid #7AB8DA;
			background: #FFFFFF;
			-webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			-moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			height: 350px;
			padding-top: 15px
		}
		
		.area-content-alert{
			background: #A5C48E;
			
		}
		.area-content-alert h2{
			color: #FFFFFF;
			font-size: 25px;
			padding: 10px;
		}
		.div-list{
			
			font-size: 13px;
		}
		.div-list a, .div-list a:hover{
			text-decoration: none;
			color: #3f3f3f;
		}
		.div-list h5{
			font-size: 16px;
			text-align: left;
		}
		.centered {
			margin: 0 auto !important;
			float: none !important;
		}
		.no-padding{
			padding: 0;
		}
		.btn-ingridix-cli{
			width: 27.4%;
			height: 50px;
			padding: 12px 10px 8px 10px;
			border-radius: 40px;
			background: #79B8DB;
			color: #FFFFFF;
			font-weight: bolder;
			font-size: 17px;
			margin-bottom: 15px;
            cursor: pointer;
            box-shadow: inset 0px 0px 30px 10px #54a1cc;
		}
		
		.btn-ingridix-cli2{
            height: 50px;
            padding: 12px 35px 12px 35px;
            border-radius: 40px;
            background: #364A56;
            color: #FFFFFF !important;
            font-weight: bolder;
            font-size: 13px;
            margin-top: -20px;
		}
		.nome-logado{
			text-align: left;
			color: #3C4F5C;
			margin-bottom: 15px;
		}
		.nome-logado a, .nome-logado a:hover{
			color: #3C4F5C;
			text-decoration: none;
		}
		#wrapper > .sh-page-layout-default{
			padding: 30px 0 ;
		}
		.list-receita li{
			background: #F5F5F5;
			display: block;
			padding: 4px;
			margin-bottom: 4px;
			text-align: left;
			
		}
		.list-receita li a, .list-receita li a:hover{
			color: #3e3e3ee6;
			font-size: 13px;
			font-weight: bolder;
		}
		
		
		
		.form-group{
			margin-bottom: 0 !important;
		}
		.form-group input, .form-group select{
			background: rgba(240, 240, 240, 0.83);
			height: 40px;
			
		}
		
		.no-padding{
			padding: 0;
		}
		
		.clique_aqui{
			margin-top: 5px;
			font-size: 14px;
			color: #876A42;
		}
		
		label{
			float: left;
		}
		
		a:hover{
		
			color: #FFFFFF;
		}

        .area-menu a, .area-menu a:hover{
            color: #3e3e3e;
            text-align: none;
            
        }

        .active{
            color: #337ab7 !important;
        }
        .error{
            color: #d60707;
            font-size: 13px;
        }
        .form-control.error{
            color: #555;
            font-size: 14px;
        }

        /* Breakpoints */
        @media only screen and (min-width: 320px) and (max-width: 620px) {

            .col-md-9{
                padding: 0 5px 0 7px;
            }

            .area-content-all{
                height: auto;
            }

            .area-content-list{
                padding: 0;
            }
            .div-list {
                padding: 0 !important;
            }

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 10px;
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }
            .nome-logado{
                display: inline-block;
            }

            .btn-ingridix-cli{
                width: 100% !important;
            }
            .btn-ingridix-cli2{
                margin-bottom: 20px;
            }

            .centered{
                text-align: left;
            }
            
            .form-cpf{
                margin-top: 15px;
            }

            .error{
                width: 100%;
            }
        }

        @media only screen and (min-width: 621px) and (max-width: 940px) {

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 10px;
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }

            .nome-logado{
                text-align: right;
            }

            .btn-ingridix-cli{
                width: 100% !important;
            }

            .form-cpf{
                margin-top: 15px;
            }

            .error{
                width: 100%;
            }

            .area-content-all{
                width: 100%;
            }
        }

        @media only screen and (min-width: 941px) and (max-width: 1260px) {

        }

        @media only screen and (min-width: 1261px) {

        }
	</style>
	<center>
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-11 centered">
					<?php
					if(isset($DATAUSER['error_cpf'])):
						?>
                        <div class="col-md-12">
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <i class="fa fa-close"></i>
								<?php echo $DATAUSER['error_cpf']?>
                            </div>
                        </div>
						
						<?php
					endif;
					
					if(isset($DATAUSER['sucesso_amed'])):
						?>
                        <div class="col-md-12">
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
								<?php echo $DATAUSER['sucesso_amed']?>
                            </div>
                        </div>
						
						<?php
					endif;
					?>
                    <div class="col-md-11 no-padding">
                        <div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área médico" onclick='location.href="<?= get_site_url().'/area-medico'?>"'>INGRIDIX LAB</div>
                    </div>
					<div class="col-md-3 nome-logado pull-left">
						Olá <b> <?= $current_user->user_login; ?></b>
					</div>
					<div class="col-md-8 nome-logado">
						<b>PACIENTES</b>
					</div>
					<div class="col-md-3">
						<ul class="area-menu list-unstyled">
                            <li><a class="active" href="<?= get_site_url().'/cadastrar-novo-paciente'?>">CADASTAR NOVO PACIENTE</a></li>
                            <li><a data-toggle="modal" data-target="#novareceitaModal" data-whatever="@mdo">CRIAR NOVA FÓRMULA</a></li>
                            <li><a href="<?= get_site_url().'/area-medico'?>">RECEITAS ANTERIORES</a></li>
							<li><a href="http://ingridix.com.br/tabela-de-cepas-e-sintomas" target="_blank">DÚVIDAS SOBRE AS CEPAS</a></li>
                            <li><a href="<?= get_site_url().'/minhas-receitas'?>">MINHAS RECEITAS</a></li>
							<li><br></li>
							
							
							<div class="area-menu-footer">
                                <li><a href="<?= get_site_url()?>/meu-cadastro-medico">MEUS DADOS DE CADASTRO</a></li>
                                <li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
							</div>
						</ul>
					</div>
					
					<div class="col-md-9 area-content">
						<div class="col-md-12 area-content-all pull-right">
							<div class="col-md-12 area-content-list">
								<div class="col-md-12 div-list">
									<form id="add_user_med" method="post" class="col-md-12 ">
										<div class="row">
											<div class="form-group">
												<div class="col-md-6">
													<label for="nome_paciente_amed">Nome completo*</label>
													<input class="form-control" id="nome_paciente_amed" name="nome_paciente_amed" placeholder="Nome completo" value="<?php echo isset($DATAUSER['error_nome']) ? $DATAUSER['error_nome'] : '';?>">
												</div>
											</div>
											<div class="form-group form-cpf">
												<div class="col-md-6">
													<label for="cpf_amed">CPF*</label>
													<input class="form-control" id="cpf_amed" name="cpf_amed" placeholder="CPF">
												</div>
											</div>
										</div>
										
										<div class="row" style="margin-top: 15px">
											<div class="form-group">
												<div class="col-md-6">
													<label for="cel_amed">Celular*</label>
													<input  class="form-control" id="cel_amed" name="cel_amed" placeholder="Celular" value="<?php echo isset($DATAUSER['error_cel']) ? $DATAUSER['error_cel'] : '';?>">
												</div>
											</div>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <label for="cel_amed">E-mail*</label>
                                                    <input  class="form-control" id="email_amed" name="email_amed" placeholder="E-mail" value="<?php echo isset($DATAUSER['error_email']) ? $DATAUSER['error_email'] : '';?>">
                                                </div>
                                            </div>
											<div class="form-group">
												<div class="col-md-6" style="padding-top: 35px;">
													<button id="cadastrar" type="submit" class="button button-primary button-large btn-ingridix-cli2"  >CONFIRMAR CADASTRO</button>
													<!--<button type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large btn-ingridix-cli"  >ACESSAR RECEITAS INGRIDIX</button>-->
												</div>
											</div>
										</div>
                                        <input type="hidden" name="novo_paciente" value="novo_paciente" >
									</form>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</center>
    <script src='<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.min.js'></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.maskedinput.min.js"></script>

    <script>
        $(function(){

            $("#cpf_amed").mask("999.999.999-99");
            $("#cel_amed").mask("(99) 99999-9999");
        
        });

        $(function() {
            // Initialize form validation on the registration form.
            // It has the name attribute "registration"
            $("#cadastrar").click(function(){

                $("#add_user_med").validate({
                    // Specify validation rules
                    rules: {
                        nome_paciente_amed: "required",
                        cpf_amed:   "required",
                        cel_amed:   "required"
                    },
                    // Specify validation error messages
                    messages: {
                        nome_paciente_amed: "Insira o nome completo!",
                        cel_amed:  "Insira um número de celular!",
                        cpf_amed:  "Insira o CPF!"
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });
        });
    </script>
<?php
get_footer();
?>