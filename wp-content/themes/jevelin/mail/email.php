<?php

function msg_email(){
    global $tipo_email;

    /*
     * E-mail para cadastro de novo usuario
     * Usado em function.php
     * */
    if($tipo_email['tipo_email'] == "novo_usuario"){
        $content = '
        <table style="width: 100%">
            <thead>
            <tr>
                <center>
                    <img src="http://gape.com.br/gape/wp-content/uploads/2017/02/logo_gape_topo.png">
                </center>
            </tr>
            </thead>
            <tbody>
            <tr>
                <center>
                    <p style="font-size: 20px; font-weight: bolder; margin-top: 20px">Parabéns</p>
                </center>
                <p style="font-size: 16px; margin: 20px 0 0 0">Você foi cadastrado com sucesso no GAPE.</p>
                <p style="font-size: 16px; ">Seu login de usuário é: '. $tipo_email['novo_usuario'] .'</p>
                <a href="http://gape.com.br/gape" style="margin-top: 40px; color: #000000" >Voltar ao <strong>GAPE</strong>.</a>
            </tr>
            </tbody>
            <tfoot></tfoot>
        </table>
        ';
    }

/*
* E-mail para cadastro de novo usuario
* Usado em function.php
* */
if($tipo_email['tipo_email'] == "nova_ordem"){

    if($tipo_email['order_status'] == "aprovado"){
        $status = '<p style="font-size: 16px; ">O seu formulário já se encontra disponível no menu <strong>Meus Contratos</strong>, 
                    clique  <a href="http://gape.com.br/gape/meus-contratos" style="margin-top: 40px; color: #000000">aqui</a> para preeche-lo.</p>';
    }else{
        $status = "";
    }


    $content = '
        <table style="width: 100%">
            <thead>
            <tr>
                <center>
                    <img src="http://gape.com.br/gape/wp-content/uploads/2017/02/logo_gape_topo.png">
                </center>
            </tr>
            </thead>
            <tbody>
            <tr>
                <center>
                    <p style="font-size: 20px; font-weight: bolder; margin-top: 20px">Nova ordem de serviço</p>
                </center>
                <p style="font-size: 16px; margin: 20px 0 0 0">Sua ordem de serviço é <strong>'. $tipo_email['nova_ordem'] .'</strong> .</p>
                <p style="font-size: 16px; ">O status de sua compra é: '. $tipo_email['order_status'] .'</p>
                '. $status .'
                <a href="http://gape.com.br/gape" style="margin-top: 40px; color: #000000" >Voltar ao <strong>GAPE</strong>.</a>
               
            </tr>
            </tbody>
            <tfoot></tfoot>
        </table>
        ';

}




return $content;

}