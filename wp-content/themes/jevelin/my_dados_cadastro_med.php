<?php
/*
Template Name: Área médico
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}
global $wpdb;
get_header();
$current_user = wp_get_current_user();

?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<link rel="stylesheet" href="<?= get_site_url().'/wp-content/themes/jevelin/css/dataTables.bootstrap.css'; ?>" >

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="<?= get_site_url().'/wp-content/themes/jevelin/js/bootstrap3-typeahead.js'?>"></script>
<script src="<?= get_site_url().'/wp-content/themes/jevelin/js/dataTables.bootstrap.js'; ?>"></script>

<style>
	.area-menu{
		background: #EAECED;
		text-align: left;
		border-top-left-radius: 20px;
		border-top-right-radius: 20px;
		padding: 0;
		font-size: 14px;
		font-weight: bolder;
		color: #3e3e3e;
		padding-top: 13px;
		height: 350px;
	}
	
	.area-menu li{
		display: block;
		padding: 1px 15px 5px 17px;
		
	}
	.area-menu li a{
		cursor: pointer;
	}
	.area-menu li a:hover{
		cursor: pointer;
		color: #337ab7 !important;
	}
	.area-menu-footer{
		background: #334C5B;
		padding: 7px 0 0 0;
		position: absolute;
		bottom: 11px;
		width: 100%;
		
	}
	
	.area-menu-footer li, .area-menu-footer li a, .area-menu-footer li a:hover{
		color: #FFFFFF !important;
		font-size: 12px;
		text-decoration: underline;
	}
	
	.col-md-3{
		padding: 0 !important;
	}
	
	.area-content-list{
	
	}
	
	.area-content-all{
		border: 1px solid #7AB8DA;
		background: #FFFFFF;
		-webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
		-moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
		box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
		height: 350px;
		padding-top: 15px
	}
	
	.area-content-alert{
		background: #79B8DB;
		
	}
	.area-content-alert h2{
		color: #FFFFFF;
		font-size: 25px;
		padding: 2px 10px 11px 10px;
	}
	.div-list{
		
		font-size: 13px;
	}
	.div-list a, .div-list a:hover{
		text-decoration: none;
		color: #3f3f3f;
	}
	.div-list h5{
		font-size: 16px;
		text-align: left;
	}
	.centered {
		margin: 0 auto !important;
		float: none !important;
	}
	.no-padding{
		padding: 0;
	}
	.btn-ingridix-cli{
		width: 27.4%;
		height: 50px;
		padding: 12px 10px 8px 10px;
		border-radius: 40px;
		background: #79B8DB;
		color: #FFFFFF;
		font-weight: bolder;
		font-size: 17px;
		margin-bottom: 15px;
		cursor: pointer;
	}
	.nome-logado{
		text-align: left;
		color: #3C4F5C;
		margin-bottom: 15px;
	}
	.nome-logado a, .nome-logado a:hover{
		color: #3C4F5C;
		text-decoration: none;
	}
	#wrapper > .sh-page-layout-default{
		padding: 30px 0 ;
	}
	.list-receita li{
		background: #F5F5F5;
		/*display: block;
		padding: 4px;*/
		margin-bottom: 4px;
		text-align: left;
		
	}
	.list-receita li a, .list-receita li a:hover{
		color: #3e3e3ee6;
		font-size: 13px;
		font-weight: bolder;
		text-transform: uppercase;
	}
	
	.color-andamento{
		color: #37A46D;
	}
	
	.color-expirou{
		color: #FD5B5B;
	}
	
	.color-cadastrada{
		color: #FF962D;
	}
	
	.area-menu a, .area-menu a:hover{
		color: #3e3e3e;
		text-align: none;
	}
	
	.active{
		color: #337ab7 !important;
	}
	
	#tabela_area_medico_filter{
		text-align: right;
	}
	#tabela_area_medico td, #tabela_area_medico th{
		border: none !important;
		text-align: left;
		padding: 2px !important;
		font-weight: bolder;
	}
	
	#tabela_area_medico td a:hover{
		color: #337ab7 !important;
	}
	
	.th_1{
		width: 80%;
	}
	.th_2{
		width: 20%;
	}
	
	.td_content {
		background-color: #F5F5F5;
		margin-right: 20px;
		padding: 6px;
	}
	
	.td_content2{
		background-color: #F5F5F5;
		padding: 6px;
		color: #56A235;
	}
	
	
	/* Breakpoints */
	@media only screen and (min-width: 320px) and (max-width: 620px) {
		
		.col-md-9{
			padding: 0 5px 0 7px;
		}
		
		.area-content-all{
			height: auto;
		}
		
		.area-content-list{
			padding: 0;
		}
		.div-list {
			padding: 0 !important;
		}
		
		.td_content {
			background-color: #F5F5F5;
			margin-right: 20px;
			padding: 6px;
		}
		
		.td_content2 {
			background-color: #F5F5F5;
			padding: 6px;
			color: #56A235;
			font-size: 10px;
		}
		
		.area-menu{
			padding-bottom: 20px;
			height: auto;
		}
		.nome-logado{
			display: inline-block;
		}
		
		.btn-ingridix-cli{
			width: 100% !important;
		}
	}
	
	@media only screen and (min-width: 621px) and (max-width: 940px) {
		
		.area-menu{
			padding-bottom: 20px;
			height: auto;
		}
		
		.nome-logado{
			display: inline-block;
		}
		
		.btn-ingridix-cli{
			width: 100% !important;
		}
	}
	
	@media only screen and (min-width: 941px) and (max-width: 1260px) {
	
	}
	
	@media only screen and (min-width: 1261px) {
	
	}
</style>
<center>
	<div class="container">
		
		<div class="row">
			
			<div class="col-md-11 centered">
				
				<div class="col-md-11 no-padding">
					<div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área médico" onclick='location.href="<?= get_site_url().'/area-medico'?>"'>INGRIDIX LAB</div>
				</div>
				<div class="col-md-3 nome-logado pull-left">
					Olá <b> <?= $current_user->user_login; ?></b>
				</div>
				<div class="col-md-8 nome-logado">
					<b><a >&nbsp;&nbsp;&nbsp;</a></b>
				</div>
				<div class="col-md-3">
					<ul class="area-menu list-unstyled">
						<li><a href="<?= get_site_url().'/cadastrar-novo-paciente'?>">CADASTAR NOVO PACIENTE</a></li>
						<li><a data-toggle="modal" data-target="#novareceitaModal" data-whatever="@mdo">CRIAR NOVA FÓRMULA</a></li>
						<li><a class="active" href="<?= get_site_url().'/area-medico'?>">RECEITAS ANTERIORES</a></li>
						<li><a href="http://ingridix.com.br/tabela-de-cepas-e-sintomas" target="_blank">DÚVIDAS SOBRE AS CEPAS</a></li>
						<li><a href="<?= get_site_url().'/minhas-receitas'?>">MINHAS RECEITAS</a></li>
						<li><br></li>
						
						<div class="area-menu-footer">
							<li><a href="<?= get_site_url()?>/meu-cadastro-medico">MEUS DADOS DE CADASTRO</a></li>
							<li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
						</div>
					</ul>
				</div>
				
				<div class="col-md-9 area-content">
					<div class="col-md-12 area-content-all">
						<?php
						$meus_pacientes = $wpdb->get_results( "select DISTINCT mr.id_ref, mr.id_receita_med, cp.nome_paciente, cp.id_paciente, rr.status from receitas_medicos rm join minhas_receitas mr join cadastros_pacientes cp join referencia_receitas rr
where mr.id_receita_med = rm.id_receita and  rr.id_ref = rm.id_ref and rr.status is not null and mr.id_pac = cp.id_paciente and rm.id_med = $current_user->id" );
						
						if(count($meus_pacientes) <= 0):
							?>
							<div class="col-md-12 area-content-alert">
								<h2>VOCÊ NÂO TEM RECEITAS CADASTRADAS!</h2>
							</div>
							<?php
						else:
						?>
						<div class="col-md-12 area-content-list">
							<div class="col-md-12 div-list">
								<ul class="list-unstyled list-receita">
									
									<table id="tabela_area_medico" class="table" cellspacing="0" width="100%" style="border: none !important;margin-top: 0 !important;">
										<thead >
										<tr>
											<th class="th_1">Selecione o paciente</th>
											<th class="th_2">Status</th>
										</tr>
										</thead>
										<?php
										foreach ($meus_pacientes as $row):
											?>
											<tr>
												<td><div class="td_content"><a href="<?= get_site_url().'/receita-salva/?idr='.$row->id_receita_med.'&idp='.$row->id_paciente.''; ?>"><?= $row->nome_paciente; ?></a></div></td>
												<td><div class="td_content2"><?php
														
														switch ($row->status){
															case $row->status = "0":
																echo "<b>Cancelado</b>";
																break;
															case $row->status = "1":
																echo "<b>Em análise</b>";
																break;
															case $row->status = "2":
																echo "<b>Aguardando</b>";
																break;
															case $row->status = "3":
																echo "<b>Processando</b>";
																break;
															case $row->status = "4":
																echo "<b>Entregando</b>";
																break;
															case $row->status = "5":
																echo "<b>Disponível</b>";
																break;
															case $row->status = "6":
																echo "<b>Concluído</b>";
																break;
														}
														
														?></div></td>
											</tr>
											<?php
										endforeach;
										?>
										
										<tbody>
										</tbody>
									</table>
								</ul>
							</div>
							
							<?php
							endif;
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</center>

<?php
get_footer();
?>

<script>

    $(function () {

        $('#tabela_area_medico').DataTable({
            "bLengthChange": false,
            oLanguage: {
                sSearch: "Buscar paciente",
                oPaginate: {
                    "sNext": '<i class="fa fa-chevron-right" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                }
            },
            "bInfo" : false,
            "pageLength": 6

        });

    })

</script>
