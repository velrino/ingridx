<?php
/*
Template Name: Minhas receitas paciente
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}
get_header();
$current_user = wp_get_current_user();
$cpf_paciente = get_user_meta(  $current_user->id , 'billing_cpf', true );

?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <link rel="stylesheet" href="<?= get_site_url().'/wp-content/themes/jevelin/css/dataTables.bootstrap.css'; ?>" >

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="<?= get_site_url().'/wp-content/themes/jevelin/js/bootstrap3-typeahead.js'?>"></script>
    <script src="<?= get_site_url().'/wp-content/themes/jevelin/js/dataTables.bootstrap.js'; ?>"></script>

    <style>
		.area-menu{
			background: #EAECED;
			text-align: left;
			border-top-left-radius: 20px;
			border-top-right-radius: 20px;
			padding: 0;
			font-size: 14px;
			font-weight: bolder;
			color: #3e3e3e;
			padding-top: 13px;
			height: 350px;
		}
		
		.area-menu li{
			display: block;
			padding: 1px 15px 5px 17px;
			
		}
		
		.area-menu-footer{
			background: #B3956B;
			padding: 7px 0 0 0;
			position: absolute;
			bottom: 11px;
			width: 100%;
			
		}

        .area-menu li a{
            color: #555555 !important;
        }
        .area-menu li a:hover{
            color: #ECBB71 !important;
        }

        .area-menu-footer li a, .area-menu-footer li a:hover{
            color: #FFFFFF !important;
            font-size: 12px;
            text-decoration: underline;
        }
		
		.col-md-3{
			padding: 0 !important;
		}
		
		.area-content-list{
			margin-top:20px;
		}
		
		.area-content-all{
			border: 1px solid #E6BA7C;
			background: #FFFFFF;
			-webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			-moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			height: 350px;
			padding-top: 15px
		}
		
		.area-content-alert{
			background: #A5C48E;
			
		}
		.area-content-alert h2{
			color: #FFFFFF;
			font-size: 25px;
			padding: 10px;
		}
		.div-list{
			background: #F5F5F5;
			
			font-size: 13px;
			font-weight: bolder;
		}
		.centered {
			margin: 0 auto !important;
			float: none !important;
		}
		.no-padding{
			padding: 0;
		}
		.btn-ingridix-cli{
			width: 27.4%;
			height: 50px;
			padding: 12px 10px 8px 10px;
			border-radius: 40px;
			background: #ECBB71 !important;
			color: #FFFFFF;
			font-weight: bolder;
			font-size: 17px;
			margin-bottom: 15px;
			cursor: pointer;
            box-shadow: inset 0px 0px 30px 10px #e89f2f;
		}
		.nome-logado{
			text-align: left;
			color: #555555;
			margin-bottom: 15px;
		}
		#wrapper > .sh-page-layout-default{
			padding: 30px 0 ;
		}
        
        .dataTables_filter, thead{
            display: none;
        }

        .td_content{
            background-color: #F5F5F5;
            margin-right: 20px;
            padding: 6px;
        }

        .td_content2{
            background-color: #F5F5F5;
            padding: 6px;
            color: #56A235;
        }

        li a.active{
            color: #ECBB71 !important;
        }
        /* Breakpoints */
        @media only screen and (min-width: 320px) and (max-width: 620px) {

            .col-md-9{
                padding: 0 5px 0 7px;
            }

            .area-content-all{
                height: auto;
                padding-top: 0;
            }

            .area-content-list{
                padding: 0;
                margin-top: 15px;
            }

            .area-content-alert h2{
                font-size: 16px;
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }
            
            .btn-ingridix-cli{
                width: 100% !important;
            }

            #tabela_area_medico td, #tabela_area_medico th{
                border: none !important;
                text-align: left;
                padding: 2px !important;
                font-weight: bolder;
            }

            #tabela_area_medico td a:hover{
                color: #ECBB71 !important;
            }
            
            .td_content{
                background-color: #F5F5F5;
                margin-right: 0px;
                padding: 6px;
                font-size: 10px;
            }

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 10px;
            }
        }

        @media only screen and (min-width: 621px) and (max-width: 940px) {

            .area-content-all{
                height: auto;
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }

            .area-content-alert h2{
                font-size: 21px;
               
            }
            
            .btn-ingridix-cli{
                width: 100% !important;
            }

            #tabela_area_medico td, #tabela_area_medico th{
                border: none !important;
                text-align: left;
                padding: 2px !important;
                font-weight: bolder;
            }

            #tabela_area_medico td a:hover{
                color: #ECBB71 !important;
            }

            .td_content{
                background-color: #F5F5F5;
                margin-right: 0px;
                padding: 6px;
                font-size: 14px;
            }

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 14px;
            }
        }

        @media only screen and (min-width: 941px) and (max-width: 1260px) {
            #tabela_area_medico td, #tabela_area_medico th{
                border: none !important;
                text-align: left;
                padding: 2px !important;
                font-weight: bolder;
            }

            #tabela_area_medico td a:hover{
                color: #ECBB71 !important;
            }

            .td_content{
                background-color: #F5F5F5;
                margin-right: 20px;
                padding: 6px;
                font-size: 14px;
            }

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 14px;
            }
        }

        @media only screen and (min-width: 1261px) {
            #tabela_area_medico td, #tabela_area_medico th{
                border: none !important;
                text-align: left;
                padding: 2px !important;
                font-weight: bolder;
            }

            #tabela_area_medico td a:hover{
                color: #ECBB71 !important;
            }

            .td_content{
                background-color: #F5F5F5;
                margin-right: 20px;
                padding: 6px;
                font-size: 14px;
            }

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 14px;
            }
        }
	</style>
	<center>
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-11 centered">
					<div class="col-md-11 no-padding">
						<div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área cliente" onclick='location.href="<?= get_site_url().'/area-cliente'?>"'>ÁREA DO CLIENTE</div>
					</div>
					<div class="col-md-11 nome-logado">
						Olá <b><?= $current_user->user_login; ?></b>
					</div>
					<div class="col-md-3">
						<ul class="area-menu list-unstyled">
                            <li><a class="active" href="<?= get_site_url()?>/receitas-anteriores" >RECEITAS ANTERIORES</a></li>
							<li><a href="http://ingridix.com.br/duvidas-sobre-o-uso" target="_blank">DÚVIDAS SOBRE O USO</a></li>
							<li><br></li>
							
							
							<div class="area-menu-footer">
								<li><a href="<?= get_site_url()?>/meu-cadastro-paciente">MEUS DADOS DE CADASTRO</a></li>
								<li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
							</div>
						</ul>
					</div>
					
					<?php
					$meus_pacientes = $rows = $wpdb->get_results( "select DISTINCT mr.id_receita_med, mr.id_ref, cp.nome_paciente, wu.user_login, rr.status from cadastros_pacientes cp join wp_usermeta wum join minhas_receitas mr join wp_users wu join referencia_receitas rr
where cp.cpf_paciente = wum.meta_value and wum.meta_value = '".$cpf_paciente."' and mr.id_ref = rr.id_ref and rr.status is not null and mr.id_pac = cp.id_paciente and mr.id_med = wu.ID and mr.status = 0" );
     
					$qtd = count($meus_pacientes);
					?>
					<div class="col-md-9 area-content">
						<div class="col-md-12 area-content-all">
							<?php
							if($qtd > 0):
								?>
								<div class="col-md-12 area-content-alert">
									<h2>VOCÊ TEM NOVA RECEITA!</h2>
								</div>

                                <div class="col-md-12 area-content-list">

                                    <table id="tabela_area_medico" class="table" cellspacing="0" width="100%" style="border: none !important;margin-top: 0 !important;">
                                        <thead >
                                        <tr>
                                            <th class="th_1"></th>
                                            <th class="th_2"></th>
                                        </tr>
                                        </thead>
										<?php
										foreach ($meus_pacientes as $row):
											?>
                                            <tr>
                                                <td><div class="td_content"> <?= $row->user_login; ?></div></td>
                                                <td><div class="td_content2"><a href="<?= get_site_url().'/produto/?idr='.$row->id_receita_med.'&idrr='.$row->id_ref; ?>" style="color: #249B60">EFETUAR COMPRA</a></div></td>
                                            </tr>
											<?php
										endforeach;
										?>

                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                                
								<?php
							else:
								?>
								<div class="col-md-12 area-content-alert">
									<h2>VOCÊ NÂO POSSUI RECEITAS!</h2>
								</div>
								<?php
							endif;
							?>
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</center>

<?php
get_footer();
?>

<script>

    $(function () {

        $('#tabela_area_medico').DataTable({
            "bLengthChange": false,
            oLanguage: {
                sSearch: "Buscar paciente",
                oPaginate: {
                    "sNext": '<i class="fa fa-chevron-right" ></i>',
                    "sPrevious": '<i class="fa fa-chevron-left" ></i>'
                }
            },
            "bInfo" : false,
            "pageLength": 5

        });

    })

</script>

