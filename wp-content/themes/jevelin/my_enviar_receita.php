<?php
/*
Template Name: Enviar receita
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}
get_header();
$current_user = wp_get_current_user();
$cpf_paciente = get_user_meta(  $current_user->id , 'billing_cpf', true );

?>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<style>
		.area-menu{
			background: #EAECED;
			text-align: left;
			border-top-left-radius: 20px;
			border-top-right-radius: 20px;
			padding: 0;
			font-size: 14px;
			font-weight: bolder;
			color: #3e3e3e;
			padding-top: 13px;
			height: 630px;
		}
		
		.area-menu li{
			display: block;
			padding: 1px 15px 5px 17px;
			
		}
        
		.area-menu-footer{
			background: #B3956B;
			padding: 7px 0 0 0;
			position: absolute;
			bottom: 11px;
			width: 100%;
			
		}
  
		.col-md-3{
			padding: 0 !important;
		}
		
		.area-content-list{
			margin-top:20px;
		}
		
		.area-content-all{
			border: 1px solid #E6BA7C;
			background: #FFFFFF;
			-webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			-moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			height: auto;
			padding-top: 15px;
            padding-bottom: 15px;
		}
        
        .area-content-list p{
            text-align: left;
            color: #555555 !important;
            font-size: 15px;
        }
		
		.area-content-alert{
		 background: #A5C48E;
		
		}
		.area-content-alert h2{
			color: #FFFFFF;
			font-size: 25px;
			padding: 10px;
            margin-top: 10px !important;
		}
  
		.centered {
			margin: 0 auto !important;
			float: none !important;
		}
		.no-padding{
			padding: 0;
		}
		.btn-ingridix-cli{
			width: 27.4%;
			height: 50px;
			padding: 12px 10px 8px 10px;
			border-radius: 40px;
			background: #ECBB71 !important;
			color: #FFFFFF;
			font-weight: bolder;
			font-size: 17px;
			margin-bottom: 15px;
			cursor: pointer;
            box-shadow: inset 0px 0px 30px 10px #e89f2f;
		}
  
		.nome-logado{
			text-align: left;
			color: #555555;
			margin-bottom: 15px;
		}
		#wrapper > .sh-page-layout-default{
			padding: 30px 0 ;
		}
        
        .page-description{
            margin-top: 20px;
        }

        .area-menu li a{
            color: #555555 !important;
        }
        .area-menu li a:hover{
            color: #ECBB71 !important;
        }

        .area-menu-footer li a, .area-menu-footer li a:hover{
            color: #FFFFFF !important;
            font-size: 12px;
            text-decoration: underline;
        }

        /* Breakpoints */
        @media only screen and (min-width: 320px) and (max-width: 620px) {

            .col-md-9{
                padding: 0 5px 0 7px;
            }

            .area-content-all{
                height: auto;
                padding-top: 0;
            }

            .area-content-list{
                padding: 0;
            }

            .area-content-alert h2{
                font-size: 16px;
            
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }

            .btn-ingridix-cli-opt{
                width: 100%;
                height: 68px;
                margin-left: 0;
                padding: 12px 10px 13px 10px
            }

            .btn-ingridix-cli{
                width: 100% !important;
            }

            .area-content-list p{
                font-size: 16px;
            }
        }

        @media only screen and (min-width: 621px) and (max-width: 940px) {

            .area-content-all{
                height: auto;
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }

            .area-content-alert h2{
                font-size: 25px;
            }

            .btn-ingridix-cli-opt{
                width: 100%;
                height: 55px;
                margin-left: 0;
                padding: 12px 10px 13px 10px
            }

            .btn-ingridix-cli{
                width: 100% !important;
            }

            .area-content-list p{
                font-size: 21px;
            }
        }

        @media only screen and (min-width: 941px) and (max-width: 1260px) {

        }

        @media only screen and (min-width: 1261px) {

        }
	</style>
	<center>
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-11 centered">
					<div class="col-md-11 no-padding">
						<div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área cliente" onclick='location.href="<?= get_site_url().'/area-cliente'?>"'>ÁREA DO CLIENTE</div>
					</div>
					<div class="col-md-11 nome-logado">
						Olá <b><?= $current_user->user_login; ?></b>
					</div>
					<div class="col-md-3">
						<ul class="area-menu list-unstyled">
                            <li><a href="<?= get_site_url()?>/receitas-anteriores" >RECEITAS ANTERIORES</a></li>
							<li><a href="http://ingridix.com.br/duvidas-sobre-o-uso" target="_blank">DÚVIDAS SOBRE O USO</a></li>
							<li><br></li>
							
							
							<div class="area-menu-footer">
								<li><a href="<?= get_site_url()?>/meu-cadastro-paciente">MEUS DADOS DE CADASTRO</a></li>
								<li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
							</div>
						</ul>
					</div>
					
					
					<div class="col-md-9 area-content">
						<div class="col-md-12 area-content-all pull-right">

                            <div class="col-md-12 area-content-alert">
                                <h2>Você pode enviar sua receita por:</h2>
                            </div>
							
							<div class="col-md-12 area-content-list">
								<div class="row">
									<div class="col-md-4 no-padding">
									    <img src="<?= get_site_url()?>/wp-content/uploads/2017/09/logo-whatsapp-black.png" width="100" />
                                    
                                        <h4>Em breve</h4>
                                    </div>
									<div class="col-md-4 no-padding">
                                        <img src="<?= get_site_url()?>/wp-content/uploads/2017/09/email.png" width="100" />
                                        <h4>Envie o e-mail com sua
                                            receita para
                                            receita@ingridix.com.br</h4>
                                    </div>
									<div class="col-md-4 no-padding">
                                        <img src="<?= get_site_url()?>/wp-content/uploads/2017/09/logo-facebook-black.png" width="100" />
                                        <h4>Envie a foto da sua receita
                                            para nossa fanpage<br>
                                            <a href="http://facebook.com/ingridix" target="_blank">facebook.com/ingridix</a></h4>
                                    </div>
								</div>
                                
                                <div class="page-description">

                                    <p>
                                        Após o envio, você receberá na plataforma de comunicação escolhida (whats app, email ou
                                        facebook), o orçamento e o link para pagamento.
                                    </p>

                                    <p>
                                        O seu composto Ingridix chegará será enviado para o seu endereço cadastrado. O tempo de
                                        entrega pode ser de 2 a 7 dias, dependendo do local. Você receberá um código de rastreamento.
                                    </p>

                                    <p>
                                        Nosso horário de atendimento é de segunda a sexta das 8:00 às 18:00. Após o envio da receita,
                                        você receberá o orçamento e o link para pagamento através da plataforma de comunicação
                                        escolhida (whats app, e-mail ou facebook).
                                    </p>
                                    
                                    <p>
                                        A sua Fórmula Ingridix será chegará no seu endereço cadastrado num prazo de 1 a 7 dias,
                                        dependendo da sua localidade.
                                    </p>
                                </div>
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</center>

<?php
get_footer();
?>