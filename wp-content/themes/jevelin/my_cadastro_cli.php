<?php
/*
Template Name: Cadastro cliente
*/
if(is_user_logged_in()){
	
	wp_redirect( get_site_url()."/area-cliente" );
}

get_header();
global $DATAUSER;

?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <style>

        .bloco-login{
            background: #FFFFFF;
            padding-bottom: 10px;
            -webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            -moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        }

        .bloco-login h1{
            color: #876A42;
            margin-bottom: 20px;
        }

        h2{
            color: #876A42 !important;
            margin-left: 15px;

        }
        h3{
            font-size: 19px;
            margin-top: 5px !important;
            margin-bottom: 15px;
        }

        .form-group{
            margin-bottom: 0 !important;
        }
        .form-group input, .form-group select{
            background: rgba(240, 240, 240, 0.83);
            height: 40px;

        }
        .btn-ingridix-cli{
            width: 80%;
            height: 50px;
            border-radius: 40px;
            background: #ECBB71 !important;
            color: #FFFFFF;
            font-weight: bolder;
            font-size: 17px;
            border: none;
            padding: 0 10px 0 10px !important;
            box-shadow: inset 0px 0px 30px 10px #e89f2f;
        }
        .no-padding{
            padding: 0;
        }
        .small{
            margin-top: 5px;
            font-size: 13px;
            color: #545454;
            margin-left: 30px;
        }
        .clique_aqui{
            margin-top: 5px;
            font-size: 14px;
            color: #876A42;
        }

        #wrapper{
            background: #f1f1f1;
        }

        .titulo-pagina{
            margin: -20px 0 20px 45px;
            font-size: 40px;
        }

        label{
            float: left;
        }

        a:hover{
            text-decoration: none;
            color: #FFFFFF;
        }

        select{
            line-height: 12px !important;
        }

        .error{
            color: #d60707;
            font-size: 13px;
        }

        /* Breakpoints */
        @media only screen and (min-width: 320px) and (max-width: 620px) {

            .btn-ingridix-cli{
                width: 100% !important;
            }
        }

        @media only screen and (min-width: 621px) and (max-width: 940px) {

        }

        @media only screen and (min-width: 941px) and (max-width: 1260px) {
            .bloco-login h2{
                float: le;
            }
        }

        @media only screen and (min-width: 1261px) {

        }
    </style>
    <center>
        <div class="container">

            <h1 class="titulo-pagina">FAÇA SEU CADASTRO</h1>
        
	        <?php
	        if(isset($DATAUSER['error'])):
		        ?>
                <div class="col-md-12">
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-close"></i>
				        <?php echo $DATAUSER['error']?>
                    </div>
                </div>
		
		        <?php
	        endif;
	        ?>
            
            <div class="col-md-12 bloco-login">

                <h2 >ACESSO CLIENTE</h2>

                <form id="add_user" method="post" class="col-md-12 ">
                    <div class="row">

                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="nome">Nome completo*</label>
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Nome completo" value="<?php echo isset($DATAUSER['first_name']) ? $DATAUSER['first_name'] : '';?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="cpf">CPF*</label>
                                <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF" value="<?php echo isset($DATAUSER['cpf']) ? $DATAUSER['cpf'] : '';?>">
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="celular">Celular*</label>
                                <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" value="<?php echo isset($DATAUSER['celular']) ? $DATAUSER['celular'] : '';?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="email">E-mail*</label>
                                <input type="text" class="form-control" id="user_email" name="user_email" placeholder="E-mail" value="<?php echo isset($DATAUSER['email']) ? $DATAUSER['email'] : '';?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="nome">CEP*</label>
                                <input type="text" class="form-control" id="cep" name="cep" placeholder="CEP" value="<?php echo isset($DATAUSER['cep']) ? $DATAUSER['cep'] : '';?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="nome">Endereço*</label>
                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço" value="<?php echo isset($DATAUSER['endereco']) ? $DATAUSER['endereco'] : '';?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="cpf">Nº*</label>
                                <input type="text" class="form-control" id="numero" name="numero" placeholder="Nº" value="<?php echo isset($DATAUSER['numero']) ? $DATAUSER['numero'] : '';?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="nome">Complemento</label>
                                <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Complemento" value="<?php echo isset($DATAUSER['complemento']) ? $DATAUSER['complemento'] : '';?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="celular">Bairro*</label>
                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" value="<?php echo isset($DATAUSER['bairro']) ? $DATAUSER['bairro'] : '';?>">
                            </div>
                        </div>
                        
                    </div>

                    <div class="row">

                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="cidade">Cidade*</label>
                                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade" value="<?php echo isset($DATAUSER['cidade']) ? $DATAUSER['cidade'] : '';?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label for="estado">Estado*</label>
                                 <select name="estado" id="estado" class="state_select " autocomplete="address-level1" data-placeholder="">
                                    <option value="">Selecione uma opção…</option>
                                    <option value="AC" <?php echo $DATAUSER['estado'] == "AC" ? "selected" : '';?>>Acre</option>
                                    <option value="AL" <?php echo $DATAUSER['estado'] == "AL" ? "selected" : '';?>>Alagoas</option>
                                    <option value="AP" <?php echo $DATAUSER['estado'] == "AP" ? "selected" : '';?>>Amapá</option>
                                    <option value="AM" <?php echo $DATAUSER['estado'] == "AM" ? "selected" : '';?>>Amazonas</option>
                                    <option value="BA" <?php echo $DATAUSER['estado'] == "BA" ? "selected" : '';?>>Bahia</option>
                                    <option value="CE" <?php echo $DATAUSER['estado'] == "CE" ? "selected" : '';?>>Ceará</option>
                                    <option value="DF" <?php echo $DATAUSER['estado'] == "DF" ? "selected" : '';?>>Distrito Federal</option>
                                    <option value="ES" <?php echo $DATAUSER['estado'] == "ES" ? "selected" : '';?>>Espírito Santo</option>
                                    <option value="GO" <?php echo $DATAUSER['estado'] == "GO" ? "selected" : '';?>>Goiás</option>
                                    <option value="MA" <?php echo $DATAUSER['estado'] == "MA" ? "selected" : '';?>>Maranhão</option>
                                    <option value="MT" <?php echo $DATAUSER['estado'] == "MT" ? "selected" : '';?>>Mato Grosso</option>
                                    <option value="MS" <?php echo $DATAUSER['estado'] == "MS" ? "selected" : '';?>>Mato Grosso do Sul</option>
                                    <option value="MG" <?php echo $DATAUSER['estado'] == "MG" ? "selected" : '';?>>Minas Gerais</option>
                                    <option value="PA" <?php echo $DATAUSER['estado'] == "PA" ? "selected" : '';?>>Pará</option>
                                    <option value="PB" <?php echo $DATAUSER['estado'] == "PB" ? "selected" : '';?>>Paraíba</option>
                                    <option value="PR" <?php echo $DATAUSER['estado'] == "PR" ? "selected" : '';?>>Paraná</option>
                                    <option value="PE" <?php echo $DATAUSER['estado'] == "PE" ? "selected" : '';?>>Pernambuco</option>
                                    <option value="PI" <?php echo $DATAUSER['estado'] == "PI" ? "selected" : '';?>>Piauí</option>
                                    <option value="RJ" <?php echo $DATAUSER['estado'] == "RJ" ? "selected" : '';?>>Rio de Janeiro</option>
                                    <option value="RN" <?php echo $DATAUSER['estado'] == "RN" ? "selected" : '';?>>Rio Grande do Norte</option>
                                    <option value="RS" <?php echo $DATAUSER['estado'] == "RS" ? "selected" : '';?>>Rio Grande do Sul</option>
                                    <option value="RO" <?php echo $DATAUSER['estado'] == "RO" ? "selected" : '';?>>Rondônia</option>
                                    <option value="RR" <?php echo $DATAUSER['estado'] == "RR" ? "selected" : '';?>>Roraima</option>
                                    <option value="SC" <?php echo $DATAUSER['estado'] == "SC" ? "selected" : '';?>>Santa Catarina</option>
                                    <option value="SP" <?php echo $DATAUSER['estado'] == "SP" ? "selected" : '';?>>São Paulo</option>
                                    <option value="SE" <?php echo $DATAUSER['estado'] == "SE" ? "selected" : '';?>>Sergipe</option>
                                    <option value="TO" <?php echo $DATAUSER['estado'] == "TO" ? "selected" : '';?>>Tocantins</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Senha*</label>
                                <input type="password" id="senha" name="senha" placeholder="Senha" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <label class="control-label">Confirmar senha*</label>
                                <input type="password" id="user_pass" name="user_pass" placeholder="Confirmar senha" class="form-control">
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 18px;">
                                <button id="cadastrar" class="button button-primary button-large btn-ingridix-cli"  >CONCLUIR CADASTRO</button>
                                <!--<button type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large btn-ingridix-cli"  >ACESSAR RECEITAS INGRIDIX</button>-->
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="tipo_user" value="paciente">
                </form>
            </div>
        </div>
    </center>

    <script src='<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.min.js'></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.maskedinput.min.js"></script>

    <script>
        $(function(){
            
            $("#cpf").mask("999.999.999-99");
           // $("#telefone").mask("(99) 9999-9999");
            $("#celular").mask("(99) 99999-9999");
           // $("#cep").mask( "99999-999" );
           $('#numero, #idade').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

            $("#cep").focusout(function(){
                
                var cep = $("#cep").val();
               
                if(cep.length > 2){
                    $.ajax({
                        url: "https://api.postmon.com.br/v1/cep/" + cep,
                        type: "GET",
                        dataType: "json",
                        success: function(data){

                            if(data['cep']){

                                $("#endereco").val(data['logradouro']);
                                $("#bairro").val(data['bairro']);
                                $("#cidade").val(data['cidade']);
                                $("#estado").val(data['estado_info']['nome']);

                            }

                            return false;
                        },
                        error: function(){
                            alert('Insira um CEP valido!');

                            $("#cep").val("");
                            $("#endereco").val("");
                            $("#bairro").val("");
                            $("#cidade").val("");
                            $("#estado").val("");
                            
                            return false;
                        }
                    });
                };
                
                return false;
            });
            
            
        })

        $(function() {
            // Initialize form validation on the registration form.
            // It has the name attribute "registration"
            
            $("#cpf").focusout(function(){
                
                var cpf = $(this).val();

                //Envia para função ajax
                $.ajax({
                    url: '<?= get_site_url() . "/functions-ajax";?>',
                    type: "post",
                    data: {
                        billing_cpf : cpf, pega_cpf : "ok"
                    },
                    dataType: "json",
                    success: function (data) {
                        $("#celular").val(data[0].cel_paciente);
                        $("#first_name").val(data[0].nome_paciente);
                        $("#user_email").val(data[0].email_paciente);
                    }
                })
                
                return false;
            });
            
            $("#cadastrar").click(function(){

                if($("#senha").val() != $("#user_pass").val()){
                    alert("Senhas diferentes!");
                    return false;
                }else{
                    $("#add_user").validate({
                        // Specify validation rules
                        rules: {
                            // The key name on the left side is the name attribute
                            // of an input field. Validation rules are defined
                            // on the right side
                            first_name: "required",
                            cpf: "required",
                            celular: "required",
                            user_email: {
                                required: true,
                                // Specify that email should be validated
                                // by the built-in "email" rule
                                email: true
                            },
                            senha: {
                                required: true,
                                minlength: 6
                            },
                            user_pass: 'required',
                            cep: "required",
                            endereco: "required",
                            numero: "required",
                            bairro: "required",
                            cidade: "required",
                            estado: "required"

                        },
                        // Specify validation error messages
                        messages: {
                            first_name: "Insira o nome completo!",
                            celular: "Insira um número de celular!",
                            senha: {
                                required: "Insira uma senha!",
                                minlength: "A senha deve conter no minimo 6 caracteres!"
                            },
                            user_pass: {
                                required: "Confirme a senha!"
                            },
                            user_email: "Insira um e-mail válido!",
                            cpf: "Insira o CPF!",
                            cep: "Insira o CEP correto!",
                            endereco: "Insira o Endereço!",
                            numero: "Insira o número!",
                            bairro: "Insira o bairro!",
                            cidade: "Insira a cidade!",
                            estado: "Insira o estado!"
                        },
                        // Make sure the form is submitted to the destination defined
                        // in the "action" attribute of the form when valid
                        submitHandler: function(form) {
                            form.submit();
                        }
                    });
                }

            })
        });
    </script>


<?php
get_footer();
?>