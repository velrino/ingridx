<?php
/*
Template Name: Receita salva
*/

if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}

global $wpdb;
get_header();
$current_user = wp_get_current_user();

//Dados do paciente
$id_receita    = $_GET['idr'];
$id_referencia = $_GET['idrr'];
$id_pac        = isset($_GET['idp']) ? $_GET['idp'] : "";

$user_id    = $current_user->ID;

if(empty($id_pac)){
	
	$rows = $wpdb->get_results( "select DISTINCT mr.id_produto, wp.post_title, mr.qtd_produto from receitas_medicos rm join minhas_receitas mr join referencia_receitas rr join wp_posts wp
where rm.id_receita = mr.id_receita_med and mr.id_ref = rr.id_ref
      and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_receita" );
	
}else{
	
	$user_id    = $current_user->ID;
	$rows = $wpdb->get_results( "select DISTINCT mr.id_produto, wp.post_title, mr.qtd_produto, cp.nome_paciente, cp.cpf_paciente, cp.cel_paciente from receitas_medicos rm join minhas_receitas mr join referencia_receitas rr join wp_posts wp join cadastros_pacientes cp
where rm.id_receita = mr.id_receita_med and mr.id_ref = rr.id_ref and cp.id_paciente = rr.id_pac
      and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_receita" );
	
	$rowUserId = $wpdb->get_results("select user_id from wp_usermeta WHERE meta_value = '". $rows[0]->cpf_paciente ."' ");
	$IdUserId  = $rowUserId->user_id;
	
	$rowUserId = $wpdb->get_results("select * from wp_usermeta WHERE user_id = $user_id ");
 
	foreach ($rowUserId as $row){
		
		$endereco = $row->billing_address_1;
		$cidade   = $row->billing_city;
		$cep      = $row->billing_postcode;
		$estado   = $row->billing_state;
		$fone     = $row->billing_phone;
    }
    
	if(isset($_GET['complete'])){
		//Envia e-mail de confirmação de cadastro
		//Função para chamar o e-mail correspondente
        
        //pega cpf do médico
        $cpf_medico = get_user_meta($user_id, "billing_cpf", "true");
		
		$trProduto = "<tr><th style='width: 20% !important;text-align: left;border-bottom: 1px solid #000;'>ID</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Nome</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Quantidade</th></tr>";
		
		foreach($rows as $row){
			
			$id_produto = $row->id_produto;
			$cepa = $row->post_title;
			$qtd = $row->qtd_produto;
			
			$trProduto .= "<tr>
                        <td style='width: 40px !important;'>". $id_produto ."</td>
                        <td style='width: 40px !important;'>". $cepa ."</td>
                        <td style='width: 40px !important;'>". $qtd ."</td>
                    </tr>";
			
		};
		
		$thProduto = "<tr><th style='width: 20% !important;text-align: left;border-bottom: 1px solid #000;'>Fórmula</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Frete</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Total</th></tr>";
		
		$trTotal .= "<tr>
                        <td style='width: 40px !important;'> R$ ". 123 ."</td>
                        <td style='width: 40px !important;'> R$ ". 123 ."</td>
                        <td style='width: 40px !important;'> R$ ". 414 ."</td>
                    </tr>";
		
		$menssage = '
	
        <center>
            <img width="200px" src="http://loja.ingridix.com.br/wp-content/uploads/2017/07/ingridix-logo.png">
        </center>
                
        <table style="width: 100%">
            <thead>
           
            </thead>
            <tbody>
            <tr>
                <p style="font-size: 16px;font-weight: bolder;margin: 20px 0 0 0">Ordem id: '. $id_referencia .'</p>
                <p style="font-size: 16px;font-weight: bolder;margin: 20px 0 0 0">Médico</p>
                <p style="font-size: 15px;margin-left: 30px;" >Nome: '. $current_user->user_login .'</p>
                <p style="font-size: 15px;margin-left: 30px;">CPF: '. $cpf_medico .'</p>
                <p style="font-size: 15px;margin-left: 30px;">CRM: '. get_user_meta(  $current_user->id , 'crm', true ) .'</p>
                <p style="font-size: 15px;margin-left: 30px;">Estado: '. get_user_meta(  $current_user->id , 'billing_state', true ) .'</p>
               
                <p style="font-size: 16px;font-weight: bolder;margin: 20px 0 0 0">Paciente</p>
                <p style="font-size: 15px;margin-left: 30px;">Nome: '. $rows[0]->nome_paciente .'</p>
                <p style="font-size: 15px;margin-left: 30px;">CPF: '. $rows[0]->cpf_paciente .' </p>
                <p style="font-size: 15px;margin-left: 30px;">Telefone: '. $rows[0]->cel_paciente .'</p>
                
                <p style="font-size: 16px;font-weight: bolder;margin: 20px 0 20px 0">Cepas</p>
            </tr>
            '. $trProduto . '
            </tbody>
            <tfoot></tfoot>
        </table>
        ';
		
		//$to = "andre@agenciabowie.com.br";
		$to = "luis.souza@texs.com.br";
		//$to = "lyndon.omarques@gmail.com";
		
		$subject = 'Nova receita INGRIDIX #'. $id_referencia .' ';
		$headers = 'Content-Type: text/html; charset=UTF-8';
		
		add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		function set_html_content_type() {
			return 'text/html';
		}
		
		add_filter('wp_mail_from','yoursite_wp_mail_from');
		function yoursite_wp_mail_from($content_type) {
			return 'contato@ingridix.com.br';
		}
		
		add_filter('wp_mail_from_name','yoursite_wp_mail_from_name');
		function yoursite_wp_mail_from_name($name) {
			return 'INGRIDIX';
		}
		//Envia e-mail de novo usuario
		wp_mail( $to, $subject, $menssage, $headers );
	}
}

?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .area-menu{
        background: #EAECED;
        text-align: left;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        padding: 0;
        font-size: 14px;
        font-weight: bolder;
        color: #3e3e3e;
        padding-top: 13px;
        height: 410px;
    }

    .area-menu li{
        display: block;
        padding: 1px 15px 5px 17px;

    }

    .area-menu-footer{
        background: #334C5A;
        padding: 7px 0 0 0;
        position: absolute;
        bottom: 11px;
        width: 100%;

    }

    .area-menu-footer li{
        color: #FFFFFF;
        font-size: 12px;
        padding: 1px 15px 5px 17px;
        text-decoration: underline;
    }

    .area-menu-footer li a, .area-menu-footer li a:hover{
        color: #FFFFFF !important;
    }

    .col-md-3{
        padding: 0 !important;
    }

    .area-content-list{
        margin-top:20px;
    }

    .area-content-all{
        border: 1px solid #8BB5CF;
        background: #FFFFFF;
        -webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        -moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        height: 410px;
        padding: 0;
    }

    .area-content-alert{
        background: #8BB4D0;
        color: #FFFFFF;
        font-weight: bolder;
        text-align: left;
        padding-top: 16px;
        font-size: 13px;
    }
    .area-content-alert span{
        width: 100%;
        height: auto;
        padding: 4px;
        background: #BCD1DE;
        color: #555555;
        margin-left: 10px;
    }
    .area-content-alert h2{
        color: #FFFFFF;
        font-size: 25px;
        padding: 5px;
    }
    .div-list{
        padding: 5px;
        font-size: 13px;

    }
    .div-list p{
        margin: 0;
        text-align: left;
        color: #555555 ;
    }

    .pagamento{
        height: 120px;
        border: 1px solid #82AC63;
        margin-bottom: 25px;
        padding: 30px 6px 0 6px;
    }
    .pagamento h3{
        font-size: 22px;
        color: #2A654F;
    }

    .centered {
        margin: 0 auto !important;
        float: none !important;
    }
    .no-padding{
        padding: 0;
    }
    .btn-ingridix-cli{
        width: 27.4%;
        height: 50px;
        padding: 12px 10px 8px 10px;
        border-radius: 40px;
        background: #79B8DB;
        color: #FFFFFF;
        font-weight: bolder;
        font-size: 17px;
        margin-bottom: 15px;
        cursor: pointer;
        box-shadow: inset 0px 0px 30px 10px #54a1cc;
    }
    .btn-ingridix{
        height: 35px;
        padding: 9px 10px 8px 10px;
        border-radius: 40px;
        background: #ECBB71;
        color: #FFFFFF;
        font-weight: bolder;
        font-size: 13px;
        margin-bottom: 5px;
    }

    .btn-ingridix a, .btn-ingridix a:hover{
        color: #FFFFFF;
    }
    .nome-logado{
        text-align: left;
        color: #3C505D;
        margin-bottom: 15px;
    }
    .nome-logado a, .nome-logado a:hover{
        color: #3C505D;
    }
    #wrapper > .sh-page-layout-default{
        padding: 30px 0 ;
    }

    label{
        color: #FFFFFF;
        margin-right: 15px;
    }
    .imprimir-color{
        background: #384C59;
    }
    .enviar-email-color{
        background: #235997;
    }
    .efetuar-color{
        background: #84B8D7;
    }
    .list-receita li{
        color: #555555;
        font-size: 12px;
        margin-bottom: 5px;
        text-align: left;
    }
    .list-receita li span{
        float: right;
    }
    .block-cepa-content{
        background: #73818A;
        margin: 0 0 5px 0;
        padding: 4px;
    }
    .content-count{
        font-weight: bolder;
        line-height: 16px;
        margin-top: 6px;
        font-size: 12px;
        color: #FFFFFF;
    }
    .square-count{
        background: #FFFFFF;
        font-size: 32px;
        margin: 0;
        padding: 0;
        color: #555555;
    }
    .receitas{
        padding-left: 0;
        margin-top: 10px;
        text-align: left;

    }
    .receitas a, .receitas a:hover{
        color: #555555;
        text-decoration: none;
        font-size: 13px;
        font-weight: bolder;
    }
    .ingrdientes-list{

        color: #292929;
    }
    .salvar-receita{
        cursor: pointer;
    }
    /**************************/
    input[type="text"]{
        padding: 0 !important;
    }
    .count-input {
        position: relative;
        width: 60px;
    }
    .count-input span{
        position: relative;
        top: -6px;
    }
    .count-input input {
        width: 20px;
        height: 20px;
        border-radius: 2px;
        text-align: center;
        color: #000000;
        background: #DBE7EE;

    }
    .count-input input:focus {
        outline: none;
    }
    .count-input .incr-btn {
        display: block;
        position: absolute;
        width: 20px;
        height: 20px;
        font-size: 20px;
        font-weight: 300;
        text-align: center;
        line-height: 30px;
        top: 15px;
        right: 0;
        margin-top: -15px;
        text-decoration:none;
        background: #8BB5D0;
    }
    .count-input .incr-btn:first-child {
        right: auto;
        left: 0;
    }
    .count-input.count-input-sm {
        max-width: 125px;
    }
    .count-input.count-input-sm input {
        height: 36px;
    }
    .count-input.count-input-lg {
        max-width: 200px;
    }
    .count-input.count-input-lg input {
        height: 70px;
        border-radius: 3px;
    }

    .ingrdientes-list div:nth-child(2){
        text-align: left;
        padding-left: 10px;
    }

    .area-menu a, .area-menu a:hover{
        color: #3e3e3e;
        text-align: none;
    }

    .active{
        color: #337ab7 !important;
    }

    /* Breakpoints */
    @media only screen and (min-width: 320px) and (max-width: 620px) {

        .col-md-9{
            padding: 0 5px 0 7px;
        }

        .area-content-all{
            height: auto;
        }

        .area-content-list{
            padding: 0;
        }
        .div-list {
            padding: 0 !important;
        }

        .td_content2 {
            background-color: #F5F5F5;
            padding: 6px;
            color: #56A235;
            font-size: 10px;
        }

        .area-menu{
            padding-bottom: 20px;
            height: auto;
        }
        .nome-logado{
            display: inline-block;
        }

        .btn-ingridix-cli{
            width: 100% !important;
        }
        .btn-ingridix-cli2{
            margin-bottom: 20px;
        }

        .centered{
            text-align: left;
        }

        .cpf-span1{

            display: inline-block;
            width: 150px !important;
        }

        .cpf-span2{
            width: 92% !important;
            padding-right: 94px !important;
        }

        .p-hidden1{
            display: inline;
        }
        .p-hidden2{
            display: none;
        }
        .p-hidden3{
            display: none;
        }

        .div-list {
            margin-bottom: 30px;
            margin-top: 20px;
        }

        .area-content-alert{
            padding-bottom: 1px;
        }
    }

    @media only screen and (min-width: 621px) and (max-width: 940px) {

        .td_content2 {
            background-color: #F5F5F5;
            padding: 6px;
            color: #56A235;
            font-size: 10px;
        }

        .area-menu{
            padding-bottom: 20px;
            height: auto;
        }

        .btn-ingridix-cli{
            width: 100% !important;
        }

        .form-cpf{
            margin-top: 15px;
        }

        .error{
            width: 100%;
        }

        .area-content-all{
            width: 100%;
            height: auto;
        }

        .p-hidden1{
            display: none;
        }
        .p-hidden2{
            display: inline;
        }
        .p-hidden3{
            display: none;
        }

        .div-list {
            margin-bottom: 30px;
            margin-top: 20px;
        }

        .cpf-span2{
            width: 92% !important;
            padding-right: 207px !important;
        }

        .cpf-span1{

            display: inline-block;
            width: 263px !important;
        }

        .nome-logado{
            margin-bottom: 0;
        }

        .area-content-alert{
            padding-bottom: 1px;
        }
    }

    @media only screen and (min-width: 941px) and (max-width: 1260px) {
        .p-hidden1{
            display: none;
            text-align: right;
        }
        .p-hidden2{
            display: none;
            text-align: right;
        }
        .p-hidden3{
            display: inline;
            text-align: right;
        }

        .cpf-span1{

            display: inline-block;
            width: 263px !important;
        }
    }

    @media only screen and (min-width: 1261px) {
        .p-hidden1{
            display: none;
            text-align: right;
        }
        .p-hidden2{
            display: none;
            text-align: right;
        }
        .p-hidden3{
            display: inline;
            text-align: right;
        }

        .cpf-span1{

            display: inline-block;
            width: 337px !important;
        }
    }
</style>
<center>
    <div class="container">

        <div class="row">

            <div class="col-md-11 centered">

                <div class="col-md-11 no-padding">
                    <div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área médico" onclick='location.href="<?= get_site_url().'/area-medico'?>"'>INGRIDIX LAB</div>
                </div>
                <div class="col-md-3 nome-logado">
                    Olá <b> <?= $current_user->user_login; ?></b>
                </div>
                <div class="col-md-8 nome-logado">
                    <b><a >&nbsp;&nbsp;&nbsp;</a></b>
                </div>
                <div class="col-md-3">
                    <ul class="area-menu list-unstyled">
                        <li><a href="<?= get_site_url().'/cadastrar-novo-paciente'?>">CADASTAR NOVO PACIENTE</a></li>
                        <li><a data-toggle="modal" data-target="#novareceitaModal" data-whatever="@mdo">CRIAR NOVA FÓRMULA</a></li>
                        <li><a href="<?= get_site_url().'/area-medico'?>">RECEITAS ANTERIORES</a></li>
                        <li><a href="http://ingridix.com.br/duvidas-sobre-o-uso-profissionais" target="_blank">DÚVIDAS SOBRE O USO</a></li>
                        <li><a href="<?= get_site_url().'/minhas-receitas'?>">MINHAS RECEITAS</a></li>
                        <li><br></li>


                        <div class="area-menu-footer">
                            <li><a href="<?= get_site_url()?>/meu-cadastro-medico">MEUS DADOS DE CADASTRO</a></li>
                            <li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
                        </div>
                    </ul>
                </div>

                <div class="col-md-9 area-content">
                    <div class="col-md-12 area-content-all pull-right">
                        <div class="col-md-12 area-content-alert">
                            <div class="form-group col-md-7">
                                PACIENTE <span class="cpf-span1" style=""><?= $rows[0]->nome_paciente?></span>
                            </div>
                            <div class="form-group col-md-4 ">
                                CPF <span class="cpf-span2" style="padding-right: 40px;"><?= $rows[0]->cpf_paciente?></span>
                            </div>
                        </div>

                        <div class="col-md-12 area-content-list">
                            <div class="col-md-4 ">
                                <div class="pagamento ">
                                    <h3>RECEITA ENVIADA COM SUCESSO!</h3>
                                </div>
                                <div id="imprimir" class="button button-primary button-large btn-ingridix imprimir-color">IMPRIMIR RECEITA</div>
                                <!--<div class="button button-primary button-large btn-ingridix imprimir-color">SALVAR COMO PDF</div>
                                <div class="button button-primary button-large btn-ingridix efetuar-color">ENVIAR PARA PACIENTE</div>-->
                                <div class="button button-primary button-large btn-ingridix enviar-email-color"><a href="<?= get_site_url().'/nova-formula/';?>">FAZER NOVA RECEITA</a></div>
                            </div>
                            <div class="col-md-8 div-list">
                                <div class="col-md-12" style="margin-bottom: 10px">
                                    <p><b>Composto probiótico:</b> 30 cápsulas contendo:</p>
                                    <p>Cada cápsula contém:</p>
                                </div>
                                <div class="col-md-12 ">
                                    <ul class="list-unstyled list-receita">
										
										<?php
										
										foreach($rows as $row):
											
											$cepa = $row->post_title;
											$lenghtCepa = strlen($cepa);
											$qtdDot = 120 - $lenghtCepa;
											
											?>

                                            <li class="li-ponto"><?php echo $row->post_title;
												
												?>
                                                <span class="pull-right"><?= "&nbsp". $row->qtd_produto; ?> BHL/UFC</span>

                                                <b class="p-hidden1 pull-right"> ..... </b>
                                                <b class="p-hidden2 pull-right"> ............................................</b>
                                                <b class="p-hidden3 pull-right"> ....................................................................................</b>

                                            </li>
											
											<?php
										endforeach;
										?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</center>


<div class="imprimir-content" style="display: none">

    <h2>Receita Ingridix</h2>

    <p><b>Dr. <?= $current_user->user_login; ?></b></p>
    <p><b>Paciente: <?= $rows[0]->nome_paciente?></b></p>

    <h3>Composto probiótico: 30 cápsulas contendo:</h3>
    <p>Cada cápsula contém:</p>
	
	<?php
	foreach($rows as $row):
		
		$cepa = $row->post_title;
		$lenghtCepa = strlen($cepa);
		$qtdDot = 120 - $lenghtCepa;
		
		?>

        <p class="li-ponto"><?php echo $row->post_title;
			
			?>

            <span style="float: right;"> <b class="p-hidden3 "> .................................................................................................................</b> <?= "&nbsp". $row->qtd_produto; ?> BHL/UFC</span>

        </p>
		
		<?php
	endforeach;
	?>
</div>



<?php
get_footer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(function(){


        $(".salvar-receita").click(function(){

            location.href="<?= get_site_url().'/receita-salva'?>";

        });

        $(".incr-btn").on("click", function (e) {
            var $button = $(this);
            var oldValue = $button.parent().find('.quantity').val();
            $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
            if ($button.data('action') == "increase") {
                var newVal = parseFloat(oldValue) + 1;
            } else {
                // Don't allow decrementing below 1
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                    $button.addClass('inactive');
                }
            }
            $button.parent().find('.quantity').val(newVal);
            e.preventDefault();
        });

        $('#imprimir').bind('click',function() {

            myWindow=window.open('','','width=700,height=500');

            var content = $(".imprimir-content").html();

            myWindow.document.write("" + content + "");


            myWindow.document.close(); //missing code


            myWindow.focus();
            myWindow.print();

        });

    })
</script>
