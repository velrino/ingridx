<?php
/*
Template Name: Ver receita
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}
global $wpdb;
get_header();
$current_user = wp_get_current_user();

//Dados do paciente
$id_receita    = $_GET['idr'];
$id_referencia = $_GET['idrr'];

$user_id    = $current_user->ID;
$rows = $wpdb->get_results( "select DISTINCT mr.id_produto, wp.post_title, mr.qtd_produto from receitas_medicos rm join minhas_receitas mr join referencia_receitas rr join wp_posts wp
where rm.id_receita = mr.id_receita_med and mr.id_ref = rr.id_ref
      and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_receita" );

//Pegar o nome da receita
$rowsNomeReceita = $wpdb->get_results("select nome_receita from receitas_medicos where id_receita = $id_receita");
$receitanome = $rowsNomeReceita[0]->nome_receita;

?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<style>
	.area-menu{
		background: #EAECED;
		text-align: left;
		border-top-left-radius: 20px;
		border-top-right-radius: 20px;
		padding: 0;
		font-size: 14px;
		font-weight: bolder;
		color: #3e3e3e;
		padding-top: 13px;
		height: 410px;
	}
	
	.area-menu li{
		display: block;
		padding: 1px 15px 5px 17px;
	}

    .area-menu li a{
        cursor: pointer;
    }
    .area-menu li a:hover{
        cursor: pointer;
        color: #337ab7 !important;
    }
    
	.area-menu-footer{
		background: #334C5A;
		padding: 7px 0 0 0;
		position: absolute;
		bottom: 11px;
		width: 100%;
		
	}

    .area-menu-footer li{
        color: #FFFFFF;
        font-size: 12px;
        padding: 1px 15px 5px 17px;
        text-decoration: underline;
    }

    .area-menu-footer li a, .area-menu-footer li a:hover{
        color: #FFFFFF !important;
    }
	
	.col-md-3{
		padding: 0 !important;
	}
	
	.area-content-list{
		margin-top:20px;
	}
	
	.area-content-all{
		border: 1px solid #8BB5CF;
		background: #FFFFFF;
		-webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
		-moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
		box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
		height: 410px;
		padding: 0;
	}
	
	.area-content-alert{
		background: #8BB4D0;
		color: #FFFFFF;
		font-weight: bolder;
		text-align: left;
		padding-top: 16px;
		font-size: 13px;
		color: #FFFFFF;
	}
	.area-content-alert .busca_paciente{
		height: auto;
		background: #BCD1DE;
		color: #555555;
		margin-left: 10px;
		margin-top: -5px;
	}
	
	.area-content-alert div input,.area-content-alert div input:focus{
		margin: 0;
		padding: 0 !important;
		height: 25px;
		width: 360px;
		background: transparent;
		outline: none;
		border: none;
		color: #555555;
		padding-left: 5px !important;
		text-transform: uppercase;
	}
	
	.area-content-alert #cpf_paciente{
		width: 200px;
		height: 25px;
		background: #BCD1DE;
		color: #555555;
		margin-left: 10px;
		margin-top: -5px;
		padding: 4px;
	}
	
	.area-content-alert ul{
		position: absolute;
		z-index: 999;
		background: #fff;
		padding: 0;
		left: 87px;
		top: 20px;
	}
	
	.area-content-alert ul li{
		padding: 10px 20px 10px 20px;
		color: #555555;
		list-style-type: none;
		display: block;
		cursor: pointer;
	}
	
	.area-content-alert ul li:hover{
		background: #73818a;
		color: #FFFFFF;
	}
	
	.area-content-alert h2{
		color: #FFFFFF;
		font-size: 25px;
		padding: 5px;
	}
	.div-list{
		padding: 5px;
		font-size: 13px;
		
	}
	.div-list p{
		margin: 0;
		text-align: left;
		color: #555555 ;
	}
	
	.pagamento{
		height: 120px;
		border: 1px solid #82AC63;
		margin-bottom: 25px;
		padding: 0 6px 0 6px;
	}
	
	.centered {
		margin: 0 auto !important;
		float: none !important;
	}
	.no-padding{
		padding: 0;
	}
	.btn-ingridix-cli{
		width: 27.4%;
		height: 50px;
		padding: 12px 10px 8px 10px;
		border-radius: 40px;
		background: #79B8DB;
		color: #FFFFFF;
		font-weight: bolder;
		font-size: 17px;
		margin-bottom: 15px;
        box-shadow: inset 0px 0px 30px 10px #54a1cc;
	}
	.btn-ingridix{
		height: 35px;
		padding: 9px 10px 8px 10px;
		border-radius: 40px;
		background: #ECBB71;
		color: #FFFFFF;
		font-weight: bolder;
		font-size: 13px;
		margin-bottom: 5px;
	}
	.nome-logado{
		text-align: left;
		color: #3C505D;
		margin-bottom: 15px;
	}
	.nome-logado a, .nome-logado a:hover{
		color: #3C505D;
	}
	#wrapper > .sh-page-layout-default{
		padding: 30px 0 ;
	}
	
	label{
		color: #FFFFFF;
		margin-right: 15px;
	}
	.imprimir-color{
		background: #00664C;
	}
	.enviar-email-color{
		background: #8C2300;
	}
	.efetuar-color{
		background: #5C8F34;
	}
	.list-receita li{
		color: #555555;
		font-size: 12px;
		margin-bottom: 5px;
        text-align: left;
	}

    .list-receita li span{
        float: right;
    }
	
	.block-cepa-content{
		background: #73818A;
		margin: 0 0 5px 0;
		padding: 4px;
	}
	.content-count{
		font-weight: bolder;
		line-height: 16px;
		margin-top: 6px;
		font-size: 12px;
		color: #FFFFFF;
	}
	.square-count{
		background: #FFFFFF;
		font-size: 26px;
		margin: 0;
		padding: 0;
		color: #555555;
	}
	.receitas{
		padding-left: 0;
		margin-top: 10px;
		text-align: left;
		
	}
	.receitas a, .receitas a:hover{
		color: #555555;
		text-decoration: none;
		font-size: 13px;
		font-weight: bolder;
	}
	.ingrdientes-list{
		
		color: #292929;
	}
	.salvar-receita{
		cursor: pointer;
        text-align: center;
	}
	/**************************/
	input[type="text"]{
		padding: 0 !important;
	}
	.count-input {
		position: relative;
		width: 65px;
	}
	.count-input span{
		position: relative;
		top: -6px;
	}
	.count-input input {
		width: 25px;
		height: 20px;
		border-radius: 2px;
		text-align: center;
		color: #000000;
		background: #DBE7EE;
		
	}
	.count-input input:focus {
		outline: none;
	}
	.count-input .incr-btn {
		display: block;
		position: absolute;
		width: 20px;
		height: 20px;
		font-size: 20px;
		font-weight: 300;
		text-align: center;
		line-height: 30px;
		top: 15px;
		right: 0;
		margin-top: -15px;
		text-decoration:none;
		background: #8BB5D0;
	}
	.count-input .incr-btn:first-child {
		right: auto;
		left: 0;
	}
	.count-input.count-input-sm {
		max-width: 125px;
	}
	.count-input.count-input-sm input {
		height: 36px;
	}
	.count-input.count-input-lg {
		max-width: 200px;
	}
	.count-input.count-input-lg input {
		height: 70px;
		border-radius: 3px;
	}
	
	.ingrdientes-list div:nth-child(2){
		text-align: left;
		padding-left: 10px;
	}
	
	.area-menu a, .area-menu a:hover{
		color: #3e3e3e;
		text-align: none;
	}
	
	.active{
		color: #337ab7 !important;
	}
	
	.inline-flex{
		display: inline-flex;
	}
	
	.hidden{
		display: none;
	}
	
	.limpar-receita{
		cursor: pointer;
	}
    
    .editar-cepa{
        background: #79B8DB;
        color: #ffffff;
    }

    .editar-cepa:hover{
        background: #00664C;
        color: #FFFFFF;
    }
    
    /* Breakpoints */
    @media only screen and (min-width: 320px) and (max-width: 620px) {

        .col-md-9{
            padding: 0 5px 0 7px;
        }

        .area-content-all{
            height: auto;
        }

        .area-content-list{
            padding: 0;
        }
        .div-list {
            padding: 0 !important;
        }

        .td_content2 {
            background-color: #F5F5F5;
            padding: 6px;
            color: #56A235;
            font-size: 10px;
        }

        .area-menu{
            padding-bottom: 20px;
            height: auto;
        }
        .nome-logado{
            display: inline-block;
        }

        .btn-ingridix-cli{
            width: 100% !important;
        }
        .btn-ingridix-cli2{
            margin-bottom: 20px;
        }

        .centered{
            text-align: left;
        }

        .cpf-span1{
            padding-right: 85px !important;
        }

        .cpf-span2{
            width: 92% !important;
            padding-right: 94px !important;
        }

        .p-hidden1{
            display: inline;
        }
        .p-hidden2{
            display: none;
        }
        .p-hidden3{
            display: none;
        }

        .div-list {
            margin-bottom: 30px;
            margin-top: 20px;
        }

        .area-content-alert{
            padding-bottom: 1px;
        }

        .area-content-alert div input{
            width: 163px !important;
        }

        .square-count{
            text-align: center;
        }
    }

    @media only screen and (min-width: 621px) and (max-width: 940px) {

        .td_content2 {
            background-color: #F5F5F5;
            padding: 6px;
            color: #56A235;
            font-size: 10px;
        }

        .area-menu{
            padding-bottom: 20px;
            height: auto;
        }

        .btn-ingridix-cli{
            width: 100% !important;
        }

        .form-cpf{
            margin-top: 15px;
        }

        .error{
            width: 100%;
        }

        .area-content-all{
            width: 100%;
            height: auto;
        }

        .p-hidden1{
            display: none;
        }
        .p-hidden2{
            display: inline;
        }
        .p-hidden3{
            display: none;
        }

        .div-list {
            margin-bottom: 30px;
            margin-top: 20px;
        }

        .cpf-span2{
            width: 92% !important;
            padding-right: 207px !important;
        }

        .area-content-alert{
            padding-bottom: 1px;
        }

        .area-content-alert .busca_paciente{
            width: 313px;
        }
        .area-content-alert #cpf_paciente{
            width: 350px;
        }
        .square-count{
            text-align: center;
        }
    }

    @media only screen and (min-width: 941px) and (max-width: 1260px) {
        .p-hidden1{
            display: none;
            text-align: right;
        }
        .p-hidden2{
            display: none;
            text-align: right;
        }
        .p-hidden3{
            display: inline;
            text-align: right;
        }
    }

    @media only screen and (min-width: 1261px) {
        .p-hidden1{
            display: none;
            text-align: right;
        }
        .p-hidden2{
            display: none;
            text-align: right;
        }
        .p-hidden3{
            display: inline;
            text-align: right;
        }
    }
</style>
<center>
	<div class="container">
		
		<div class="row">
			
			<div class="col-md-11 centered">
				
				<div class="col-md-11 no-padding">
					<div class="button button-primary button-large btn-ingridix-cli pull-left" style="width: 27.4%">INGRIDIX LAB</div>
				</div>
				<div class="col-md-3 nome-logado">
					Olá <b> <?= $current_user->user_login; ?></b>
				</div>
				<div class="col-md-8 nome-logado">
					<b><a >&nbsp;&nbsp;&nbsp;</a></b>
				</div>
				<div class="col-md-3">
					<ul class="area-menu list-unstyled">
						<li><a href="<?= get_site_url().'/cadastrar-novo-paciente'?>">CADASTAR NOVO PACIENTE</a></li>
                        <li><a data-toggle="modal" data-target="#novareceitaModal" data-whatever="@mdo">CRIAR NOVA FÓRMULA</a></li>
						<li><a href="<?= get_site_url().'/area-medico'?>">RECEITAS ANTERIORES</a></li>
                        <li><a href="http://ingridix.com.br/duvidas-sobre-o-uso-profissionais" target="_blank">DÚVIDAS SOBRE O USO</a></li>
						<li><a class="active" href="<?= get_site_url().'/minhas-receitas'?>">MINHAS RECEITAS</a></li>
						<li><br></li>
						
						
						<div class="area-menu-footer">
                            <li><a href="<?= get_site_url()?>/meu-cadastro-medico">MEUS DADOS DE CADASTRO</a></li>
                            <li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
                        </div>
					</ul>
				</div>
				
				<div class="col-md-9 area-content">
					<div class="col-md-12 area-content-all ">
						<div class="col-md-12 area-content-alert">
							<div class="form-group col-md-7 inline-flex">
								PACIENTE <div id="busca_paciente" class="busca_paciente"><input id="get_paciente" type="text" value=""></div>
								<ul class="hidden">
								
								</ul>
							</div>
							<div class="form-group col-md-4 inline-flex">
								CPF <div id="cpf_paciente" ></div>
							</div>
						</div>
						
						<div class="col-md-12  pull-right" style="padding: 0">
							<div class="col-md-12 area-content-list">
								<div class="col-md-4 ">
									<div class="col-md-12 " style="padding-left: 0">
										<div class="col-md-12 block-cepa-content">
											<div class="col-md-9 content-count">CONTAGEM DE UFC</div>
											<div class="col-md-3 square-count">0</div>
										</div>
									</div>
									
									<div class="col-md-12 " style="padding-left: 0; margin-top: 20px">
										<div class="button button-primary button-large btn-ingridix imprimir-color salvar-receita">SALVAR RECEITA</div>
                                        <div class="button button-primary button-large btn-ingridix enviar-email-color limpar-receita">LIMPAR RECEITA</div>
									</div>

                                    <div class="col-md-12" style="padding-left: 0; margin-top: 20px">
                                        <label for="editar-cepa" class="control-label"style="color: #555555;float: left;">Dê um nome à sua receita</label>
                                        <div class="input-group col-md-12">
                                          
                                            <input type="text" class="form-control" id="editar-cepa" placeholder="Editar cepa" style=" padding-left: 5px !important;" value="<?=  empty($receitanome) ? "CEPA_".$id_receita : $receitanome; ?>">
                                            <span class="input-group-btn">
                                                    <button class="btn btn-default editar-cepa" id="btn-editar-cepa" type="button">Renomear</button>
                                            </span>
                                        </div><!-- /input-group -->
                                    </div><!-- /.col-lg-6 -->
									
								</div>
								<div class="col-md-8 div-list" style="padding: 0">
									<div class="col-md-12" style="margin-bottom: 10px">
										<p><b>Composto probiótico:</b> 30 cápsulas contendo:</p>
										<p>Cada cápsula contém:</p>
									</div>
									<div class="col-md-12 ">
										<ul class="list-unstyled list-receita">
											
											<?php
											
											foreach($rows as $row):
												
												$cepa = $row->post_title;
												$lenghtCepa = strlen($cepa);
												$qtdDot = 120 - $lenghtCepa;
												
												?>

                                                <li class="li-ponto"><?php echo $row->post_title;
													
													?>
                                                    <span class="pull-right"><?= "&nbsp". $row->qtd_produto; ?> BHL/UFC</span>

                                                    <b class="p-hidden1 pull-right"> ..... </b>
                                                    <b class="p-hidden2 pull-right"> ............................................</b>
                                                    <b class="p-hidden3 pull-right"> ....................................................................................</b>

                                                    <input type="hidden" class="quantity" data-cepa-id="<?= $row->id_produto;?>" value="<?= $row->qtd_produto;?>">

                                                </li>
                                                
                                                
												<?php
											endforeach;
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</center>

<?php
get_footer();
?>

<script>
    $(function(){

        $('[data-toggle="tooltip"]').tooltip();
        var total = 0;
        $(".list-receita li").each(function(){

            var qtd = $(this).find("input").val();

            total += parseFloat(qtd);

        });

        $(".square-count").text(total);
        $("#qtd_capsulas").text(total);
        
        //Salvar Receita
        $(".salvar-receita").click(function(){

            if($("#cpf_paciente").text().length <= 0){
                alert("Você deve selecionar um paciente antes de seguir.");
            }else{

                var idpaciente = $("#get_paciente").attr('data-id-paciente');
                var user_id = <?= $current_user->ID ;?>;
                var id_minha_receita = <?= $id_receita; ?>;
                
                $.ajax({
                    url: '<?= get_site_url()."/functions-ajax";?>',
                    type: "post",
                    data: {id_medico : user_id, id_minha_receita : id_minha_receita, idpaciente : idpaciente, status_minha_receita : "ok"},
                    dataType: "json",
                    success: function(data){

                        var id_ref = data;
                        
                        //Envia para função ajax, adiciona nova receita ao medico

                        $.ajax({
                            url: '<?= get_site_url() . "/functions-ajax";?>',
                            type: "post",
                            data: {
                                id_medico:              user_id,
                                idpaciente:             idpaciente,
                                repetir_receita_medico: "ok"
                            },
                            dataType: "json",
                            success: function (data) {

                                $(".list-receita li").each(function(){
                                    var qtdProdutoUnit = parseFloat($(this).find(".quantity").val());
                                    var idProdutoCepa  = $(this).find(".quantity").attr("data-cepa-id");

                                    //Envia para função ajax
                                    $.ajax({
                                        url: '<?= get_site_url()."/functions-ajax";?>',
                                        type: "post",
                                        data: {id_medico : user_id, id_minha_receita : id_minha_receita, idpaciente : idpaciente, id_ref : id_ref, idProduto : idProdutoCepa, qtdProdutoUnit : qtdProdutoUnit, add_minha_receita : "ok"},
                                       
                                        success: function(data){

                                            //apagar done alert(data)

                                        },
                                        error: function(){
                                            //  alert('errorr')
                                        }
                                    });
                                });
                                setInterval(function(){
                                    location.href="<?= get_site_url().'/receita-salva/?idr='?>"+ id_minha_receita +"&idp="+ idpaciente +" ";
                                },6000)
                            },
                            error: function(){
                            
                            }
                        });
                    },
                    error: function(){
                          alert('Não foi possível salvar!')
                    }
                });
            }
        });
        
        $("#get_paciente").keyup(function(){

            var input = $(this).val();

            $.ajax({
                url: '<?= get_site_url()."/functions-ajax";?>',
                type: "post",
                data: {nome : input, selecionar_paciente : "ok"},
                dataType: 'json',
                success: function(data){

                    $("#get_paciente").attr('value', input);

                    if(data.length >= 1){

                        $(".area-content-alert ul li").remove();

                        for(var i = 0; i < data.length; i++){

                            $('<li id="pacienteId-'+data[i].id_paciente+'" data-id-paciente="'+data[i].id_paciente+'" data-nome-paciente="'+data[i].nome_paciente+'" data-cpf-paciente="'+data[i].cpf_paciente+'" onclick="nome_paciente(id)" >'+data[i].nome_paciente+'</li>').appendTo(".area-content-alert ul")
                            $(".area-content-alert ul").removeClass('hidden');
                        }

                    }else{
                        $("#cpf_paciente").text('');
                        $(".area-content-alert ul").addClass('hidden');
                        $(".area-content-alert ul li").remove();
                    }

                    if(input.length <= 0){
                        $("#cpf_paciente").text('');
                        $(".area-content-alert ul").addClass('hidden');
                        $(".area-content-alert ul li").remove();
                    }

                }
            })
            return false;
        });

        $("#btn-editar-cepa").click(function () {

            var input_editar_receita = $("#editar-cepa").val();

            if(input_editar_receita.length <= 0 ){
                alert("Insira um nome na receita para editar!");
            }else{

                var id_minha_receita = <?= $id_receita; ?>;

                $.ajax({
                    url: '<?= get_site_url()."/functions-ajax";?>',
                    type: "post",
                    data: { nome_receita : input_editar_receita, id_minha_receita : id_minha_receita, editar_receita : "ok"},
                    success: function(data){
                       
                        if(data = "done"){
                            alert('Nome da receita editado com sucesso!')
                        }else{
                            alert('Não foi possível editar!')
                        }

                    },
                    error: function(){
                        alert('Não foi possível editar!')
                    }
                });
            }
            
        });
        
        $(".limpar-receita").click(function(){
           
            $("#get_paciente").val('');
            $("#cpf_paciente").text('');
            
        });
    })

    function nome_paciente(id){

        var nome = $("#"+id).attr('data-nome-paciente');
        var cpf = $("#"+id).attr('data-cpf-paciente');
        var idpaciente = $("#"+id).attr('data-id-paciente');

        $("#get_paciente").val(nome).attr('data-id-paciente', idpaciente);
        $("#cpf_paciente").text(cpf);
        $(".area-content-alert ul").addClass('hidden');
        $(".area-content-alert ul li").remove();
    }
</script>
