<?php
/*
Template Name: Meu cadastro paciente
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}
get_header();
$current_user = wp_get_current_user();
$cpf_paciente = get_user_meta(  $current_user->id , 'billing_cpf', true );

$user_dados = get_user_meta($current_user->ID);

$arr = explode(",", get_user_meta(  $current_user->id , 'billing_address_1', true ));
$rua = $arr[0];
$numero = $arr[1];
?>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<style>
		.area-menu{
			background: #EAECED;
			text-align: left;
			border-top-left-radius: 20px;
			border-top-right-radius: 20px;
			padding: 0;
			font-size: 14px;
			font-weight: bolder;
			color: #3e3e3e;
			padding-top: 13px;
			height: 400px;
		}
		
		.area-menu li{
			display: block;
			padding: 1px 15px 5px 17px;
			
		}
		
		.area-menu-footer{
			background: #B3956B;
			padding: 7px 0 0 0;
			position: absolute;
			bottom: 11px;
			width: 100%;
			
		}
		
		.col-md-3{
			padding: 0 !important;
		}
		
		.area-content-list{
			margin-top:20px;
		}
		
		.area-content-all{
			border: 1px solid #E6BA7C;
			background: #FFFFFF;
			-webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			-moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
			height: 400px;
			padding-top: 15px
		}
		
		.area-content-alert{
		//  background: #A5C48E;
		
		}
		.area-content-alert h2{
			color: #555555;
			font-size: 25px;
			padding: 10px;
		}
		.div-list{
			background: #F5F5F5;
			
			font-size: 13px;
			font-weight: bolder;
		}
		.centered {
			margin: 0 auto !important;
			float: none !important;
		}
		.no-padding{
			padding: 0;
		}
		.btn-ingridix-cli{
			width: 27.4%;
			height: 50px;
			padding: 12px 10px 8px 10px;
			border-radius: 40px;
			background: #ECBB71 !important;
			color: #FFFFFF;
			font-weight: bolder;
			font-size: 17px;
			margin-bottom: 15px;
			cursor: pointer;
            box-shadow: inset 0px 0px 30px 10px #e89f2f;
		}
		
		.btn-ingridix-cli-opt{
			width: 32%;
			height: 75px;
			padding: 12px 10px 8px 10px;
			border-radius: 40px;
			background: #ECBB71 !important;
			color: #FFFFFF;
			font-weight: bolder;
			font-size: 17px;
			margin-bottom: 15px;
			margin-left: 5px;
			cursor: pointer;
		}
		
		.nome-logado{
			text-align: left;
			color: #555555;
			margin-bottom: 15px;
		}
		#wrapper > .sh-page-layout-default{
			padding: 30px 0 ;
		}
		
		.area-menu li a{
			color: #555555 !important;
		}
		.area-menu li a:hover{
			color: #ECBB71 !important;
		}
		
		.area-menu-footer li a, .area-menu-footer li a:hover{
			color: #FFFFFF !important;
			font-size: 12px;
			text-decoration: underline;
		}

        .error{
            color: #d60707;
            font-size: 13px;
        }
		/* Breakpoints */
		@media only screen and (min-width: 320px) and (max-width: 620px) {
			
			.col-md-9{
				padding: 0 5px 0 7px;
			}
			
			.area-content-all{
				height: auto;
				padding-top: 0;
			}
			
			.area-content-list{
				padding: 0;
			}
			
			.area-content-alert h2{
				font-size: 16px;
				padding: 0;
			}
			
			.area-menu{
				padding-bottom: 20px;
				height: auto;
			}
			
			.btn-ingridix-cli-opt{
				width: 100%;
				height: 68px;
				margin-left: 0;
				padding: 12px 10px 13px 10px
			}
			
			.btn-ingridix-cli{
				width: 100% !important;
			}
		}
		
		@media only screen and (min-width: 621px) and (max-width: 940px) {
			
			.area-content-all{
				height: auto;
			}
			
			.area-menu{
				padding-bottom: 20px;
				height: auto;
			}
			
			.area-content-alert h2{
				font-size: 21px;
				padding: 0;
			}
			
			.btn-ingridix-cli-opt{
				width: 100%;
				height: 55px;
				margin-left: 0;
				padding: 12px 10px 13px 10px
			}
			
			.btn-ingridix-cli{
				width: 100% !important;
			}
		}
		
		@media only screen and (min-width: 941px) and (max-width: 1260px) {
		
		}
		
		@media only screen and (min-width: 1261px) {
		
		}
	
	</style>
	<center>
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-11 centered">
					<div class="col-md-11 no-padding">
						<div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área cliente" onclick='location.href="<?= get_site_url().'/area-cliente'?>"'>ÁREA DO CLIENTE</div>
					</div>
					<div class="col-md-11 nome-logado">
						Olá <b><?= $current_user->user_login; ?></b>
					</div>
					<div class="col-md-3">
						<ul class="area-menu list-unstyled">
							<li><a href="<?= get_site_url()?>/receitas-anteriores" >RECEITAS ANTERIORES</a></li>
							<li><a href="http://ingridix.com.br/duvidas-sobre-o-uso" target="_blank">DÚVIDAS SOBRE O USO</a></li>
							<li><br></li>
							
							
							<div class="area-menu-footer">
								<li><a href="<?= get_site_url()?>/meu-cadastro-paciente">MEUS DADOS DE CADASTRO</a></li>
								<li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
							</div>
						</ul>
					</div>
					
					<div class="col-md-9 area-content">
						<div class="col-md-12 area-content-all ">

                            <div class="col-md-6" style="text-align: left; font-weight: bolder;margin-bottom: 20px;padding-left: 0;">

                                <div class="col-md-10 ">Nome completo: <?= $current_user->user_login;?></div>
                                <div class="col-md-10 " id="cpf_user">CPF: <?= get_user_meta(  $current_user->id , 'billing_cpf', true );?></div>
                                <div class="col-md-10 ">E-mail: <?= $current_user->user_email; ?></div>

                            </div>
                            
                            <form id="edit_user" method="post" class="col-md-12 form-horizontal ">
                    
                                <div class="row">

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="celular" class="col-md-3 control-label">Celular*</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="celular" name="celular" placeholder="Celular" value="<?php echo get_user_meta(  $current_user->id , 'billing_phone', true ); ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nome" class="col-md-3 control-label">CEP*</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="cep" name="cep" placeholder="CEP" value="<?php echo get_user_meta(  $current_user->id , 'billing_postcode', true ); ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="endereco" class="col-md-3 control-label">Endereço*</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço" value="<?php echo $rua; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="cpf" class="col-md-2 control-label">Nº*</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="numero" name="numero" placeholder="Nº" value="<?php echo trim($numero) ; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="nome" class="col-md-3 control-label">Complemento</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="complemento" name="complemento" placeholder="Complemento" value="<?php echo get_user_meta(  $current_user->id , 'billing_address_2', true ); ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="celular" class="col-md-2 control-label">Bairro*</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" value="<?php echo get_user_meta(  $current_user->id , 'billing_bairro', true ); ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="cidade" class="col-md-2 control-label">Cidade*</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Cidade" value="<?php echo get_user_meta(  $current_user->id , 'billing_city', true ); ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="estado" class="col-md-2 control-label">Estado*</label>
                                            <div class="col-md-8">
                                                <select name="estado" id="estado" class="state_select " autocomplete="address-level1" data-placeholder="">
                                                    <option value="">Selecione uma opção…</option>
                                                    <option value="AC" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "AC" ? "selected" : '';?>>Acre</option>
                                                    <option value="AL" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "AL" ? "selected" : '';?>>Alagoas</option>
                                                    <option value="AP" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "AP" ? "selected" : '';?>>Amapá</option>
                                                    <option value="AM" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "AM" ? "selected" : '';?>>Amazonas</option>
                                                    <option value="BA" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "BA" ? "selected" : '';?>>Bahia</option>
                                                    <option value="CE" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "CE" ? "selected" : '';?>>Ceará</option>
                                                    <option value="DF" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "DF" ? "selected" : '';?>>Distrito Federal</option>
                                                    <option value="ES" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "ES" ? "selected" : '';?>>Espírito Santo</option>
                                                    <option value="GO" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "GO" ? "selected" : '';?>>Goiás</option>
                                                    <option value="MA" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "MA" ? "selected" : '';?>>Maranhão</option>
                                                    <option value="MT" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "MT" ? "selected" : '';?>>Mato Grosso</option>
                                                    <option value="MS" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "MS" ? "selected" : '';?>>Mato Grosso do Sul</option>
                                                    <option value="MG" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "MG" ? "selected" : '';?>>Minas Gerais</option>
                                                    <option value="PA" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "PA" ? "selected" : '';?>>Pará</option>
                                                    <option value="PB" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "PB" ? "selected" : '';?>>Paraíba</option>
                                                    <option value="PR" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "PR" ? "selected" : '';?>>Paraná</option>
                                                    <option value="PE" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "PE" ? "selected" : '';?>>Pernambuco</option>
                                                    <option value="PI" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "PI" ? "selected" : '';?>>Piauí</option>
                                                    <option value="RJ" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "RJ" ? "selected" : '';?>>Rio de Janeiro</option>
                                                    <option value="RN" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "RN" ? "selected" : '';?>>Rio Grande do Norte</option>
                                                    <option value="RS" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "RS" ? "selected" : '';?>>Rio Grande do Sul</option>
                                                    <option value="RO" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "RO" ? "selected" : '';?>>Rondônia</option>
                                                    <option value="RR" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "RR" ? "selected" : '';?>>Roraima</option>
                                                    <option value="SC" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "SC" ? "selected" : '';?>>Santa Catarina</option>
                                                    <option value="SP" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "SP" ? "selected" : '';?>>São Paulo</option>
                                                    <option value="SE" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "SE" ? "selected" : '';?>>Sergipe</option>
                                                    <option value="TO" <?php echo get_user_meta(  $current_user->id , 'billing_state', true ) == "TO" ? "selected" : '';?>>Tocantins</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3" style="padding-top: 18px;">
                                                <button id="cadastrar" class="button button-primary button-large btn-ingridix-cli"  >CONCLUIR CADASTRO</button>
                                                <!--<button type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large btn-ingridix-cli"  >ACESSAR RECEITAS INGRIDIX</button>-->
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="tipo_user" value="edit_paciente">
                                </div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</center>

    <script src='<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.min.js'></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.maskedinput.min.js"></script>

    <script>
        $(function(){

            $("#cpf").mask("999.999.999-99");
            // $("#telefone").mask("(99) 9999-9999");
            $("#celular").mask("(99) 99999-9999");
            // $("#cep").mask( "99999-999" );
            $('#numero, #idade').on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

            $("#cep").focusout(function(){

                var cep = $("#cep").val();

                if(cep.length > 2){
                    $.ajax({
                        url: "https://api.postmon.com.br/v1/cep/" + cep,
                        type: "GET",
                        dataType: "json",
                        success: function(data){

                            if(data['cep']){

                                $("#endereco").val(data['logradouro']);
                                $("#bairro").val(data['bairro']);
                                $("#cidade").val(data['cidade']);
                                $("#estado").val(data['estado_info']['nome']);

                            }

                            return false;
                        },
                        error: function(){
                            alert('Insira um CEP valido!');

                            $("#cep").val("");
                            $("#endereco").val("");
                            $("#bairro").val("");
                            $("#cidade").val("");
                            $("#estado").val("");

                            return false;
                        }
                    });
                };

                return false;
            });
            
            $("#cadastrar").click(function(){

                $("#edit_user").validate({
                    // Specify validation rules
                    rules: {
                        // The key name on the left side is the name attribute
                        // of an input field. Validation rules are defined
                        // on the right side
                        celular: "required",
                        cep: "required",
                        endereco: "required",
                        numero: "required",
                        bairro: "required",
                        cidade: "required",
                        estado: "required"

                    },
                    // Specify validation error messages
                    messages: {
                        celular: "Insira um número de celular!",
                        cep: "Insira o CEP correto!",
                        endereco: "Insira o Endereço!",
                        numero: "Insira o número!",
                        bairro: "Insira o bairro!",
                        cidade: "Insira a cidade!",
                        estado: "Insira o estado!"
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function(form) {
                        form.submit();
                    }
                });

            })


        })
        
    </script>

<?php
get_footer();
?>