<?php
/**
 * Header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<?php if( jevelin_option('responsive_layout') ) : ?>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php endif; ?>

	<?php
	/* Callback for older WordPress versions */
	if( !function_exists( 'has_site_icon' ) || ! has_site_icon() ) : ?>
		<?php if( jevelin_option_image('favicon') ) : ?>
			<link rel="icon" type="image/png" href="<?php echo jevelin_option_image('favicon'); ?>">
		<?php endif; ?>
	<?php endif; ?>

	<?php wp_head(); ?>


    <!-- Start of agenciabowie Zendesk Widget script -->
    <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var e=this.createElement("script");n&&(this.domain=n),e.id="js-iframe-async",e.src="https://assets.zendesk.com/embeddable_framework/main.js",this.t=+new Date,this.zendeskHost="agenciabowie.zendesk.com",this.zEQueue=a,this.body.appendChild(e)},o.write('<body onload="document._l();">'),o.close()}();
        /*]]>*/</script>
    <!-- End of agenciabowie Zendesk Widget script -->
    <style>
        #billing_company_field, .woocommerce-additional-fields{
            display: none;
        }

        .woocommerce form .form-row-last, .woocommerce-page form .form-row-first, .woocommerce-page form .form-row-last {
            width: 100%;
            overflow: visible;
        }
        .btn-ingridix-cli:hover{
            box-shadow: none;
        }
    </style>
    
</head>
<body <?php body_class(); ?>>
<?php
	/* Get Titlebar options */
	$titlebar_style_val = jevelin_post_option( jevelin_page_id(), 'header_style' );
	$titlebar_style_val2 = ( isset( $titlebar_style_val['header_style'] ) ) ? esc_attr($titlebar_style_val['header_style']) : 'default';

	/* Inlcude page loader HTML */
	get_template_part('inc/templates/page_loader' );

	/* Header popup search */
	if( jevelin_option( 'header_search_style', 'style1' ) != 'style1' ) :
		get_template_part('inc/headers/header-search');
	endif;
?>

	<?php
		if( jevelin_header_right() ) :
			/* Inlcude right header HTML */
			get_template_part('inc/headers/header-right-1' );
		endif;
	?>

	<div id="page-container" class="<?php echo jevelin_header_page_container(); ?>">
		<?php
			/* Inlcude page notice HTML */
			get_template_part('inc/templates/notice' );

			/* Inlcude breadcrumbs HTML, if bottom titlebar style */
			if( $titlebar_style_val2 == 'bottom_titlebar' ) :
				get_template_part('inc/templates/titlebar' );
			endif;
		?>


		<?php if( jevelin_post_option( jevelin_page_id(), 'header', 'on' ) != 'off' ) : ?>
			<header class="primary-mobile<?php echo jevelin_header_mobile_style(); ?>">
				<?php /* Inlcude mobile header */
					get_template_part('inc/headers/header-mobile' );
				?>
			</header>
			<header class="primary-desktop<?php echo jevelin_header_desktop_style(); ?>">
				<?php /* Inlcude desktop header */
					get_template_part('inc/headers/header-'.jevelin_header_layout() );
				?>
			</header>
		<?php endif; ?>


		<?php
			/* Inlcude breadcrumbs HTML, if not bottom titlebar style */
			if( $titlebar_style_val2 != 'bottom_titlebar' ) :
				get_template_part('inc/templates/titlebar' );
			endif;

			$class = '';
			if( jevelin_post_option( jevelin_page_id(), 'page_layout', 'default' ) != 'full' ) {
				$class = ' sh-page-layout-default';
			} else if( jevelin_post_option( jevelin_page_id(), 'page_layout', 'default' ) == 'full' ) {
				$class = ' sh-page-layout-full';
			}

		?>


			<div id="wrapper">
				<div class="content-container<?php echo esc_attr( $class ); ?>"">
				<?php if( jevelin_post_option( jevelin_page_id(), 'page_layout', 'default' ) != 'full' ) : ?>
					<div class="container entry-content">
				<?php endif; ?>


				<?php
					/* Inlcude white borders option HTML */
					get_template_part('inc/templates/white_borders' );
				?>
