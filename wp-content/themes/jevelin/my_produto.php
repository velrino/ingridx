<?php
/*
Template Name: Área cliente Produto
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}

global $wpdb;
get_header();
$current_user = wp_get_current_user();
$cpf_paciente = get_user_meta(  $current_user->id , 'billing_cpf', true );

//Dados do paciente
$id_receita    = strip_tags(filter_var ( $_GET['idr'], FILTER_SANITIZE_NUMBER_INT));
$id_referencia = strip_tags(filter_var ( $_GET['idrr'], FILTER_SANITIZE_NUMBER_INT));
$user_id       = $current_user->ID;

$rows = $wpdb->get_results( "select DISTINCT mr.id_produto, wp.post_title, cp.nome_paciente, cp.cpf_paciente, wu.user_login, mr.id_med, wp.ID, mr.qtd_produto, rr.valor, rr.frete from receitas_medicos rm join minhas_receitas mr join cadastros_pacientes cp join wp_posts wp join wp_users wu join referencia_receitas rr
where rm.id_receita = mr.id_receita_med and mr.id_pac = cp.id_paciente and rr.id_ref = rm.id_ref and rr.id_ref = mr.id_ref
      and wu.ID = mr.id_med and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_receita" );

$cpf_medico = get_user_meta(  $rows[0]->id_med , 'billing_cpf', true );
$crm        = get_user_meta(  $rows[0]->id_med , 'crm', true );

//Verifica se existe frete para esse id refeencia
if(empty($rows[0]->frete)){
	
	$cep = get_user_meta( $user_id , 'billing_postcode', true );
	$frete      = calcula_frete('04510','05314000','' . $cep . '','1','n','0','n');
	
	$wpdb->query("UPDATE referencia_receitas set frete = $frete where id_ref = $id_referencia ");
	
}else{
	$frete = $rows[0]->frete;
}

?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <style>
        .area-menu{
            background: #EAECED;
            text-align: left;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
            padding: 0;
            font-size: 14px;
            font-weight: bolder;
            color: #3e3e3e;
            padding-top: 13px;
            height: 350px;
        }

        .area-menu li{
            display: block;
            padding: 1px 15px 5px 17px;

        }

        .area-menu-footer{
            background: #B3956B;
            padding: 7px 0 0 0;
            position: absolute;
            bottom: 11px;
            width: 100%;

        }

        .col-md-3{
            padding: 0 !important;
        }

        .area-content-list{
            margin-top:20px;
        }

        .area-content-all{
            border: 1px solid #E6BA7C;
            background: #FFFFFF;
            -webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            -moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            height: 350px;
            padding: 0;
        }

        .area-content-alert{
            background: #E0B885;
            color: #FFFFFF;
            font-weight: bolder;
            text-align: left;
            padding-top: 16px;
            font-size: 13px;
        }
        .area-content-alert span{
            width: 100%;
            height: auto;
            padding: 4px;
            background: #E7D3B9;
            color: #555555;
            margin-left: 10px;
        }
        .area-content-alert h2{
            color: #FFFFFF;
            font-size: 25px;
            padding: 5px;
        }
        .div-list{
            padding: 5px;
            font-size: 13px;

        }
        .div-list p{
            margin: 0;
            text-align: left;
            color: #555555 ;
        }

        .pagamento{
            height: 120px;
            border: 1px solid #82AC63;
            margin-bottom: 25px;
            padding: 0 6px 0 6px;
        }

        .centered {
            margin: 0 auto !important;
            float: none !important;
        }
        .no-padding{
            padding: 0;
        }
        .btn-ingridix-cli{
            height: 50px;
            padding: 12px 10px 8px 10px;
            border-radius: 40px;
            background: #ECBB71 !important;
            color: #FFFFFF;
            font-weight: bolder;
            font-size: 17px;
            margin-bottom: 15px;
            cursor: pointer;
            box-shadow: inset 0px 0px 30px 10px #e89f2f;
        }
        .btn-ingridix{
            height: 35px;
            padding: 9px 10px 8px 10px;
            border-radius: 40px;
            background: #ECBB71;
            color: #FFFFFF;
            font-weight: bolder;
            font-size: 13px;
            margin-bottom: 10px;
            cursor: pointer;
        }
        .nome-logado{
            text-align: left;
            color: #555555;
            margin-bottom: 15px;
        }
        #wrapper > .sh-page-layout-default{
            padding: 30px 0 ;
        }

        label{
            color: #FFFFFF;
            margin-right: 15px;
        }
        .imprimir-color{
            background: #374B58;
        }
        .enviar-email-color{
            background: #84B8D6;
        }
        .efetuar-color{
            background: #5C8F34;
        }
        .list-receita li{
            color: #555555;
            font-size: 12px;
            margin-bottom: 5px;
            text-align: left;
        }

        .preco h3{
            margin: 0;
            padding: 0;
            float: right;
        }

        .ingridix-pagamento{
            cursor: pointer;
        }

        .list-receita li span{
            float: right;
        }

        .area-menu li a{
            color: #555555 !important;
        }
        .area-menu li a:hover{
            color: #ECBB71 !important;
        }

        .area-menu-footer li a, .area-menu-footer li a:hover{
            color: #FFFFFF !important;
            font-size: 12px;
            text-decoration: underline;
        }

        /* Breakpoints */
        @media only screen and (min-width: 320px) and (max-width: 620px) {

            .col-md-9{
                padding: 0 5px 0 7px;
            }

            .area-content-all{
                height: auto;
            }

            .area-content-list{
                padding: 0;
            }
            .div-list {
                padding: 0 !important;
            }

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 10px;
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }
            .nome-logado{
                display: inline-block;
            }

            .btn-ingridix-cli{
                width: 100% !important;
            }
            .btn-ingridix{
                text-align: center;
            }

            .centered{
                text-align: left;
            }

            .cpf-span1{

                display: inline-block;
                width: 160px !important;
            }

            .cpf-span2{
                width: 92% !important;
                padding-right: 85px !important;
            }

            .p-hidden1{
                display: inline;
            }
            .p-hidden2{
                display: none;
            }
            .p-hidden3{
                display: none;
            }

            .div-list {
                margin-bottom: 30px;
                margin-top: 20px;
            }

            .area-content-alert{
                padding-bottom: 1px;
            }

            .preco{
                margin-bottom: 30px;
            }
        }

        @media only screen and (min-width: 621px) and (max-width: 940px) {

            .td_content2 {
                background-color: #F5F5F5;
                padding: 6px;
                color: #56A235;
                font-size: 10px;
            }

            .area-menu{
                padding-bottom: 20px;
                height: auto;
            }

            .btn-ingridix-cli{
                width: 100% !important;
            }

            .form-cpf{
                margin-top: 15px;
            }

            .error{
                width: 100%;
            }

            .area-content-all{
                width: 100%;
                height: auto;
            }

            .p-hidden1{
                display: none;
            }
            .p-hidden2{
                display: inline;
            }
            .p-hidden3{
                display: none;
            }

            .div-list {
                margin-bottom: 30px;
                margin-top: 20px;
            }

            .cpf-span2{
                width: 92% !important;
                padding-right: 234px !important;
            }

            .cpf-span1{

                display: inline-block;
                width: 310px !important;
            }

            .nome-logado{
                margin-bottom: 16px;
            }

            .area-content-alert{
                padding-bottom: 1px;
            }

            .preco{
                margin-bottom: 30px;
            }
        }

        @media only screen and (min-width: 941px) and (max-width: 1260px) {
            .p-hidden1{
                display: none;
                text-align: right;
            }
            .p-hidden2{
                display: none;
                text-align: right;
            }
            .p-hidden3{
                display: inline;
                text-align: right;
            }

            .cpf-span1{

                display: inline-block;
                width: 263px !important;
            }
        }

        @media only screen and (min-width: 1261px) {
            .p-hidden1{
                display: none;
                text-align: right;
            }
            .p-hidden2{
                display: none;
                text-align: right;
            }
            .p-hidden3{
                display: inline;
                text-align: right;
            }

            .cpf-span1{

                display: inline-block;
                width: 337px !important;
            }
        }
    </style>
    <center>
        <div class="container">

            <div class="row">

                <div class="col-md-11 centered">
                    <div class="col-md-11 no-padding">
                        <div class="button button-primary button-large btn-ingridix-cli pull-left"  style="width: 27.4%" title="Área cliente" onclick='location.href="<?= get_site_url().'/area-cliente'?>"'>ÁREA DO CLIENTE</div>
                    </div>
                    <div class="col-md-11 nome-logado">
                        Olá <b><?= $current_user->user_login; ?></b>
                    </div>
                    <div class="col-md-3">
                        <ul class="area-menu list-unstyled">
                            <li><a href="<?= get_site_url()?>/receitas-anteriores" >RECEITAS ANTERIORES</a></li>
                            <li><a href="http://ingridix.com.br/duvidas-sobre-o-uso" target="_blank">DÚVIDAS SOBRE O USO</a></li>
                            <li><br></li>

                            <div class="area-menu-footer">
                                <li><a href="<?= get_site_url()?>/meu-cadastro-paciente">MEUS DADOS DE CADASTRO</a></li>
                                <li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
                            </div>
                        </ul>
                    </div>

                    <div class="col-md-9 area-content">
                        <div class="col-md-12 area-content-all pull-right">
                            <div class="col-md-12 area-content-alert">
                                <div class="form-group col-md-7">
                                    MÉDICO <span class="cpf-span1" style="text-transform: uppercase;"><?= $rows[0]->user_login?></span>
                                </div>
                                <div class="form-group col-md-4">
									
									<?php
									if(!empty($crm)){
										?>
                                        CRM <span class="cpf-span2"><?= $crm; ?></span>
										
										<?php
									}else{
										?>
                                        CPF <span class="cpf-span2"><?= $cpf_medico; ?></span>
										<?php
									}
									?>

                                </div>
                            </div>

                            <div class="col-md-12 area-content-list">
                                <div class="col-md-4 ">
                                    <div class="pagamento ">
                                        <div class="button button-primary button-large btn-ingridix efetuar-color ingridix-pagamento" style="margin-top: 60px">EFETUAR O PAGAMENTO</div>
                                    </div>
                                    <div id="imprimir" class="button button-primary button-large btn-ingridix imprimir-color">IMPRIMIR RECEITA</div>
                                </div>
                                <div class="col-md-8 div-list" >
                                    <div class="col-md-12" style="margin-bottom: 10px">
                                        <p><b>Composto probiótico:</b> 30 cápsulas contendo:</p>
                                        <p>Cada cápsula contém:</p>
                                    </div>
                                    <div class="col-md-12 ">
                                        <ul class="list-unstyled list-receita">
											<?php
											$product_ids = "";
											$qtdProd = 0;
											foreach($rows as $key => $row):
												$qtdProd++;
												$cepa = $row->post_title;
												$lenghtCepa = strlen($cepa);
												$qtdDot = 120 - $lenghtCepa;
												
												?>
                                                <li class="li-ponto">
                                                    <input type="hidden" id="cepa_<?= ($key+1);?>" data-produto-id="<?= $row->ID; ?>" data-cepaQtd="<?= $row->qtd_produto;?>" >
													
													<?php echo $row->post_title;
													
													?>
                                                    <span class="pull-right"><?= "&nbsp". $row->qtd_produto; ?> BHL/UFC</span>

                                                    <b class="p-hidden1 pull-right"> ..... </b>
                                                    <b class="p-hidden2 pull-right"> ............................................</b>
                                                    <b class="p-hidden3 pull-right"> ....................................................................................</b>

                                                </li>
												
												<?php
												
												$_product = wc_get_product( $row->ID );
												$total +=  $_product->get_price() * $row->qtd_produto;
												
												$product_ids .= $row->ID.":".$row->qtd_produto.",";
												$valor = $row->valor + $frete;
												
											endforeach;
											
											$size = strlen($product_ids);
											
											$sis = substr($product_ids,0, -1);
											
											$product_ids = $sis;
											$add_to_cart_url = esc_url_raw( add_query_arg( 'add-to-cart', $product_ids, wc_get_checkout_url() ) );
											
											?>
                                        </ul>
                                    </div>
                                    <input type="hidden" id="qtdProd" value="<?= $qtdProd; ?>">
                                    <div class="col-md-12 pull-right preco">
                                        <h3>R$ <?=  number_format($valor, 2, ',', '.'); ?> </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </center>


    <div class="imprimir-content" style="display: none">

        <h2>Receita Ingridix</h2>

        <p><b>Dr. <?= $current_user->user_login; ?></b></p>
        <p><b>Paciente: <?= $rows[0]->nome_paciente?></b></p>

        <h3>Composto probiótico: 30 cápsulas contendo:</h3>
        <p>Cada cápsula contém:</p>
		
		<?php
		foreach($rows as $row):
			?>

            <p class="li-ponto"><?php echo $row->post_title;
				
				?>

                <span style="float: right;"> <b class="p-hidden3 "> .................................................................................................................</b> <?= "&nbsp". $row->qtd_produto; ?> BHL/UFC</span>

            </p>
			
			<?php
		endforeach;
		?>
    </div>



    <script>

        $(function(){

            $(".ingridix-pagamento").click(function(e){

                var totalQtdProd = $("#qtdProd").val();
                var idProd       = $(".list-receita li input").attr("data-produto-id");
                var qtdProd  = $(".list-receita li input").attr("data-cepaQtd");

                $("#id_loaderajax").removeClass("hidden");

                if(totalQtdProd <= 1){
                    
                    //Envia para função ajax, adiciona nova receita ao medico
                    $.ajax({
                        url: '<?= get_site_url(); ?>/?add-to-cart='+ idProd + '&quantity=' + qtdProd + '',
                        type: "get",
                        headers: {'X-Requested-With': 'XMLHttpRequest'},
                        success: function (data) {
                            
                            window.location.href="<?= get_site_url() . '/checkout?idr=' . $id_receita . '&idrr=' . $id_referencia; ?> ";
                        }
                    });

                }else{
               
                    //Envia para função ajax, adiciona nova receita ao medico
                    $.ajax({
                        url: '<?= $add_to_cart_url; ?>',
                        type: "get",
                        headers: {'X-Requested-With': 'XMLHttpRequest'},
                        success: function (data) {
                           
                            window.location.href="<?= get_site_url() . '/checkout?idr=' . $id_receita . '&idrr=' . $id_referencia; ?> ";
                        }
                    });
                }
            });

            $('#imprimir').bind('click',function() {

                myWindow=window.open('','','width=700,height=500');

                var content = $(".imprimir-content").html();

                myWindow.document.write("" + content + "");


                myWindow.document.close(); //missing code


                myWindow.focus();
                myWindow.print();

            });
        })

    </script>

<?php
get_footer();
?>