<?php
/**
 * Pay for order form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-pay.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see      https://docs.woocommerce.com/document/template-structure/
 * @author   WooThemes
 * @package  WooCommerce/Templates
 * @version  2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<form id="order_review" method="post">

	<table class="shop_table">
		<thead>
			<tr>
				<th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
				<th class="product-quantity"><?php _e( 'Qty', 'woocommerce' ); ?></th>
				<th class="product-total"><?php _e( 'Totals', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php if ( sizeof( $order->get_items() ) > 0 ) : ?>
				<?php foreach ( $order->get_items() as $item_id => $item ) : ?>
					<?php
						if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
							continue;
						}
					?>
					<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
						<td class="product-name">
							<?php
								echo apply_filters( 'woocommerce_order_item_name', esc_html( $item->get_name() ), $item, false );

								do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

								wc_display_item_meta( $item );

								do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
							?>
						</td>
						<td class="product-quantity"><?php echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', esc_html( $item->get_quantity() ) ) . '</strong>', $item ); ?></td>
						<td class="product-subtotal"><?php echo $order->get_formatted_line_subtotal( $item ); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<?php if ( $totals = $order->get_order_item_totals() ) : ?>
				<?php foreach ( $totals as $total ) : ?>
					<tr>
						<th scope="row" colspan="2"><?php echo $total['label']; ?></th>
						<td class="product-total"><?php echo $total['value']; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tfoot>
	</table>

	<div id="payment">
		<?php if ( $order->needs_payment() ) : ?>
			<ul class="wc_payment_methods payment_methods methods">
				<?php
					if ( ! empty( $available_gateways ) ) {
						foreach ( $available_gateways as $gateway ) {
							wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
						}
					} else {
						echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters( 'woocommerce_no_available_payment_methods_message', __( 'Sorry, it seems that there are no available payment methods for your location. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce' ) ) . '</li>';
					}
				?>
			</ul>
		<?php endif; ?>
		<div class="form-row">
			<input type="hidden" name="woocommerce_pay" value="1" />

			<?php wc_get_template( 'checkout/terms.php' ); ?>

			<?php do_action( 'woocommerce_pay_order_before_submit' ); ?>

			<?php echo apply_filters( 'woocommerce_pay_order_button_html', '<input type="submit" class="button alt" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '" />' ); ?>

			<?php do_action( 'woocommerce_pay_order_after_submit' ); ?>

			<?php wp_nonce_field( 'woocommerce-pay' ); ?>
		</div>
	</div>
</form>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src='<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.min.js'></script>
<script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.validate.min.js"></script>
<script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.maskedinput.min.js"></script>


<script>

    $(function(){

        $('[data-toggle="tooltip"]').tooltip();

        //Valida campos
        $("#place_order_custome").click(function() {

            var billing_first_name = $("#billing_first_name").val();
            var billing_last_name = $("#billing_last_name").val();
            var billing_country = $("#billing_country").val();
            var billing_address_1 = $("#billing_address_1").val();
            var billing_city = $("#billing_city").val();
            var billing_state = $("#billing_state").val();
            var billing_postcode = $("#billing_postcode").val();
            var billing_email = $("#billing_email").val();

            if (billing_first_name.length <= 0) {
                $("html, body").animate({ scrollTop: 300 }, "slow");
            }
            else if (billing_last_name.length <= 0) {
                $("html, body").animate({ scrollTop: 300 }, "slow");
            }
            else if (billing_country.length <= 0) {
                $("html, body").animate({ scrollTop: 500 }, "slow");
            }
            else if (billing_address_1.length <= 0) {
                $("html, body").animate({ scrollTop: 500 }, "slow");
            }
            else if (billing_city.length <= 0) {
                $("html, body").animate({ scrollTop: 700 }, "slow");
            }
            else if (billing_state.length <= 0) {
                $("html, body").animate({ scrollTop: 700 }, "slow");
            }
            else if (billing_postcode.length <= 0) {
                $("html, body").animate({ scrollTop: 900 }, "slow");
            }
            else if (billing_email.length <= 0) {
                $("html, body").animate({ scrollTop: 900 }, "slow");
            } else {

                $("#teste").click()
            }
        });


        $("#cartao_nascimento").mask( "99/99/9999" );
        $("#cartao_cpf").mask( "999.999.999-99" );
        $("#cartao_celular").mask( "(99) 99999-9999" );


        $("#cartao_credito, #cartao_mes, #cartao_ano, #cartao_codigo").on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('New message to ' + recipient)
            modal.find('.modal-body input').val(recipient)
        })

        //Remove a calass error do modal de pagamento
        $("#pagamento_ingridix input").change(function(){

            if($(this).val().length > 0){
                $(this).removeClass("error")
            }
        });

        //Validar tipo de cartão de credito
        $("#cartao_credito").keyup(function(){

            var cartao_credito = $(this).val();

            if(cartao_credito.length >= 13){

                $.ajax({
                    url: '<?= get_site_url()."/functions-ajax";?>',
                    type: "post",
                    data: { "bandeia_cartao" : cartao_credito,  "check_bandeira_cartao": "ok" },
                    dataType: "json",
                    success: function(data){

                        $(".img_cartoes img").removeClass("cartao_selecionado");

                        var bandeira_cartao = data[0];

                        switch(bandeira_cartao){
                            case bandeira_cartao = 'elo':
                                $("#elo").addClass("cartao_selecionado");
                                break;
                            case bandeira_cartao = 'hipercard':
                                $("#hiper").addClass("cartao_selecionado");
                                break;

                            case bandeira_cartao = 'amex':
                                $("#amex").addClass("cartao_selecionado");
                                break;

                            case bandeira_cartao = 'diners':
                                $("#diner").addClass("cartao_selecionado");
                                break;

                            case bandeira_cartao = 'discover':
                                $("#discover").addClass("cartao_selecionado");
                                break;

                            case bandeira_cartao = 'aura':
                                $("#aura").addClass("cartao_selecionado");
                                break;

                            case bandeira_cartao = 'jcb':
                                $("#jcb").addClass("cartao_selecionado");
                                break;

                            case bandeira_cartao = 'visa':
                                $("#visa").addClass("cartao_selecionado");
                                break;

                            case bandeira_cartao = 'master':
                                $("#master").addClass("cartao_selecionado");
                                break;
                        }

                        $("#bandeira").val("" + bandeira_cartao + "");

                    }
                })
            }
            return false;

        });

        //Botão de comprar
        $(".comprar").click(function(){

            var formCheckout      = $('form[name="checkout"]').serialize();
            var cartao_credito    = $("#cartao_credito").val();
            var cartao_mes        = $("#cartao_mes").val();
            var cartao_ano        = $("#cartao_ano").val();
            var cartao_codigo     = $("#cartao_codigo").val();
            var cartao_nome       = $("#catao_nome").val();
            var cartao_bandeira   = $("#bandeira").val();

            var referenciaReceita = $("#referenciaReceita").val();
            var itemNumero        = $("#itemNumero").val();

            var urlProd           = "";
            var numerodeid        = 0;

            $("#produtos_hidden input").each(function(){
                numerodeid++;
                var prodId  = $(this).attr("data-prod-id");
                var prodQtd = $(this).attr("data-prod-qtd");

                urlProd += "&prodId_"+ numerodeid +"=" + prodId + "&prodQtd_"+ numerodeid +"=" + prodQtd ;
            });


            var qtd = 0;
            $("#pagamento_ingridix input").each(function(){

                if($(this).val() == ""){

                    qtd++;
                    $(this).addClass("error")
                }
            });


            if(qtd <= 0){
                //Envia para função ajax
                $(".comprar").attr("disabled", "disabled");
                $("#id_loaderajax").removeClass("hidden");

                $.ajax({
                    url: '<?= get_site_url()."/functions-ajax";?>',
                    type: "post",
                    data: formCheckout + "&woocommerce_confirmacao=ok&cartao_credito=" +
                    cartao_credito + "&cartao_mes=" + cartao_mes + "&cartao_ano=" + cartao_ano + "&cartao_codigo=" + cartao_codigo +
                    "&cartao_nome=" + cartao_nome + urlProd + "&itemNumero=" + itemNumero + "&referenciaReceita=" + referenciaReceita + "&cartao_bandeira=" + cartao_bandeira ,
                    dataType: "json",
                    success: function(data){

                        if(data.status == 1){
                            location.href="" + data.url + "<?= '&idr='. $id_receita .'&idrr='. $id_referencia .'' ?>";
                        }else{

                            if(data.mensagem){
                                $("#id_loaderajax").addClass("hidden");
                                $(".comprar").removeAttr("disabled");

                                $("#alert_error_ultrapag").removeClass("hidden");
                                $("#alert_error_ultrapag_msg").html("Transação não autoriazada!<br> Entre em contato com seu banco emissor.");
                                $("#exampleModal").animate({ scrollTop: 0 }, "slow");
                            }else{
                                $("#id_loaderajax").addClass("hidden");
                                $(".comprar").removeAttr("disabled");

                                $("#alert_error_ultrapag").removeClass("hidden");
                                $("#alert_error_ultrapag_msg").html("" + data.erros[0]['mensagem'] + "");
                                $("#exampleModal").animate({ scrollTop: 0 }, "slow");
                            }
                        }
                    }
                })
            }
        });
    })

</script>