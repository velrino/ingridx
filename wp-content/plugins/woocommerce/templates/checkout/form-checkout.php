<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

global $wpdb;
//Dados do paciente
$id_receita    = strip_tags(filter_var ( $_GET['idr'], FILTER_SANITIZE_NUMBER_INT));
$id_referencia = strip_tags(filter_var ( $_GET['idrr'], FILTER_SANITIZE_NUMBER_INT));
$user_id       = $current_user->ID;

$rows = $wpdb->get_results( "select rm.nome_receita, rr.valor, rr.frete from referencia_receitas rr join receitas_medicos rm where rr.id_ref = rm.id_ref and rr.id_pac
                                    and rr.id_ref = $id_referencia" );


?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
	
	<?php if ( $checkout->get_checkout_fields() ) : ?>
		
		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

        <div class="col2-set" id="customer_details">
            <div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
            </div>

            <div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
            </div>

            <div id="produtos_hidden">
				<?php
				global $woocommerce;
				$items = $woocommerce->cart->get_cart();
				
				$total = 0;
				$_product =  wc_get_product(941);
				$itemNumero = 0;
		  
				foreach($items as $item => $values) {
					$itemNumer++;
					$_product =  wc_get_product( $values['data']->get_id());
					$produtoId = $values['data']->get_id();
					
					$qty = $values['quantity'];
					$price = get_post_meta($values['product_id'] , '_price', true);
					
					?>
					<input type="hidden" id="produto_<?= $itemNumer; ?>" name="produto_<?= $itemNumer; ?>" data-prod-id="<?= $produtoId; ?>" data-prod-qtd="<?= $qty?>" >
					<?php
     
				}
				
				?>
            </div>
            <input type="hidden" id="referenciaReceita" name="referenciaReceita" value="<?= $_GET['idrr']; ?>" >
            <input type="hidden" id="itemNumero" name="itemNumero" value="<?= $itemNumer; ?>" >
           
        </div>
		
		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
	
	<?php endif; ?>

    <h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
	

    <div >
        <table class="shop_table">
            <thead>
            <tr>
                <th class="product-name">Produto</th>
                <th class="product-total">Total</th>
            </tr>
            </thead>
            <tbody>
            <tr class="cart_item">
                <td class="product-name"><?php
	                if(empty($rows[0]->nome_produto)){
		                echo "Receita_".$id_receita . " x 1";
	                }else{
		                echo $rows[0]->nome_produto . " x 1";
	                }
	                ?>
                </td>
                <td class="product-total">
                </td>
            </tr>
            </tbody>
            <tfoot>

            <tr class="cart-subtotal">
                <th>Subtotal</th>
                <td>R$ <?php echo number_format($rows[0]->valor, 2, ',', '.'); ?></td>
            </tr>

            <tr class="cart-subtotal">
                <th>Frete</th>
                <td>R$ <?php
	                //$current_user = wp_get_current_user();
	                //	$cep = get_user_meta( $current_user->ID, 'billing_postcode', true );
	
	                $frete = number_format($rows[0]->frete, 2, ',', '.') ;
	                echo $frete;
	                ?></td>
            </tr>

            
            <tr class="order-total">
                <th>Total</th>
                <td>R$ <?php
	
	                $subtotal = number_format($rows[0]->valor, 2, ',', '.');
	
	                $total = $rows[0]->valor + $rows[0]->frete;
	                echo number_format($total, 2, ',', '.');
	                ?></td>
            </tr>


            </tfoot>
        </table>
        
        <div >
            <ul class="wc_payment_methods payment_methods methods">

                <li class="wc_payment_method payment_method_paypal">
                    <input type="radio" checked="" value="paypal" style="display: inline !important;margin: 0 1em 0 0 !important;">

                    <label for="payment_method_paypal">
                        Ultrapag </label>
                    <div class="payment_box payment_method_paypal">
                        <p>Pague com Ultrapag; você pode pagar com o seu cartão de crédito.</p>
                    </div>
                </li>
            </ul>
            <div class="form-row place-order">
                <noscript>
                    Seu navegador não suporta JavaScript ou ele está desativado, por favor clique no botão &amp;lt;em&amp;gt;Atualizar&amp;lt;/em&amp;gt; antes de finalizar seu pedido. Você pode ser cobrado mais do que a quantidade indicada acima, se você deixar de clicar.			&amp;lt;br/&amp;gt;&amp;lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Atualizar" /&amp;gt;
                </noscript>

                <input class="button alt pull-right" id="place_order_custome" value="Finalizar compra" data-value="Finalizar compra" style="text-align: center">
                <input type="hidden" id="teste" data-toggle="modal" data-target="#exampleModal" style="text-align: center">

            </div>

        </div>
        
        <style>
            .modal-dialog input{
                padding: 0px 5px 0px 5px;
                font-size: 16px;
                height: 45px;

            }

            .modal-dialog .btn.cancelar{
                background-color: #F0BE60 !important;
                color: #FFFFFF;
            }
            .modal-dialog .btn.comprar{
                background-color: #15bee4 !important;
                color: #FFFFFF;
            }

            select {
                line-height: 40px !important;
                height: 45px !important;
                padding: 0 5px 10px 7px !important;
            }

            hr{
                margin-top: 0 !important;
            }

            .inline-flex{
                display: inline-flex;
            }

            .inline-flex input{
                width: 40px;
                margin-right: 10px;
                text-align: center;
            }

            #cartao_codigo{
                width: 100px;
            }

            .error, .error:focus {
                box-shadow: 0 0 10px red;
            }

            .woocommerce-checkout .woocommerce h2 {
                margin-top: 0;
                margin-bottom: 0;
            }

            .entry-content img {
                margin: 0 -1px 0 0;
                width: 42px;
            }

            .img_cartoes {
                margin-bottom: 10px;
                text-align: center;
            }

            .cartao_selecionado{
                border: 2px solid #47e5cf;
                width: 55px !important;
                -webkit-transition: width 0.1s;
                -moz-transition: width 0.1s;
                -o-transition: width 0.1s;
                transition: width 0.1s;
            }

            @media only screen and (min-width: 320px) and (max-width: 620px) {
                .btn {
                    padding: 6px 8px;
                }
            }

            .hidden{
                display: none;
            }
        </style>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="border: 4px solid #ccc;">
                    <div class="modal-header" style="background: #f1f1f1; ">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <h2 style="color: #6C3B97">Pagamento</h2>
                    </div>
                    <div class="modal-body">

                        <!-- Menssagem de error  -->
                        <div id="alert_error_ultrapag" class="sh-alert sh-alert-error hidden">
                            <div class="sh-alert-data sh-table">
                                <div class="sh-table-cell sh-alert-data-icon">
                                    <i class="icon-info sh-alert-icon"></i>
                                </div>
                                <div class="sh-table-cell width-full">
                                    <h3 class="sh-alert-title">
                                        <div id="alert_error_ultrapag_msg"></div>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <!-- Menssagem de error  -->


                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group ">
                                    <label for="cartao_credito" class="control-label">Número do cartão*</label>
                                    <input type="text" class="form-control" name="cartao_credito" id="cartao_credito" maxlength="19" minlength="13">
                                </div>

                                <div class="row img_cartoes">
                                    <img id="visa" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/visa.jpg">
                                    <img id="master" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/master.jpg">
                                    <img id="diner" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/diners.jpg">
                                    <img id="elo" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/elo.jpg">
                                    <img id="amex" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/amex.jpg">
                                    <img id="discover" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/discovery.jpg">
                                    <img id="jcb" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/jcb.jpg">
                                    <img id="hiper" src="<?=  get_site_url(); ?>/wp-content/uploads/2017/10/hiper.jpg">
                                </div>

                                <input type="hidden" name="bandeira" id="bandeira">
                            </div>

                            <div class="col-md-4">
                                <label for="recipient-name" class="control-label">Data de validade*</label>
                                <div class="col-md-5">
                                    <div class="form-group inline-flex">
                                        <input type="text" class="form-control" name="cartao_mes" id="cartao_mes" maxlength="2" placeholder="MM">
                                        <input type="text" class="form-control" name="cartao_ano" id="cartao_ano" maxlength="2" placeholder="AA">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="cartao_nome" class="control-label">Nome impresso no cartão*</label>
                                    <input type="text" class="form-control" name="catao_nome" id="catao_nome">
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="cartao_codigo" class="control-label">Código de segurança*</label>

                                    <div class="inline-flex">
                                        <input type="text" class="form-control" name="cartao_codigo" id="cartao_codigo" maxlength="4">
                                        <i class="icon-info sh-alert-icon" data-toggle="tooltip" data-placement="top" title="" style="margin-top: 5px;" data-original-title="Código de 3 dígitos localizado no verso do cartão."></i>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">

                            <div class="col-md-12">
                                <h4 style="color: #6C3B97">Dados do dono do cartão</h4>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cartao_cpf" class="control-label">CPF do dono do cartão*</label>
                                    <input type="text" class="form-control" name="cartao_cpf" id="cartao_cpf">
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="cartao_celular" class="control-label">Celular do dono do cartão*</label>
                                    <input type="text" class="form-control" name="cartao_celular" id="cartao_celular">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="cartao_nascimento" class="control-label">Data de nascimento*</label>
                                    <input type="text" class="form-control" name="cartao_nascimento" id="cartao_nascimento" style="margin-bottom: 0">
                                    <small>Ex.: 20/05/1980</small>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">

                            <div class="col-md-12">
                                <h4 style="color: #6C3B97">Parcelamento</h4>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Pague em até 1x sem juros</label>
                                    <select class="form-control">
                                        <option selected="">1x sem juros</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn cancelar" data-dismiss="modal">Cancelar compra</button>
                        <button type="button" class="btn comprar">Confirmar pagamento</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <script src="<?=  get_site_url(); ?>/wp-content/themes/jevelin/js/jquery.min.js"></script>
        <script src="<?=  get_site_url(); ?>/wp-content/themes/jevelin/js/jquery.validate.min.js"></script>
        <script src="<?=  get_site_url(); ?>/wp-content/themes/jevelin/js/jquery.maskedinput.min.js"></script>
        
        <script>

            $(function(){

                $('[data-toggle="tooltip"]').tooltip();

                //Valida campos
                $("#place_order_custome").click(function() {

                    var billing_first_name = $("#billing_first_name").val();
                    var billing_last_name = $("#billing_last_name").val();
                    var billing_country = $("#billing_country").val();
                    var billing_address_1 = $("#billing_address_1").val();
                    var billing_city = $("#billing_city").val();
                    var billing_state = $("#billing_state").val();
                    var billing_postcode = $("#billing_postcode").val();
                    var billing_email = $("#billing_email").val();

                    if (billing_first_name.length <= 0) {
                        $("html, body").animate({ scrollTop: 300 }, "slow");
                    }
                    else if (billing_last_name.length <= 0) {
                        $("html, body").animate({ scrollTop: 300 }, "slow");
                    }
                    else if (billing_country.length <= 0) {
                        $("html, body").animate({ scrollTop: 500 }, "slow");
                    }
                    else if (billing_address_1.length <= 0) {
                        $("html, body").animate({ scrollTop: 500 }, "slow");
                    }
                    else if (billing_city.length <= 0) {
                        $("html, body").animate({ scrollTop: 700 }, "slow");
                    }
                    else if (billing_state.length <= 0) {
                        $("html, body").animate({ scrollTop: 700 }, "slow");
                    }
                    else if (billing_postcode.length <= 0) {
                        $("html, body").animate({ scrollTop: 900 }, "slow");
                    }
                    else if (billing_email.length <= 0) {
                        $("html, body").animate({ scrollTop: 900 }, "slow");
                    } else {

                        $("#teste").click()
                    }
                });


                $("#cartao_nascimento").mask( "99/99/9999" );
                $("#cartao_cpf").mask( "999.999.999-99" );
                $("#cartao_celular").mask( "(99) 99999-9999" );


                $("#cartao_credito, #cartao_mes, #cartao_ano, #cartao_codigo").on('keydown', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

                $('#exampleModal').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) // Button that triggered the modal
                    var recipient = button.data('whatever') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                    var modal = $(this)
                    modal.find('.modal-title').text('New message to ' + recipient)
                    modal.find('.modal-body input').val(recipient)
                })

                //Remove a calass error do modal de pagamento
                $("#pagamento_ingridix input").change(function(){

                    if($(this).val().length > 0){
                        $(this).removeClass("error")
                    }
                });

                //Validar tipo de cartão de credito
                $("#cartao_credito").keyup(function(){

                    var cartao_credito = $(this).val();

                    if(cartao_credito.length >= 13){

                        $.ajax({
                            url: '<?=  get_site_url(); ?>/functions-ajax',
                            type: "post",
                            data: { "bandeia_cartao" : cartao_credito,  "check_bandeira_cartao": "ok" },
                            dataType: "json",
                            success: function(data){

                                $(".img_cartoes img").removeClass("cartao_selecionado");

                                var bandeira_cartao = data[0];

                                switch(bandeira_cartao){
                                    case bandeira_cartao = 'elo':
                                        $("#elo").addClass("cartao_selecionado");
                                        break;
                                    case bandeira_cartao = 'hipercard':
                                        $("#hiper").addClass("cartao_selecionado");
                                        break;

                                    case bandeira_cartao = 'amex':
                                        $("#amex").addClass("cartao_selecionado");
                                        break;

                                    case bandeira_cartao = 'diners':
                                        $("#diner").addClass("cartao_selecionado");
                                        break;

                                    case bandeira_cartao = 'discover':
                                        $("#discover").addClass("cartao_selecionado");
                                        break;

                                    case bandeira_cartao = 'aura':
                                        $("#aura").addClass("cartao_selecionado");
                                        break;

                                    case bandeira_cartao = 'jcb':
                                        $("#jcb").addClass("cartao_selecionado");
                                        break;

                                    case bandeira_cartao = 'visa':
                                        $("#visa").addClass("cartao_selecionado");
                                        break;

                                    case bandeira_cartao = 'master':
                                        $("#master").addClass("cartao_selecionado");
                                        break;
                                }

                                $("#bandeira").val("" + bandeira_cartao + "");

                            }
                        })
                    }
                    return false;

                });

                //Botão de comprar
                $(".comprar").click(function(){

                    var formCheckout      = $('form[name="checkout"]').serialize();
                    var cartao_credito    = $("#cartao_credito").val();
                    var cartao_mes        = $("#cartao_mes").val();
                    var cartao_ano        = $("#cartao_ano").val();
                    var cartao_codigo     = $("#cartao_codigo").val();
                    var cartao_nome       = $("#catao_nome").val();
                    var cartao_bandeira   = $("#bandeira").val();

                    var referenciaReceita = $("#referenciaReceita").val();
                    var itemNumero        = $("#itemNumero").val();

                    var urlProd           = "";
                    var numerodeid        = 0;

                    $("#produtos_hidden input").each(function(){
                        numerodeid++;
                        var prodId  = $(this).attr("data-prod-id");
                        var prodQtd = $(this).attr("data-prod-qtd");

                        urlProd += "&prodId_"+ numerodeid +"=" + prodId + "&prodQtd_"+ numerodeid +"=" + prodQtd ;
                    });


                    var qtd = 0;
                    $("#pagamento_ingridix input").each(function(){

                        if($(this).val() == ""){

                            qtd++;
                            $(this).addClass("error")
                        }
                    });

                    if(qtd <= 0){
                        //Envia para função ajax
                        $(".comprar").attr("disabled", "disabled");
                        $("#id_loaderajax").removeClass("hidden");

                        $.ajax({
                            url: '<?=  get_site_url(); ?>/functions-ajax',
                            type: "post",
                            data: formCheckout + "&woocommerce_confirmacao=ok&cartao_credito=" +
                            cartao_credito + "&cartao_mes=" + cartao_mes + "&cartao_ano=" + cartao_ano + "&cartao_codigo=" + cartao_codigo +
                            "&cartao_nome=" + cartao_nome + urlProd + "&itemNumero=" + itemNumero + "&referenciaReceita=" + referenciaReceita + "&cartao_bandeira=" + cartao_bandeira ,
                            dataType: "json",
                            success: function(data){

                                if(data.status == 1){
                                    location.href="" + data.url + "&idr="+ <?= $id_receita;?> +"&idrr="+ <?= $id_referencia;?> ;
                                }else{

                                    if(data.mensagem){
                                        $("#id_loaderajax").addClass("hidden");
                                        $(".comprar").removeAttr("disabled");

                                        $("#alert_error_ultrapag").removeClass("hidden");
                                        $("#alert_error_ultrapag_msg").html("Transação não autoriazada!<br> Entre em contato com seu banco emissor.");
                                        $("#exampleModal").animate({ scrollTop: 0 }, "slow");
                                    }else{
                                        $("#id_loaderajax").addClass("hidden");
                                        $(".comprar").removeAttr("disabled");

                                        $("#alert_error_ultrapag").removeClass("hidden");
                                        $("#alert_error_ultrapag_msg").html("" + data.erros[0]['mensagem'] + "");
                                        $("#exampleModal").animate({ scrollTop: 0 }, "slow");
                                    }
                                }
                            }
                        })
                    }
                });
            })

        </script>

    </div>
	
</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
