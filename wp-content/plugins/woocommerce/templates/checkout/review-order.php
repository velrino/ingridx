<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $wpdb;
//Dados do paciente
$id_receita    = strip_tags(filter_var ( $_GET['idr'], FILTER_SANITIZE_NUMBER_INT));
$id_referencia = strip_tags(filter_var ( $_GET['idrr'], FILTER_SANITIZE_NUMBER_INT));
$user_id       = $current_user->ID;

$rows = $wpdb->get_results( "select rm.nome_receita, rr.valor, rr.frete from referencia_receitas rr join receitas_medicos rm where rr.id_ref = rm.id_ref and rr.id_pac
                                    and rr.id_ref = $id_referencia" );

/*
A função é composta dos seguintes itens:

$servico: Modalidade de frete, modalidadas válidas:
04510 - PAC (código antigo 41106 , alterado em 05/05/2017)
04014 - SEDEX (código antigo 40010, alterado em 05/05/2017)
40045 - SEDEX a Cobrar
40215 - SEDEX 10
40290 - SEDEX Hoje

$cep_origem: CEP de origem, utilize apenas números!
$cep_destino: CEP de destino, utilize apenas números!
$peso: Peso da encomenda, qualquer valor entre 0.3 e 30 kg.
$mao_propria: Entrega na sua casa, só são aceitos dois valores 's' e 'n', se for passado outro valor a função entenderá como 'n'
$valor_declarado: Valor declarado da encomenda, se desejar declarar, por exemplo, R$1,00, use 100.
$aviso_recebimento: Aviso de recebimento, só são aceitos dois valores 's' e 'n', se for passado outro valor a função entenderá como 'n'

Abaixo o exemplo de uso:
04014 - Sedex
97032120 - CEP de origem
71939360 - CEP de destino
2 - Peso (2 kilos)
n - Mão própria
700 - Valor declarado (R$7,00)
s - Aviso de recebimento
*/

?>
<table class="shop_table woocommerce-checkout-review-order-table">
    <thead>
    <tr>
        <th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
        <th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
    </tr>
    </thead>
    <tbody>
    <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
        <td class="product-name">
			<?php
			if(empty($rows[0]->nome_produto)){
				echo "Receita_".$id_receita . " x 1";
			}else{
				echo $rows[0]->nome_produto . " x 1";
			}
			
			?>
        </td>
        <td class="product-total">
        </td>
    </tr>
    </tbody>
    <tfoot>

    <tr class="cart-subtotal">
        <th><?php _e( 'Subtotal', 'woocommerce' ); ?></th>
        <td>R$<?php echo number_format($rows[0]->valor, 2, ',', '.'); ?></td>
    </tr>

    <tr class="cart-subtotal">
        <th>Frete</th>
        <td>R$ <?php
			//$current_user = wp_get_current_user();
		//	$cep = get_user_meta( $current_user->ID, 'billing_postcode', true );
	
	        $frete = number_format($rows[0]->frete, 2, ',', '.') ;
			echo $frete;
	        ?>
        </td>
    </tr>
	
	<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
        <tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
            <th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
            <td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
        </tr>
	<?php endforeach; ?>
	
	<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
		
		<?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
		
		<?php wc_cart_totals_shipping_html(); ?>
		
		<?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
	
	<?php endif; ?>
	
	<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
        <tr class="fee">
            <th><?php echo esc_html( $fee->name ); ?></th>
            <td><?php wc_cart_totals_fee_html( $fee ); ?></td>
        </tr>
	<?php endforeach; ?>
	
	<?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
		<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
			<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
                <tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
                    <th><?php echo esc_html( $tax->label ); ?></th>
                    <td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
                </tr>
			<?php endforeach; ?>
		<?php else : ?>
            <tr class="tax-total">
                <th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
                <td><?php wc_cart_totals_taxes_total_html(); ?></td>
            </tr>
		<?php endif; ?>
	<?php endif; ?>
	
	<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

    <tr class="order-total">
        <th><?php _e( 'Total', 'woocommerce' ); ?></th>
        <td>R$<?php
            
            $subtotal = number_format($rows[0]->valor, 2, ',', '.');
	       
	        $total = $rows[0]->valor + $rows[0]->frete;
            echo number_format($total, 2, ',', '.');
	        ?></td>
    </tr>
	
	<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

    </tfoot>
</table>
