<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! $order = wc_get_order( $order_id ) ) {
	return;
}

$tid            = $order->meta_data[0]->value;
$bandeira       = $order->meta_data[1]->value;
$tipo_pagamento = $order->meta_data[2]->value;
$nsu            = $order->meta_data[3]->value;

global $wpdb;
$current_user = wp_get_current_user();

//Dados do paciente
$id_receita    = strip_tags(filter_var ( $_GET['idr'], FILTER_SANITIZE_NUMBER_INT));
$id_referencia = strip_tags(filter_var ( $_GET['idrr'], FILTER_SANITIZE_NUMBER_INT));
$user_id       = $current_user->ID;

$rows = $wpdb->get_results( "select rm.nome_receita from referencia_receitas rr join receitas_medicos rm where rr.id_ref = rm.id_ref and rr.id_pac
                                    and rr.id_ref = $id_referencia" );

$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();

//Enviar email para tex
$rowsEmail = $wpdb->get_results( "select DISTINCT mr.id_produto, wp.post_title, mr.qtd_produto, cp.nome_paciente, cp.cpf_paciente, cp.cel_paciente, rr.valor, rr.frete from receitas_medicos rm join minhas_receitas mr join referencia_receitas rr join wp_posts wp join cadastros_pacientes cp
where rm.id_receita = mr.id_receita_med and mr.id_ref = rr.id_ref and cp.id_paciente = rr.id_pac
      and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_receita" );

$cpf_paciente = get_user_meta($user_id, "billing_cpf", "true");
$rowUserId = $wpdb->get_results("select user_id from wp_usermeta WHERE meta_value = '". $cpf_paciente ."' ");

$rowUserId = $wpdb->get_results("select * from wp_usermeta WHERE user_id = $user_id ");

foreach ($rowsEmail as $row){
	
	$endereco = $row->billing_address_1;
	$cidade   = $row->billing_city;
	$cep      = $row->billing_postcode;
	$estado   = $row->billing_state;
	$fone     = $row->billing_phone;
}

//Envia e-mail de confirmação de cadastro
//Função para chamar o e-mail correspondente

//pega cpf do médico


$trProduto = "<tr><th style='width: 20% !important;text-align: left;border-bottom: 1px solid #000;'>ID</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Nome</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Quantidade</th></tr>";

foreach($rowsEmail as $row){
	
	$id_produto = $row->id_produto;
	$cepa = $row->post_title;
	$qtd = $row->qtd_produto;
	
	$trProduto .= "<tr>
                        <td style='width: 40px !important;'>". $id_produto ."</td>
                        <td style='width: 40px !important;'>". $cepa ."</td>
                        <td style='width: 40px !important;'>". $qtd ."</td>
                    </tr>";
	
};

$thProduto = "<tr><th style='width: 20% !important;text-align: left;border-bottom: 1px solid #000;'>Fórmula</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Frete</th><th style='width: 40px !important;text-align: left;border-bottom: 1px solid #000;'>Total</th></tr>";

//Formata valores da fórmula
$formulaTotal = $rowsEmail[0]->valor + $rowsEmail[0]->frete;
$formulaTotal = number_format($formulaTotal, 2, ",", ".");

$trTotal .= "<tr>
                        <td style='width: 40px !important;'> R$ ". number_format($rowsEmail[0]->valor, 2, ",", ".") ."</td>
                        <td style='width: 40px !important;'> R$ ". number_format($rowsEmail[0]->frete, 2, ",", ".") ."</td>
                        <td style='width: 40px !important;'> R$ ". $formulaTotal ."</td>
                    </tr>";

$menssage = '
	
        <center>
                    <img width="200px" src="http://loja.ingridix.com.br/wp-content/uploads/2017/07/ingridix-logo.png">
                </center>
                
        <table style="width: 100%">
            <thead>
           
            </thead>
            <tbody>
            <tr>
                <td colspan="2">
                    <p style="font-size: 16px;font-weight: bolder;margin: 20px 0 0 0">Ordem id: '. $id_referencia .'</p>
                    <p style="font-size: 16px;font-weight: bolder;margin: 20px 0 0 0">Paciente</p>
                    <p style="font-size: 15px;margin-left: 30px;" >Nome: '. $current_user->user_login .'</p>
                    <p style="font-size: 15px;margin-left: 30px;">CPF: '. $cpf_paciente .'</p>
                    <p style="font-size: 15px;margin-left: 30px;">Celular: '. get_user_meta(  $user_id , 'billing_phone', true ) .'</p>
                    <p style="font-size: 15px;margin-left: 30px;">Endereço: '. get_user_meta(  $user_id , 'billing_address_1', true ) .'</p>
                    <p style="font-size: 15px;margin-left: 30px;">Complemento: '. get_user_meta(  $user_id , 'billing_address_2', true ) .'</p>
                    <p style="font-size: 15px;margin-left: 30px;">Bairro: '. get_user_meta(  $user_id , 'billing_bairro', true ) .' </p>
                    <p style="font-size: 15px;margin-left: 30px;">Cidade: '. get_user_meta(  $user_id , 'billing_city', true ) .'</p>
                    <p style="font-size: 15px;margin-left: 30px;">Estado: '. get_user_meta(  $user_id , 'billing_state', true ) .'</p>
            
                </td>
            </tr>
            <tr><p style="font-size: 16px;font-weight: bolder;margin: 20px 0 20px 0">Cepas</p></tr>
              '. $trProduto . '
            <p style="font-size: 16px;font-weight: bolder;margin: 20px 0 20px 0">Total</p>
            '. $thProduto . $trTotal .'
            
            </tbody>
            <tfoot></tfoot>
        </table>
        ';

//$to = "andre@agenciabowie.com.br";
$to = "luis.souza@texs.com.br";
//$to = "lyndon.omarques@gmail.com";

$subject = 'Confirmação de pagamento da receita INGRIDIX #'. $id_referencia .' ';
$headers = 'Content-Type: text/html; charset=UTF-8';

add_filter( 'wp_mail_content_type', 'set_html_content_type' );
function set_html_content_type() {
	return 'text/html';
}

add_filter('wp_mail_from','yoursite_wp_mail_from');
function yoursite_wp_mail_from($content_type) {
	return 'contato@ingridix.com.br';
}

add_filter('wp_mail_from_name','yoursite_wp_mail_from_name');
function yoursite_wp_mail_from_name($name) {
	return 'INGRIDIX';
}
//Envia e-mail de novo usuario
wp_mail( $to, $subject, $menssage, $headers );


?>

<section class="woocommerce-order-details">

    <h2 class="woocommerce-order-details__title"><?php _e( 'Order details', 'woocommerce' ); ?></h2>

    <table class="woocommerce-table woocommerce-table--order-details shop_table order_details">

        <thead>
        <tr>
            <th class="woocommerce-table__product-name product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
            <th class="woocommerce-table__product-table product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
        </tr>
        </thead>

        <tbody>
        <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
            <td class="product-name">
				<?php
				if(empty($rows[0]->nome_produto)){
					echo "Receita INGRIDIX x 1";
				}else{
					echo $rows[0]->nome_produto . " x 1";
				}
				?>
            </td>
            <td class="product-total">
            </td>
        </tr>
        <tr>
            <th scope="row">Tipo pagamento</th>
            <td><?php echo $tipo_pagamento; ?></td>
        </tr>

        <tr>
            <th scope="row">Bandeira do cartão</th>
            <td><?php echo $bandeira; ?></td>
        </tr>

        <tr>
            <th scope="row">Parcelas</th>
            <td>1x</td>
        </tr>

        <tr>
            <th scope="row">TID</th>
            <td><?php echo $tid; ?></td>
        </tr>

        <tr>
            <th scope="row">NSU</th>
            <td><?php echo $nsu; ?></td>
        </tr>
        </tbody>

        <!--<tfoot>
			<?php
		foreach ( $order->get_order_item_totals() as $key => $total ) {
			?>
					<tr>
						<th scope="row"><?php echo $total['label']; ?></th>
						<td><?php echo $total['value']; ?></td>
					</tr>
					<?php
		}
		?>
		</tfoot>-->

    </table>
    <div class="col-md-11 no-padding">
        <div class="button button-primary button-large btn-ingridix-cli pull-left"  style="width: 27.4%" title="Área cliente" onclick='location.href="<?= get_site_url().'/area-cliente'?>"'>ÁREA DO CLIENTE</div>
    </div>
	<?php do_action( 'woocommerce_order_details_after_order_table', $order ); ?>
	
	<?php if ( $show_customer_details ) : ?>
		<?php wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) ); ?>
	<?php endif; ?>

</section>
